package com.auezeride.driver.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.TripHistoryModel;
import com.auezeride.driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.MyViewHolder> {

    List<TripHistoryModel> tripModels;
    Activity activity;
    CallTripDetailFragment callTripDetailFragment;

    public TripAdapter(List<TripHistoryModel> tripModels, Activity activity, CallTripDetailFragment callTripDetailFragment) {
        this.tripModels = tripModels;
        this.activity = activity;
        this.callTripDetailFragment = callTripDetailFragment;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.youtrip_adpater, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TripHistoryModel tripHistoryModel = tripModels.get(position);
        if (tripHistoryModel.getStatus().equalsIgnoreCase("Finished")) {
            holder.bookIdTxt.setText("BookID:" + Utiles.Nullpointer(tripHistoryModel.getTripno()));
            holder.pickupTxt.setText(Utiles.Nullpointer(tripHistoryModel.getAdsp().getFrom()));
            holder.dropAddressTxt.setText(Utiles.Nullpointer(tripHistoryModel.getAdsp().getTo()));
            holder.statusTxt.setText(Utiles.Nullpointer(tripHistoryModel.getStatus()));
            holder.dateTxt.setText(tripHistoryModel.getDate());
        } else {
            holder.bookIdTxt.setText("BookID:" + Utiles.Nullpointer(tripHistoryModel.getTripno()));
            holder.pickupTxt.setText(Utiles.Nullpointer(tripHistoryModel.getDsp().getStart()));
            holder.dropAddressTxt.setText(Utiles.Nullpointer(tripHistoryModel.getDsp().getEnd()));
            holder.statusTxt.setText(Utiles.Nullpointer(tripHistoryModel.getStatus()));
            holder.dateTxt.setText(Utiles.Nullpointer(tripHistoryModel.getDate()));
        }
        holder.layoutOnclick.setTag(position);
        holder.layoutOnclick.setOnClickListener(v -> {
            int position1 = (int) v.getTag();
            callTripDetailFragment.tripFragment(Utiles.Nullpointer(String.valueOf(tripModels.get(position1).getId())));
        });


    }

    @Override
    public int getItemCount() {
        return tripModels.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.book_id_txt)
        TextView bookIdTxt;
        @BindView(R.id.date_txt)
        TextView dateTxt;
        @BindView(R.id.pickup_txt)
        TextView pickupTxt;
        @BindView(R.id.drop_address_txt)
        TextView dropAddressTxt;
        @BindView(R.id.status_txt)
        TextView statusTxt;
        @BindView(R.id.layout_onclick)
        LinearLayout layoutOnclick;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface CallTripDetailFragment {
        void tripFragment(String tripid);
    }
}
