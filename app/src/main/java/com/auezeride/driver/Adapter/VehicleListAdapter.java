package com.auezeride.driver.Adapter;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import java.util.List;

import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.R;

public class VehicleListAdapter extends RecyclerView.Adapter<VehicleListAdapter.MyViewHolder> {

    private List<ListVehicleModel> vehicleModels;
    private Activity activity;
    private UpdateVehicleInterface updateVehicleInterface;

    public VehicleListAdapter(Activity activity, List<ListVehicleModel> vehicleModels, UpdateVehicleInterface updateVehicleInterface) {
        this.activity = activity;
        this.vehicleModels = vehicleModels;
        this.updateVehicleInterface = updateVehicleInterface;
    }

    @NonNull
    @Override
    public VehicleListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_list_adapter, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        return new MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull VehicleListAdapter.MyViewHolder holder, int position) {
        final ListVehicleModel vehicleModel = vehicleModels.get(position);
        if (SharedHelper.getKey(activity.getApplicationContext(), "vehicleId") != null && SharedHelper.getKey(activity.getApplicationContext(), "vehicleId").equalsIgnoreCase(vehicleModel.getId())) {
            holder.vehicle_radio_button.setChecked(true);
        } else {
            holder.vehicle_radio_button.setChecked(false);
        }
        holder.vehicle_radio_button.setText("  " + vehicleModel.getMakename() + " " + vehicleModel.getModel() +"( "+vehicleModel.getVehicletype() +" )");
        holder.vehicle_radio_button.setTag(position);
        holder.vehicle_radio_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                updateVehicleInterface.UpdateVehicle(vehicleModels.get(pos));
            }
        });

    }

    @Override
    public int getItemCount() {
        return vehicleModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        RadioButton vehicle_radio_button;

        public MyViewHolder(View itemView) {
            super(itemView);
            vehicle_radio_button = itemView.findViewById(R.id.vehicle_radio_button);
        }
    }

    public interface UpdateVehicleInterface {
        void UpdateVehicle(ListVehicleModel listVehicleModel);
    }
}
