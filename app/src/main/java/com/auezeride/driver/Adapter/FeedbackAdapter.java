package com.auezeride.driver.Adapter;

import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.FeedbackListdataModel;
import com.auezeride.driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackAdapter extends RecyclerView.Adapter<FeedbackAdapter.ViewHolder> {
    Activity context;
    List<FeedbackListdataModel> feedbacksmodel;
    String profile_url;

    public FeedbackAdapter(Activity context, List<FeedbackListdataModel> feedbacksmodel, String profile_url) {
        this.context = context;
        this.feedbacksmodel = feedbacksmodel;
        this.profile_url = profile_url;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_feedback, parent, false);
        FontChangeCrawler fontChangeCrawler = new FontChangeCrawler(context.getAssets(), context.getString(R.string.app_font));
        fontChangeCrawler.replaceFonts(context.findViewById(android.R.id.content));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FeedbackListdataModel feedback = feedbacksmodel.get(position);

        Utiles.CircleImageView(profile_url + feedback.getUserpic(), holder.feedbackImage, context);
        holder.txtName.setText(feedback.getUserbaneme());
        if (feedback.getComment()!=null) {
            holder.feedBackTxt.setText(feedback.getComment());
        }

    }

    @Override
    public int getItemCount() {
        return feedbacksmodel.size();
    }

    static

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.feedback_image)
        ImageView feedbackImage;
        @BindView(R.id.txt_name)
        TextView txtName;
        @BindView(R.id.feedBack_txt)
        TextView feedBackTxt;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }


}
