package com.auezeride.driver.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.DeleteVehicle;
import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.Presenter.ManageVehicelPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.VehicleManageView;
import retrofit2.Response;

public class VehiclesAdapter extends RecyclerView.Adapter<VehiclesAdapter.ViewHolder> implements VehicleManageView {

    private Activity context;
    private List<ListVehicleModel> vehicleModels;
    private RemvetheVehicle remvetheVehicle;
    private ManageVehicelPresenter manageVehicelPresenter;
    private int position_del;

    public VehiclesAdapter(Activity context, List<ListVehicleModel> vehicleModels, RemvetheVehicle remvetheVehicle) {
        this.context = context;
        this.vehicleModels = vehicleModels;
        this.remvetheVehicle = remvetheVehicle;
        manageVehicelPresenter = new ManageVehicelPresenter(this);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_manage_vehicle, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(parent.getContext().getAssets(), context.getString(R.string.app_font));
        fontChanger.replaceFonts(context.findViewById(android.R.id.content));

        return new ViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ListVehicleModel listVehicleModel = vehicleModels.get(position);
        holder.make_txt.setText(listVehicleModel.getMakename());
        holder.active_txt.setText(listVehicleModel.getTaxistatus());
        holder.licence_txt.setText(listVehicleModel.getVehicletype());
        holder.delete_img.setTag(position);
        holder.delete_img.setOnClickListener(v -> {
            position_del = (int) v.getTag();
            if (SharedHelper.getKey(context.getApplicationContext(), "vehicleId") != null && SharedHelper.getKey(context.getApplicationContext(), "vehicleId").equalsIgnoreCase(vehicleModels.get(position_del).getId())) {
                Alertdialog("You Cannot Delete Current Vehicle..", true, 0);
            } else {

                Alertdialog("Are You Sure Wanna Remove the Vehicle", false, position_del);
            }

        });
        holder.edit_img.setTag(position);
        holder.edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_del = (int) v.getTag();
                remvetheVehicle.editVehicle(vehicleModels.get(position_del).getId(), position_del);
            }
        });
        holder.doc_img.setTag(position);
        holder.doc_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position_del = (int) v.getTag();
                if (Utiles.IsNull(vehicleModels.get(position_del).getInsurance())) {
                    SharedHelper.putKey(context, "vehi_insurance", SharedHelper.getKey(context, "filepath") + vehicleModels.get(position_del).getInsurance());

                } else {
                    SharedHelper.putKey(context, "vehi_insurance", "");

                }
                if (Utiles.IsNull(vehicleModels.get(position_del).getPermit())) {
                    SharedHelper.putKey(context, "vehi_premit", SharedHelper.getKey(context, "filepath") + vehicleModels.get(position_del).getPermit());

                } else {
                    SharedHelper.putKey(context, "vehi_premit", "");

                }
                if (Utiles.IsNull(vehicleModels.get(position_del).getRegistration())) {
                    SharedHelper.putKey(context, "vehi_reg", SharedHelper.getKey(context, "filepath") + vehicleModels.get(position_del).getRegistration());

                } else {
                    SharedHelper.putKey(context, "vehi_reg", "");

                }
                SharedHelper.putKey(context, "vehi_insurance_date", "");
                SharedHelper.putKey(context, "vehi_premit_date", vehicleModels.get(position_del).getPermitexpdate());
                SharedHelper.putKey(context, "vehi_reg_date", vehicleModels.get(position_del).getRegistrationexpdate());
                remvetheVehicle.docupdate(vehicleModels.get(position_del).getId(), position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return vehicleModels.size();
    }

    @Override
    public void OnsuccessFully(Response<List<ListVehicleModel>> Response) {

    }

    @Override
    public void OnFailure(Response<List<ListVehicleModel>> Response) {

    }

    @Override
    public void DeletesuccessFully(Response<DeleteVehicle> Response) {
        vehicleModels.remove(position_del);
        notifyItemRemoved(position_del);
        notifyItemRangeChanged(position_del, vehicleModels.size());
    }

    @Override
    public void DeleteFailure(Response<DeleteVehicle> Response) {

    }

    public interface RemvetheVehicle {
        void Removehicle(String strMake_id);

        void editVehicle(String strMake_id, int position);

        void docupdate(String strMake_id, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView doc_img, edit_img;
        ImageView delete_img;
        TextView make_txt, licence_txt, active_txt;

        public ViewHolder(View view) {
            super(view);
            delete_img = view.findViewById(R.id.delete_img);
            doc_img = view.findViewById(R.id.doc_img);
            edit_img = view.findViewById(R.id.edit_img);
            make_txt = view.findViewById(R.id.make_txt);
            licence_txt = view.findViewById(R.id.licence_txt);
            active_txt = view.findViewById(R.id.active_txt);
        }
    }

    public void Alertdialog(String Message, Boolean status, final int position) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        if (status) {

            builder1.setMessage(Message);
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();

                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        } else {

            builder1.setMessage(Message);
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            HashMap<String, String> map = new HashMap<>();
                            map.put("makeid", vehicleModels.get(position).getId());
                            map.put("driverid", SharedHelper.getKey(context, "userid"));
                            manageVehicelPresenter.getDeleteVehicleList(map, context);
                            dialog.cancel();

                        }
                    });
            builder1.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();

                }
            });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

    }
}
