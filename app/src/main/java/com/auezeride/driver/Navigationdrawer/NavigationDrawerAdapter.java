package com.auezeride.driver.Navigationdrawer;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.R;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Activity context;

    public NavigationDrawerAdapter(Activity context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.activity_navigation_drawer_adapter, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(context.getAssets(),context.getString(R.string.app_font));
        fontChanger.replaceFonts(context.findViewById(android.R.id.content));
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(Constants.capitalizeFirstLetter(current.getTitle()));
        switch (position) {
            case 0:
                holder.nav_img.setImageResource(R.drawable.ic_profiles);
                break;
            case 1:
                holder.nav_img.setImageResource(R.drawable.ic_bank);
                break;
            case 2:
                holder.nav_img.setImageResource(R.drawable.ic_trip);
                break;
            case 3:
                holder.nav_img.setImageResource(R.drawable.ic_rating);
                break;
            case 4:
                holder.nav_img.setImageResource(R.drawable.ic_vehicle);
                break;
            case 5:
                holder.nav_img.setImageResource(R.drawable.ic_doc);
                break;
            case 6:
                holder.nav_img.setImageResource(R.drawable.ic_money);
                break;

        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView nav_img;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.nav_title);
            nav_img = itemView.findViewById(R.id.nav_img);
        }
    }
}