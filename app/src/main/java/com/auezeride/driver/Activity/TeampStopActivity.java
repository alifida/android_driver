package com.auezeride.driver.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.auezeride.driver.CommonClass.CommonFirebaseListoner;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.R;
import com.auezeride.driver.Service.TripRquestService;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TeampStopActivity extends AppCompatActivity {

    @Nullable
    @BindView(R.id.app_logo_img)
    ImageView appLogoImg;
    @BindView(R.id.english)
    @Nullable
    TextView english;
    @Nullable
    @BindView(R.id.tamil)
    TextView tamil;
    Context context = TeampStopActivity.this;
    AppCompatActivity activity = TeampStopActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teamp_stop);
        ButterKnife.bind(this);
        if (Utiles.isMyServiceRunning(activity, TripRquestService.class)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                stopService(new Intent(context, TripRquestService.class));
            } else {
                stopService(new Intent(context, TripRquestService.class));
            }
        }


        try {
            assert tamil != null;
            tamil.setText(CommonFirebaseListoner.strTamilAlert);
            assert english != null;
            english.setText(CommonFirebaseListoner.strEnglishAlert);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
