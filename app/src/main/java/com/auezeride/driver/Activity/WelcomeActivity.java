package com.auezeride.driver.Activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.CommonFirebaseListoner;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Fragment.LoginFragment;
import com.auezeride.driver.Fragment.RegisterFragment;
import com.auezeride.driver.Model.LanguageCurrencyModel;
import com.auezeride.driver.R;
import com.auezeride.driver.View.CurrencyLanguageView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity implements CurrencyLanguageView {

    @BindView(R.id.language_spn)
    Spinner languageSpn;
    @BindView(R.id.currency_spn)
    Spinner currencySpn;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.register_btn)
    Button registerBtn;
    Fragment fragment = null;
    AppCompatActivity activity = WelcomeActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
       // CurrencyLanguagePresenter currencyLanguagePresenter = new CurrencyLanguagePresenter(this);
       // currencyLanguagePresenter.getCurrencyLanguage(this);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(),getString(R.string.app_font));
        fontChanger.replaceFonts(this.findViewById(android.R.id.content));
        CommonFirebaseListoner.setActivity(activity);
       // BatteryOptimization();

    }

    @OnClick({R.id.login_btn, R.id.register_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                fragment = new LoginFragment();
                moveToFragment(fragment);
                break;
            case R.id.register_btn:
                fragment = new RegisterFragment();
                moveToFragment(fragment);
                break;
        }
    }

    public void LanguageSpinner(List<LanguageCurrencyModel.Data> Data) {
        List<String> langaugeList = new ArrayList<String>();
        for (int i = 0; Data.size() > i; i++) {
            //langaugeList.add(Data.get(i).getName());
        }
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(this,
                R.layout.spinnertext, langaugeList);
        languageAdapter.setDropDownViewResource(R.layout.dropdown);
        languageSpn.setAdapter(languageAdapter);
        languageSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                CommonData.strLanguage = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void Currencyspinner(List<LanguageCurrencyModel.Data> Data) {

        List<String> currencyList = new ArrayList<String>();
        for (int i = 0; Data.size() > i; i++) {
           // currencyList.add(Data.get(i).getName());

        }
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<String>(this,
                R.layout.spinnertext, currencyList);
        currencyAdapter.setDropDownViewResource(R.layout.dropdown);
        currencySpn.setAdapter(currencyAdapter);
        currencySpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                CommonData.strCurrency = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void moveToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.containter, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

    }

    @Override
    public void countriesReady(List<LanguageCurrencyModel> LanguageCurrencyModel) {

        /*for (int i = 0; LanguageCurrencyModel.size() > i; i++) {
            if (LanguageCurrencyModel.get(i).getId().equalsIgnoreCase("1")) {
                Currencyspinner(LanguageCurrencyModel.get(i).getDatas());
            } else if (LanguageCurrencyModel.get(i).getId().equalsIgnoreCase("2")) {
                LanguageSpinner(LanguageCurrencyModel.get(i).getDatas());
            }

        }*/


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utiles.clearInstance();
    }
   /* private void BatteryOptimization(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String packageName = activity.getPackageName();
            PowerManager pm = (PowerManager) activity.getSystemService(activity.POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {
                Intent intent = new Intent();
                intent.setAction(android.provider.Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("package:" + packageName));
                activity.startActivity(intent);
            }
        }


    }*/
}
