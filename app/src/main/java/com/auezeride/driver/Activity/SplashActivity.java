package com.auezeride.driver.Activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.CommonFirebaseListoner;
import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Fragment.AddVehicleFragment;
import com.auezeride.driver.MainActivity;
import com.auezeride.driver.Model.DriverProfileModel;
import com.auezeride.driver.Presenter.DriverProfilePresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.ProfileView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements ProfileView  {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;
    private static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 5;
    Context context = SplashActivity.this;
    AppCompatActivity activity = SplashActivity.this;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Constants.Previousstatus = "0";
        CommonFirebaseListoner.Previous = "0";
        CommonData.RequestBoolean = false;
        CommonData.isFistTime = true;
        CommonFirebaseListoner.setActivity(activity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkMultiplePermissions();
        } else {
            GO();
        }
        firebasetoken();
    }


    public void GO() {
        new Handler().postDelayed(() -> {
            CommonFirebaseListoner.FirebaseTripFlow();
            String login_status = SharedHelper.getKey(getApplicationContext(), "login_status");
            if (login_status != null && !login_status.isEmpty()) {
                ProfilePresenter();
            } else {
                Intent i = new Intent(context, WelcomeActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    public void ProfilePresenter() {
        showLoader();
        DriverProfilePresenter driverProfilePresenter = new DriverProfilePresenter(this);
        driverProfilePresenter.getProfile(activity, false);
    }

    private void checkMultiplePermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            List<String> permissionsNeeded = new ArrayList<String>();
            List<String> permissionsList = new ArrayList<String>();


            if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION)) {
                permissionsNeeded.add("Access the Current Location");
                if (Build.VERSION.SDK_INT >= 29){
                    if (!addPermission(permissionsList, Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                        permissionsNeeded.add("Access the Background Location");
                    }
                }
            }

            if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                permissionsNeeded.add("Access Fine Location");
            }
           /* if(!addPermission(permissionsList, android.Manifest.permission.CALL_PHONE))
            {
                permissionsNeeded.add("Call Phone");
            }*/

            if (permissionsList.size() > 0) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            } else {
                GO();
            }
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= 23)

            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);

                // Check for Rationale Option
                return shouldShowRequestPermissionRationale(permission);
            }
        return true;
    }

    public void showLoader() {
        animationView.setVisibility(View.VISIBLE);
    }

    public void dismissiLoader() {
        animationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<String, Integer>();

                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    GO();
                    return;
                } else {
                    if (Build.VERSION.SDK_INT >= 23) {
                        Toast.makeText(getApplicationContext(), "Please permit all the permissions", Toast.LENGTH_LONG).show();
                        finish();
                    }
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    @Override
    public void OnSuccessfully(Response<List<DriverProfileModel>> Response) {
        dismissiLoader();
        if (Response.body() != null && !Response.body().isEmpty()) {
            List<DriverProfileModel> driverProfileModels = Response.body();
            SharedHelper.putKey(getApplicationContext(), "fname", driverProfileModels.get(0).getFname());
            SharedHelper.putKey(getApplicationContext(), "code", driverProfileModels.get(0).getCode());
            SharedHelper.putKey(getApplicationContext(), "lname", driverProfileModels.get(0).getLname());
            SharedHelper.putKey(getApplicationContext(), "email", driverProfileModels.get(0).getEmail());
            SharedHelper.putKey(getApplicationContext(), "phcode", driverProfileModels.get(0).getPhcode());
            SharedHelper.putKey(getApplicationContext(), "phone", driverProfileModels.get(0).getPhone());
            SharedHelper.putKey(getApplicationContext(), "lang", driverProfileModels.get(0).getLang());
            SharedHelper.putKey(getApplicationContext(), "cur", driverProfileModels.get(0).getCur());
            SharedHelper.putKey(getApplicationContext(), "filepath", driverProfileModels.get(0).getBaseurl());
            SharedHelper.putOnline(activity, "isconnect", driverProfileModels.get(0).getConnected());
            if (Utiles.IsNull(driverProfileModels.get(0).getLicence())) {
                SharedHelper.putKey(getApplicationContext(), "licence", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getLicence());

            }
            if (Utiles.IsNull(driverProfileModels.get(0).getAadhar())) {
                SharedHelper.putKey(getApplicationContext(), "insurance", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getAadhar());

            }
       /* if (Utiles.IsNull(driverProfileModels.get(0).getPassing())) {
            SharedHelper.putKey(getApplicationContext(), "passing", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getPassing());

        }*/
            SharedHelper.putKey(getApplicationContext(), "licence_date", driverProfileModels.get(0).getLicenceexp());
            SharedHelper.putKey(getApplicationContext(), "insurance_date", driverProfileModels.get(0).getAadharexp());
            // SharedHelper.putKey(getApplicationContext(), "passing_date", driverProfileModels.get(0).getPassingexp());

            SharedHelper.putKey(context, "profile", driverProfileModels.get(1).getProfileurl());
            try {
                Constants.WalletAlertEnable = driverProfileModels.get(3).getIsDriverCreditModuleEnabledForUseAfterLogin();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (driverProfileModels.get(2).getCurrentActiveTaxi() != null) {
                SharedHelper.putKey(context, "vehicleId", driverProfileModels.get(2).getCurrentActiveTaxi().getId());
                SharedHelper.putKey(context, "vmake", driverProfileModels.get(2).getCurrentActiveTaxi().getMakename());
                SharedHelper.putKey(context, "vmodel", driverProfileModels.get(2).getCurrentActiveTaxi().getModel());
                SharedHelper.putKey(context, "numplate", driverProfileModels.get(2).getCurrentActiveTaxi().getLicence());
                Intent i = new Intent(context, MainActivity.class);
                startActivity(i);
                finish();
            } else {
                Alertdialog();
            }
        } else {
            Utiles.displayMessage(getCurrentFocus(), context, context.getResources().getString(R.string.something_went_wrong));
            Intent i = new Intent(context, WelcomeActivity.class);
            startActivity(i);
            finish();
        }

    }

    @Override
    public void OnFailure(Response<List<DriverProfileModel>> Response) {
        Utiles.displayMessage(getCurrentFocus(), context, context.getResources().getString(R.string.something_went_wrong));
    }

    public void Alertdialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage("Please add Your Vehicle");
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                "OK",
                (dialog, id) -> {
                    SharedHelper.putKey(context, "appflow", "registration");
                    Fragment fragment = new AddVehicleFragment();
                    moveToFragment(fragment);
                    dialog.cancel();

                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void moveToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, fragment.getClass().getSimpleName()).commitAllowingStateLoss();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utiles.clearInstance();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }

    public void firebasetoken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    // Get new Instance ID token
                    String token = Objects.requireNonNull(task.getResult()).getToken();
                    SharedHelper.putToken(getApplicationContext(),"device_token",token);
                });

    }
}
