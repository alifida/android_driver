package com.auezeride.driver.Service;

import android.Manifest;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import android.util.Log;


import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import com.auezeride.driver.Activity.WelcomeActivity;
import com.auezeride.driver.BuildConfig;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Stopwatch;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Geofire.GeoFire;
import com.auezeride.driver.MainActivity;

import com.auezeride.driver.Apllicationcontroller.Appcontroller;
import com.auezeride.driver.CommonClass.Constants;
import com.google.android.gms.location.LocationListener;
import com.auezeride.driver.Presenter.FeedBackPresenter;
import com.auezeride.driver.R;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;


/**
 * Created by com on 08-Jun-18.
 */

public class TripRquestService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, Stopwatch.StopWatchListener {
    public static Context context;
    public String strDriverID = "";
    static DatabaseReference RequestDatabase, AppAutoLoggedOut;
    static ValueEventListener RequestValueEvent, AppAutoLoggedOutValue;
    private static final String TAG = TripRquestService.class.getSimpleName();
    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();

    Location mCurrentLocation, starLocation, EndLocation;
    LatLng updateLcation = new LatLng(0.0, 0.0);

    public static final String ACTION_LOCATION_BROADCAST = TripRquestService.class.getName() + "LocationBroadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    public static GeoFire geoFire;
    public float previousBearing = 0;
    private Subscription subscription;

    private Boolean isNotification = true;
    NotificationManager notificationManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        context = getApplicationContext();
        strDriverID = SharedHelper.getKey(context, "userid");

        if (subscription == null) {
            subscription = Observable.timer(10, TimeUnit.SECONDS)
                    .repeat()
                    .subscribe(aLong -> {
                        CallApi();
                        System.out.println("enter the no request" + strDriverID);
                    });

        }
        //
        addAutoStartup();
        getRequest();
        getCheckLogout();
        ReinitializeTimer();
    }

    private void CallApi() {
        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!gps_enabled) {
            if (SharedHelper.getKey(context, "category") != null && !SharedHelper.getKey(context, "category").isEmpty()) {
                geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child(SharedHelper.getKey(context, "category").toLowerCase()));
                geoFire.removeLocation(strDriverID);
            }
            upDateLocationPresnter("0", new LatLng(0.0, 0.0));
            if (notificationManager == null) {
                sendNotification("Please turn on your location");
            }

        } else {
            if (SharedHelper.getOnlineStatus(context, "onlineStatus")) {
                upDateLocationPresnter("1", updateLcation);
                if (!Utiles.isNetworkAvailable(context)) {
                    SharedHelper.putOnline(context, "isnetwork", true);
                } else {
                    if (SharedHelper.getOnlineStatus(context, "isnetwork")) {
                        SharedHelper.putOnline(context, "isnetwork", false);
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        ref.child("drivers_data").child(SharedHelper.getKey(context, "userid")).child("online_status").setValue("1");
                    }
                }
            }
            if (notificationManager != null) {
                cancelNotification(context, 100);
            }

        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Notification notification = new NotificationCompat.Builder(this, Appcontroller.CHANNEL_ID).setAutoCancel(true)
                .build();
        startForeground(1, notification);

        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        mLocationRequest.setInterval(Constants.SET_INTERVAL);
        mLocationRequest.setFastestInterval(Constants.SET_FASTESTINTERVAL);


        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes


        mLocationRequest.setPriority(priority);
        mLocationClient.connect();

        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        RemoveListener();
        Intent myIntent = new Intent(this, TripRquestService.class);

        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 0, myIntent, 0);

        AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();

        calendar.setTimeInMillis(System.currentTimeMillis());

        calendar.add(Calendar.HOUR, 6);

        alarmManager1.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

    }

    public void getRequest() {

        if (!strDriverID.isEmpty()) {
            RequestDatabase = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(strDriverID).child("request").child("status");
            RequestValueEvent = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("1") && Constants.RquestScreen) {
                            CommonData.RequestBoolean = false;
                            CommonData.isRequestFrom = false;
                            Constants.RequestStart = false;
                            System.out.println("enter the request");
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);


                        } else {
                            System.out.println("enter the no request");
                        }

                        if (dataSnapshot.getValue().toString().equalsIgnoreCase("0")) {
                            CommonData.RequestBoolean = false;
                            CommonData.isRequestFrom = true;
                            Constants.RequestStart = true;
                            CommonData.requestionInprogress = false;
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
            RequestDatabase.addValueEventListener(RequestValueEvent);

        }

    }

    public void getCheckLogout() {


        if (!strDriverID.isEmpty()) {
            AppAutoLoggedOut = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(strDriverID).child("FCM_id");
            AppAutoLoggedOutValue = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        if (!dataSnapshot.getValue().toString().equalsIgnoreCase(SharedHelper.getToken(context, "device_token"))) {
                            /* if(!Constants.RquestScreen){*/
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            ref.child("drivers_data").child(strDriverID).child("online_status").setValue("0");
                            Intent intent = new Intent(context, WelcomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            SharedHelper.clearSharedPreferences(context);
                            /*}else {
                                SharedHelper.clearSharedPreferences(context);
                            }*/
                            strDriverID = "";
                            RemoveListener();
                            stopForeground(true);
                            stopSelf();
                        } else {
                            System.out.println("enter same fcm id");
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
            AppAutoLoggedOut.addValueEventListener(AppAutoLoggedOutValue);

        }

    }

    public void RemoveListener() {
        try {
            if (RequestDatabase != null) {
                RequestDatabase.removeEventListener(RequestValueEvent);
            }
            if (AppAutoLoggedOut != null) {
                AppAutoLoggedOut.removeEventListener(AppAutoLoggedOutValue);
            }
            if (mLocationClient != null && mLocationClient.isConnected()) {
                mLocationClient.disconnect();
            }
            if (subscription != null) {
                subscription.unsubscribe();
                subscription = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if (starLocation == null) {
            starLocation = location;
            EndLocation = location;
        }
        if (CommonData.strDistanceBegin != null) {

            if (CommonData.strDistanceBegin.matches("distancebegin")) {
                if (CommonData.lStart == null) {
                    CommonData.lStart = location;
                    CommonData.lEnd = location;
                } else
                    CommonData.lEnd = location;

                //Calling the method below updates the  live values of distance and speed to the TextViews.
                updateUI();
                //calculating the speed with getSpeed method it returns speed in m/s so we are converting it into kmph
                CommonData.speed = location.getSpeed() * 18 / 5;


            }
        }
        updateLcation = new LatLng(location.getLatitude(), location.getLongitude());
        System.out.println("enter the address online" + SharedHelper.getOnlineStatus(context, "onlineStatus"));
        if (location.getBearing() != 0.0)
            previousBearing = location.getBearing();
        upladteLocation(location);
        double distanceInKiloMeters = (starLocation).distanceTo(EndLocation) / 1000; // as distance is in meter
        if (distanceInKiloMeters >= 0.2 || CommonData.isFistTime) {
            CommonData.isFistTime = false;
            EndLocation = starLocation;
            if (SharedHelper.getOnlineStatus(context, "onlineStatus")) {
                upDateLocationPresnter("1", new LatLng(location.getLatitude(), location.getLongitude()));

            }
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            Log.d(TAG, "== Error On onConnected() Permission not granted");
            //Permission not granted by user so cancel the further execution.

            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);

        Log.d(TAG, "Connected to Google API");

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void upDateLocationPresnter(String status, LatLng latLng) {
        HashMap<String, String> map = new HashMap<>();
        map.put("lat", String.valueOf(latLng.latitude));
        map.put("lon", String.valueOf(latLng.longitude));
        map.put("status", status);
        FeedBackPresenter feedBackPresenter = new FeedBackPresenter();
        feedBackPresenter.UpdateLocation(map, context);
    }

    public void upladteLocation(Location location) {
        System.out.println("enter the category"+SharedHelper.getKey(context, "category") );
        if (SharedHelper.getKey(context, "category") != null && !SharedHelper.getKey(context, "category").isEmpty()) {
            if (SharedHelper.getKey(context, "trip_id") == null || SharedHelper.getKey(context, "trip_id").equalsIgnoreCase("null") || SharedHelper.getKey(context, "trip_id").isEmpty()){
                geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child(SharedHelper.getKey(context, "category").toLowerCase()));
            }else {
                geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child("trip_location"));

            }
            if (strDriverID != null && !strDriverID.equalsIgnoreCase("null")) {
                mCurrentLocation = location;
                if (mCurrentLocation != null) {
                    if (SharedHelper.getOnlineStatus(context, "onlineStatus")) {
                        System.out.println("location which is updated in==>" + mCurrentLocation.getLatitude() + "<====>" + mCurrentLocation.getLongitude());
                        //this.geoFire.offlineLocation(driverId, new GeoLocation(0.0, 0.0), new GeoFire.CompletionListener() {
                        geoFire.setLocation(strDriverID, new GeoLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), previousBearing, (key, error) -> {
                            if (error != null) {
                                System.err.println("There was an error saving the location to GeoFire: " + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());
                            } else {
                                System.out.println("OnlineOflline Location saved on server successfully!");
                            }
                        });
                    } else {

                        geoFire.offlineLocation(strDriverID, new GeoLocation(0.0, 0.0), new GeoFire.CompletionListener() {
                            @Override
                            public void onComplete(String key, DatabaseError error) {
                                if (error != null) {
                                    System.err.println("There was an error saving the location to GeoFire: " + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());
                                } else {
                                    System.out.println("Offline Location saved on server successfully!");
                                }
                            }
                        });
                    }
                }
            }
        }
    }

    private void sendNotification(String messageBody) {
        int color = getResources().getColor(R.color.white);
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(messageBody)
                        .setAutoCancel(false)
                        .setOngoing(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }
        notificationBuilder.setColor(color);
        notificationManager.notify(100 /* ID of notification */, notificationBuilder.build());
    }

    public void cancelNotification(Context ctx, int notifyId) {
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancel(notifyId);
        notificationManager = null;
    }

    private void addAutoStartup() {
        try {

            String manufacturer = android.os.Build.MANUFACTURER;
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("brand").child(manufacturer).setValue("0");

          if ("oppo".equalsIgnoreCase(manufacturer)) {
                initOPPO();
            } else {
              for (Intent intents : POWERMANAGER_INTENTS)
                  if (getPackageManager().resolveActivity(intents, PackageManager.MATCH_DEFAULT_ONLY) != null) {
                      // show dialog to ask user action
                      startActivity(intents);
                      break;
                  }

          }


        } catch (Exception e) {
            Log.e("exc", String.valueOf(e));
        }
    }

    private void initOPPO() {
        try {

            Intent i = new Intent(Intent.ACTION_MAIN);
            i.setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.floatwindow.FloatWindowListActivity"));
            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();
            try {

                Intent intent = new Intent("action.coloros.safecenter.FloatWindowListActivity");
                intent.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.floatwindow.FloatWindowListActivity"));
                startActivity(intent);
            } catch (Exception ee) {

                ee.printStackTrace();
                try {

                    Intent i = new Intent("com.coloros.safecenter");
                    i.setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.sysfloatwindow.FloatWindowListActivity"));
                    startActivity(i);
                } catch (Exception e1) {

                    e1.printStackTrace();
                }
            }

        }
    }

    private void updateUI() {
        if (CommonData.p == 0) {

            double maxAccuracy = CommonData.speed * 2;
            double minAccuracy = 25;
            if (maxAccuracy < minAccuracy) {
                maxAccuracy = minAccuracy;
            }
            if (CommonData.lStart != null && CommonData.lEnd.getAccuracy() < maxAccuracy
                    && CommonData.lEnd.getTime() > CommonData.lStart.getTime()) {
                if (CommonData.speed > 0.0) {

                    CommonData.distance = CommonData.distance + (CommonData.lStart.distanceTo(CommonData.lEnd) * 1.04 / 1000.00);
                    CommonData.lStart = CommonData.lEnd;
                    if (CommonData.stopWatch.isRunning()) {
                        CommonData.stopWatch.pause();
                    }
                } else {
                    if (!CommonData.stopWatch.isRunning()) {
                        CommonData.stopWatch.resume();
                    }


                }
            }

            if (CommonData.distance > 0) {
                CommonData.strTotalDistance = new DecimalFormat("0.0##").format(CommonData.distance);
                System.out.println("TOTAL DISTANCE+++>" + CommonData.strTotalDistance);
                //SavePref.saveInt(context,"TotalDistance", strDistance);
                System.out.println("Distance in shared preference==>in mappppp" + CommonData.strTotalDistance);
            } else {
                CommonData.strTotalDistance = String.valueOf(CommonData.distance);
            }
        }
    }

    public void ReinitializeTimer() {
        if (CommonData.stopWatch == null) {
            CommonData.stopWatch = new Stopwatch();
            CommonData.stopWatch.setListener(this);
        }
    }

    @Override
    public void onTick(String time) {

    }

    private static final Intent[] POWERMANAGER_INTENTS = {
            new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
            new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.startupmgr.ui.StartupNormalAppListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerUsageModelActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerSaverModeActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.oppoguardelf", "com.coloros.powermanager.fuelgaue.PowerConsumptionActivity")),
            new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
            new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
            new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
            new Intent().setComponent(new ComponentName("com.samsung.android.lool", "com.samsung.android.sm.ui.battery.BatteryActivity")),
            new Intent().setComponent(new ComponentName("com.htc.pitroad", "com.htc.pitroad.landingpage.activity.LandingPageActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.autostart.AutoStartActivity")),
            new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.MainActivity")),
            new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"))
            .setData(android.net.Uri.parse("mobilemanager://function/entry/AutoStart")),
            new Intent().setComponent(new ComponentName("com.meizu.safe", "com.meizu.safe.security.SHOW_APPSEC")).addCategory(Intent.CATEGORY_DEFAULT).putExtra("packageName", BuildConfig.APPLICATION_ID)

    };

}
