package com.auezeride.driver.OTP;

/**
 * Created by com on 25-Jul-18.
 */

public interface OtpListener {
    void onOtpEntered(String otp);
}
