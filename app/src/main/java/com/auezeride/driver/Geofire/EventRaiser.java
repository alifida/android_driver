package com.auezeride.driver.Geofire;

interface EventRaiser {
    void raiseEvent(Runnable r);
}
