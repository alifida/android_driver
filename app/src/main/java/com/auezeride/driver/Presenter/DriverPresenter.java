package com.auezeride.driver.Presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.Model.OnlineOflline;
import com.auezeride.driver.Model.UpdateVehicleModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.DriverView;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DriverPresenter {
    public RetrofitGenerator retrofitGenerator = null,retrofitGeneratorvehicle=null,OnlineOffline =null;
    public DriverView driverView;

    public DriverPresenter(DriverView driverView) {
        this.driverView = driverView;

    }

    public void getVehcleList(String driver_id, final AppCompatActivity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGeneratorvehicle == null) {
                Utiles.ShowLoader(activity);
                retrofitGeneratorvehicle = new RetrofitGenerator();
                ApiInterface service = retrofitGeneratorvehicle.getRetrofitUrl().create(ApiInterface.class);
                Call<List<ListVehicleModel>> call = service.getVehicleList( SharedHelper.getKey(activity.getApplicationContext(), "token"), driver_id);
                call.enqueue(new Callback<List<ListVehicleModel>>() {
                    @Override
                    public void onResponse(Call<List<ListVehicleModel>> call, Response<List<ListVehicleModel>> response) {
                        Utiles.DismissLoader();
                        retrofitGeneratorvehicle = null;
                        if(driverView!=null){
                            if (response.isSuccessful() && response.body() != null) {
                                driverView.VehicleListsuccessFully(response);
                            } else {
                                driverView.VehicleListFailure(response);
                            }
                        }

                    }

                    @Override
                    public void onFailure(Call<List<ListVehicleModel>> call, Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        retrofitGeneratorvehicle = null;
                    }
                });

            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }

    public void getOnlineOffline(final String status, final AppCompatActivity activity, Context context, Boolean isfrom) {
        if (Utiles.isNetworkAvailable(context)) {
            if (OnlineOffline == null) {
                if(isfrom){
                    Utiles.ShowLoader(activity);
                }

                OnlineOffline = new RetrofitGenerator();
                ApiInterface service = OnlineOffline.getRetrofitUrl().create(ApiInterface.class);
                Call<OnlineOflline> call = service.Onlineoffline( SharedHelper.getKey(context, "token"), status);
                call.enqueue(new Callback<OnlineOflline>() {
                    @Override
                    public void onResponse(Call<OnlineOflline> call, Response<OnlineOflline> response) {
                        Utiles.DismissLoader();
                        OnlineOffline = null;
                        if(driverView!=null){
                            if (response.isSuccessful() && response.body() != null) {
                                driverView.OnlineSuccess(response, status);

                            } else {
                                driverView.OnlineFailed(response, status);
                            }
                        }


                    }

                    @Override
                    public void onFailure(Call<OnlineOflline> call, Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        OnlineOffline = null;
                    }
                });
            }

        } else {
            if(isfrom){
                Utiles.showNoNetwork(activity);
            }

        }



    }

    public void UpdateVicle(HashMap<String, String> map, final AppCompatActivity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<UpdateVehicleModel> call = service.UpdateVehicle( SharedHelper.getKey(activity.getApplicationContext(), "token"), map);
                call.enqueue(new Callback<UpdateVehicleModel>() {
                    @Override
                    public void onResponse(@NonNull Call<UpdateVehicleModel> call, @NonNull Response<UpdateVehicleModel> response) {
                        retrofitGenerator = null;
                        if (response.isSuccessful()) {
                            Utiles.CommonToast(activity, response.body().getMessage());
                        } else {
                            Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<UpdateVehicleModel> call, @NonNull Throwable t) {
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
            retrofitGenerator = null;
        }

    }

    public void UpdateLocation(HashMap<String, String> map, final AppCompatActivity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ResponseBody> call = service.UpdateLocation( SharedHelper.getKey(activity.getApplicationContext(), "token"), map);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        retrofitGenerator = null;
                        if (response.isSuccessful()) {
                            Log.e("tag", "Succcess update location");
                        } else {
                            Log.e("tag", "Succcess failed to location");
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("tag", "Succcess Failure location");
                        retrofitGenerator = null;
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }
}
