package com.auezeride.driver.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.AcceptRequestModel;
import com.auezeride.driver.Model.DiclineRequest;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.RequestView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RequestPresenter {

    RequestView requestView;
    RetrofitGenerator retrofitGenerator = null;

    public RequestPresenter(RequestView requestView) {
        this.requestView = requestView;

    }

    public void AcceptRequest(String Request_id, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AcceptRequestModel> call = service.getAcceptRequest( SharedHelper.getKey(activity.getApplicationContext(), "token"), Request_id);
                call.enqueue(new Callback<AcceptRequestModel>() {
                    @Override
                    public void onResponse(@NonNull Call<AcceptRequestModel> call, @NonNull Response<AcceptRequestModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            requestView.OnSuccessAccept(response);
                        } else {
                            requestView.onFailureAccept(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<AcceptRequestModel> call,@NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }
        } else {
            Utiles.showNoNetwork(activity);
        }


    }
    public void SchudeleAcceptRequest(String Request_id,String datetime, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AcceptRequestModel> call = service.getScheduleAcceptRequest( SharedHelper.getKey(activity.getApplicationContext(), "token"), Request_id,datetime);
                call.enqueue(new Callback<AcceptRequestModel>() {
                    @Override
                    public void onResponse(@NonNull Call<AcceptRequestModel> call,@NonNull Response<AcceptRequestModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            requestView.OnScheduleSuccessAccept(response);
                        } else {
                            requestView.onScheduleFailureAccept(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<AcceptRequestModel> call,@NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }
        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void DiclineRequest(String Request_id, final Activity activity) {
        if (retrofitGenerator == null) {
            Utiles.ShowLoader(activity);
            retrofitGenerator = new RetrofitGenerator();
            ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
            Call<DiclineRequest> call = service.getDiclineRequest( SharedHelper.getKey(activity.getApplicationContext(), "token"), Request_id);
            call.enqueue(new Callback<DiclineRequest>() {
                @Override
                public void onResponse(@NonNull Call<DiclineRequest> call,@NonNull Response<DiclineRequest> response) {
                    Utiles.DismissLoader();
                    retrofitGenerator = null;
                    if (response.isSuccessful() && response.body() != null) {
                        requestView.onSuccessDicline(response);
                    } else {
                        requestView.onFailureDicline(response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DiclineRequest> call,@NonNull  Throwable t) {
                    Utiles.DismissLoader();
                    retrofitGenerator = null;
                    System.out.println("enter the request"+t.getMessage());
                    Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                }
            });


        }
    }
}
