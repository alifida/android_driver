package com.auezeride.driver.Presenter;

import android.app.Activity;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.DriverProfileModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.ProfileView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DriverProfilePresenter {
    public RetrofitGenerator retrofitGenerator = null;

    public ProfileView profileView;

    public DriverProfilePresenter(ProfileView profileView) {
        this.profileView = profileView;

    }

    public void getProfile(final Activity activity, boolean status) {

        if (Utiles.isNetworkAvailable(activity)) {
            if(status){
                Utiles.ShowLoader(activity);
            }
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<List<DriverProfileModel>> call = service.getProfile( SharedHelper.getKey(activity.getApplicationContext(), "token"));
                call.enqueue(new Callback<List<DriverProfileModel>>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onResponse(Call<List<DriverProfileModel>> call, Response<List<DriverProfileModel>> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            Log.e("resuestrul",""+call.request().url());
                            profileView.OnSuccessfully(response);
                            Log.e("TAG", "response 33: "+new Gson().toJson(response.body()) );


                        } else {
                            profileView.OnFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<DriverProfileModel>> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }


}
