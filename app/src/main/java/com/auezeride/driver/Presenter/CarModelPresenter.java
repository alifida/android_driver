package com.auezeride.driver.Presenter;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.util.Log;

import com.google.gson.Gson;

import java.util.HashMap;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.AddVehicleModel;
import com.auezeride.driver.Model.CarModel;
import com.auezeride.driver.Model.ServiceModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.CarModelView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CarModelPresenter {

    private RetrofitGenerator retrofitGenerator = null, retrofitGeneratorservictye = null;
    private CarModelView carModelView;
    private CompositeDisposable disposable;

    public CarModelPresenter(CarModelView carModelView, CompositeDisposable disposable) {
        this.carModelView = carModelView;
        this.disposable = disposable;

    }

    public void getCarModel(final Activity activity) {

        if (Utiles.isNetworkAvailable(activity)) {
            Utiles.ShowLoader(activity);
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<CarModel> call = service.getCarModel(SharedHelper.getKey(activity.getApplicationContext(), "token"));
                call.enqueue(new Callback<CarModel>() {
                    @Override
                    public void onResponse(@NonNull Call<CarModel> call, @NonNull Response<CarModel> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            carModelView.OnSuccessfullsy(response);
                        } else {
                            carModelView.OnFailurse(response);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<CarModel> call,@NonNull Throwable t) {
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                    }
                });

            }
        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void getAddVehicle(final Activity activity, HashMap<String, String> data) {
        Log.e("tag", "entertheaddvihicle" + data);
        if (Utiles.isNetworkAvailable(activity)) {
            Utiles.ShowLoader(activity);
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AddVehicleModel> call = service.AddVehicle(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<AddVehicleModel>() {
                    @Override
                    public void onResponse(@NonNull Call<AddVehicleModel> call,@NonNull Response<AddVehicleModel> response) {
                        Log.e("TAG", "response:addvehicle " + new Gson().toJson(response.body()));
                        retrofitGenerator =null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            carModelView.AddvehicleSucessfully(response);
                        } else {
                            carModelView.AddvehicleFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<AddVehicleModel> call,@NonNull Throwable t) {
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        Utiles.DismissLoader();
                        retrofitGenerator =null;
                    }
                });
            }
        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void geteditVehicle(final Activity activity, HashMap<String, String> data) {

        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AddVehicleModel> call = service.editVehicle(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<AddVehicleModel>() {
                    @Override
                    public void onResponse(@NonNull Call<AddVehicleModel> call,@NonNull Response<AddVehicleModel> response) {
                        Log.e("TAG", "response:editvehicle " + new Gson().toJson(response.body()));
                        retrofitGenerator =null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            carModelView.editVehicleSuccessfull();
                        } else {
                            carModelView.editVehicleFauiler();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<AddVehicleModel> call,@NonNull Throwable t) {
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        Utiles.DismissLoader();
                        retrofitGenerator =null;
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void getservicetype(final Activity activity) {

        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGeneratorservictye == null) {
                Utiles.ShowLoader(activity);
                retrofitGeneratorservictye = new RetrofitGenerator();
                ApiInterface service = retrofitGeneratorservictye.getRxJavaRetrofit().create(ApiInterface.class);
                disposable.add(service.getServiceType(SharedHelper.getKey(activity.getApplicationContext(), "token")).subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe(this::OnsucessFull, this::OnError));


            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    private void OnsucessFull(ServiceModel serviceModels) {
        retrofitGeneratorservictye = null;
        carModelView.onSuccessServiceList(serviceModels);
    }

    private void OnError(Throwable throwable) {
        retrofitGeneratorservictye = null;
        carModelView.onFailureService(throwable);
        System.out.println("enter the  error message" + throwable.getMessage());
    }
}
