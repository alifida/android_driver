package com.auezeride.driver.Presenter;


import com.google.android.gms.maps.model.LatLng;
import com.auezeride.driver.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;


import com.auezeride.driver.CommonClass.Constants;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by com on 18-May-18.
 */

public class GoogleGeocoderPresenter {
    public RetrofitGenerator retrofitGenerator = null;

    private CompositeDisposable disposable;
    private GoogleGeoCoderView googleGeoCoderView;

    public GoogleGeocoderPresenter(GoogleGeoCoderView googleGeoCoderView, CompositeDisposable disposable) {
        this.googleGeoCoderView = googleGeoCoderView;
        this.disposable = disposable;
    }

    public void getAddressFromLocation(LatLng latLng) {

        if (retrofitGenerator == null) {
            retrofitGenerator = new RetrofitGenerator();
            ApiInterface service = retrofitGenerator.getRx2JavaRetrofit().create(ApiInterface.class);
            disposable.add(service.getAddressFromLocation(latLng.latitude + "," + latLng.longitude, Constants.GoogleDirectionApi)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(this::OnsucessFull, this::OnError));

        }
    }

    private void OnsucessFull(GeocoderModel geocoderModel) {
        retrofitGenerator = null;
        googleGeoCoderView.geocoderOnSucessful(geocoderModel);
    }

    private void OnError(Throwable throwable) {
        retrofitGenerator = null;
        googleGeoCoderView.geocoderOnFailure(throwable);
    }

    public interface GoogleGeoCoderView {

        void geocoderOnSucessful(GeocoderModel geocoderModel);

        void geocoderOnFailure(Throwable throwable);


    }
}
