package com.auezeride.driver.Presenter;

import androidx.annotation.NonNull;
import android.app.Activity;
import android.util.Log;
import com.google.gson.Gson;
import java.util.HashMap;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.OTPModel;
import com.auezeride.driver.Model.RegisterModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.RegisterView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {
    private RetrofitGenerator retrofitGenerator = null;
    public RegisterView registerView;

    public RegisterPresenter(RegisterView registerView) {
        this.registerView = registerView;
    }

    public void getRegisterApi(HashMap<String, String> data, Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<RegisterModel> call = service.getRegister(data);
                call.enqueue(new Callback<RegisterModel>() {
                    @Override
                    public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Log.e("url_register", "" + call.request().url() + "\n" + new Gson().toJson(response.body()));
                        if (response.isSuccessful() && response.body() != null) {
                            registerView.RegisterView(response);

                        } else {
                            if (response.errorBody() != null) {
                                registerView.Errorlogview(response);
                            }

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<RegisterModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        System.out.printf("enter the error response " + t.getMessage());


                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void getOTP(String data, String email_id, String cc, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<OTPModel> call = service.getOpt(data, email_id, cc);
                call.enqueue(new Callback<OTPModel>() {
                    @Override
                    public void onResponse(@NonNull Call<OTPModel> call, @NonNull Response<OTPModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            registerView.onSuccessOTP(response);
                            retrofitGenerator = null;

                        } else {
                            if (response.errorBody() != null) {
                                registerView.onFailureOTP(response);
                                retrofitGenerator = null;
                            }

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<OTPModel> call, Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        retrofitGenerator = null;
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }
    }

}
