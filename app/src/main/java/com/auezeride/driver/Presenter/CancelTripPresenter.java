package com.auezeride.driver.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.CancelTripModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.CancelView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CancelTripPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public CancelView cancelView = null;

    public CancelTripPresenter(CancelView cancelView) {
        this.cancelView = cancelView;
    }

    public void CancelTrip(String Tripid, final Activity activity, String reason) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<CancelTripModel> call = service.CancelTrip(SharedHelper.getKey(activity.getApplicationContext(), "token"), Tripid, reason);
                call.enqueue(new Callback<CancelTripModel>() {
                    @Override
                    public void onResponse(@NonNull Call<CancelTripModel> call, @NonNull Response<CancelTripModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            cancelView.OnSuccessfully(response);

                        } else {
                            cancelView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(Call<CancelTripModel> call, Throwable t) {
                        Utiles.DismissLoader();
                        System.out.println("enter the cancel request error" + t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }
}
