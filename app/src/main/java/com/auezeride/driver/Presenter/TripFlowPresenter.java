package com.auezeride.driver.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import java.util.HashMap;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.TripFlowModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.TripFlowView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TripFlowPresenter {

    public RetrofitGenerator retrofitGenerator = null;
    public TripFlowView tripFlowView = null;

    public TripFlowPresenter(TripFlowView tripFlowView) {
        this.tripFlowView = tripFlowView;
    }

    public void TripFlowStatusApi(HashMap<String, String> data, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                //   Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<TripFlowModel> call = service.TripFlowStatus(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<TripFlowModel>() {
                    @Override
                    public void onResponse(@NonNull Call<TripFlowModel> call, @NonNull Response<TripFlowModel> response) {
                        //  Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            tripFlowView.OnSuccessfully(response);

                        } else {
                            tripFlowView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(Call<TripFlowModel> call, Throwable t) {
                        //  Utiles.DismissLoader();
                        System.out.println("enter the error" + t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }
}
