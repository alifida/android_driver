package com.auezeride.driver.Presenter;

import android.app.Activity;
import android.content.Context;

import java.util.HashMap;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.ChangePasswordModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;
import com.auezeride.driver.View.ChangePasswordView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public ChangePasswordView changePasswordView;
    public ChangePasswordPresenter(ChangePasswordView changePasswordView) {
        this.changePasswordView = changePasswordView;

    }
    public void ChangePasswor(HashMap<String, String> data, final Activity activity, Context context){
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ChangePasswordModel> call = service.ChangePassword( SharedHelper.getKey(context,"token"),data);
                call.enqueue(new Callback<ChangePasswordModel>() {
                    @Override
                    public void onResponse(Call<ChangePasswordModel> call, Response<ChangePasswordModel> response) {
                        Utiles.DismissLoader();
                        if(response.isSuccessful() && response.body()!=null){
                            changePasswordView.OnSuccessfully(response);

                        }else {
                            changePasswordView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(Call<ChangePasswordModel> call, Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }



    }
}
