package com.auezeride.driver.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.OTPVerificationModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayoutPresenter {
    private PayoutView payoutView;
    private Activity activity;
    private RetrofitGenerator retrofitGenerator = null;

    public PayoutPresenter(Activity activity, PayoutView payoutView) {
        this.activity = activity;
        this.payoutView = payoutView;

    }

    public void submitpayment(HashMap<String, String> data) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<OTPVerificationModel> call = service.getSentPayoutRequest(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<OTPVerificationModel>() {
                    @Override
                    public void onResponse(@NonNull Call<OTPVerificationModel> call, @NonNull Response<OTPVerificationModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            payoutView.onSuccess(response);
                        } else {
                            if (response.errorBody() != null) {
                                payoutView.onFailure(response);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<OTPVerificationModel> call,@NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        System.out.println("enter the rating error" + t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }

    public interface PayoutView {

        void onSuccess(Response<OTPVerificationModel> Response);

        void onFailure(Response<OTPVerificationModel> Response);
    }
}