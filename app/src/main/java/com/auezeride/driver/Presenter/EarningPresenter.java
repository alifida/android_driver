package com.auezeride.driver.Presenter;

import androidx.annotation.NonNull;

import android.app.Activity;
import android.util.Log;

import java.util.HashMap;
import java.util.List;

import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.EarningModel;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.ApiInterface;
import com.auezeride.driver.Retrofit.RetrofitGenerator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EarningPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    EarningViews tripDetailsView;
   Activity activity;

    public EarningPresenter(EarningViews tripDetailsView, Activity activity) {
        this.tripDetailsView = tripDetailsView;
        this.activity = activity;
    }

    public void getEanings(HashMap<String,String> map) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<List<EarningModel>> call = service.getEaringList(SharedHelper.getKey(activity.getApplicationContext(), "token"),map);
                call.enqueue(new Callback<List<EarningModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<EarningModel>> call, @NonNull Response<List<EarningModel>> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            tripDetailsView.Onsuccess(response);
                        } else {
                            tripDetailsView.onFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<List<EarningModel>> call, @NonNull Throwable t) {
                        Log.e("tag", "trip history api exception" + t.getMessage());
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }
        } else {
            Utiles.showNoNetwork(activity);
        }



    }

    public interface EarningViews{
        void Onsuccess(Response<List<EarningModel>> Response);

        void onFailure(Response<List<EarningModel>> Response);
    }

}
