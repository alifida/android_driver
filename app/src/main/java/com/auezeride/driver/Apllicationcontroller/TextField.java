package com.auezeride.driver.Apllicationcontroller;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.auezeride.driver.R;


/**
 * Created by chris on 17/03/15.
 * For Calligraphy.
 */
public class TextField extends AppCompatTextView {

    public TextField(final Context context, final AttributeSet attrs) {
        super(context, attrs, R.attr.textFieldStyle);
    }

}
