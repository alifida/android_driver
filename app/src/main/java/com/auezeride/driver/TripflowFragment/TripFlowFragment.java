package com.auezeride.driver.TripflowFragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.CustomizeDialog.OTPDialog;
import com.auezeride.driver.CustomizeDialog.RiderProfile;
import com.auezeride.driver.EventBus.DestinationAddressEvent;
import com.auezeride.driver.FlowInterface.RequestInterface;
import com.auezeride.driver.MainActivity;
import com.auezeride.driver.Model.TripFlowModel;
import com.auezeride.driver.Presenter.TripFlowPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.TripFlowView;
import com.auezeride.driver.View.TripInterface;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.auezeride.driver.MainActivity.destLocation;
import static com.auezeride.driver.MainActivity.getCurrentTime;


@SuppressLint("ValidFragment")
public class TripFlowFragment extends BaseFragment implements TripFlowView, TripInterface {

    private RequestInterface requestInterface;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.trip_btn)
    Button tripBtn;
    @BindView(R.id.rider_detail_layout)
    LinearLayout riderDetailLayout;
    @BindView(R.id.navigation_layout)
    LinearLayout navigationLayout;
    Unbinder unbinder;
    @BindView(R.id.overall_layout)
    LinearLayout overallLayout;
    private String tripStatus, strTripStatus = "";
    private Location mCurrentLocation;
    private Double routeLat, routeLng;
    private RiderProfile riderDialog;
    private OTPDialog otpDialog;
    private String duration = "0";

    @SuppressLint("ValidFragment")
    public TripFlowFragment(String status, Location mCurrentLocation) {
        this.tripStatus = status;
        this.mCurrentLocation = mCurrentLocation;
    }

    public TripFlowFragment() {

    }

   Activity activity;
    Context context;

    Response<TripFlowModel> response = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_trip_flow, container, false);
        unbinder = ButterKnife.bind(this, view);
        address.setSelected(true);
        activity = getActivity();
        context = getContext();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        if (tripStatus != null) {
            switch (tripStatus) {
                case "1":
                    tripBtn.setText(activity.getResources().getString(R.string.tap_to_arrive));
                    strTripStatus = "Tap to arrive";
                    PresenterCall("1", "");
                    break;
                case "2":
                    tripBtn.setText(activity.getResources().getString(R.string.top_to_start));
                    strTripStatus = "Tap to Start";
                    PresenterCall("2", "");
                    break;
                case "3":
                    tripBtn.setText(activity.getResources().getString(R.string.tap_to_end));
                    PresenterCall("3", CommonData.driveAllovanceDis);
                    strTripStatus = "Tap to End";
                    CommonData.strDistanceBegin = "distancebegin";
                    // distance = 0;
                    CommonData.p = 0;

                    break;
                case "empty":
                    tripBtn.setText(activity.getResources().getString(R.string.tap_to_arrive));
                    strTripStatus = "Tap to arrive";
                    PresenterCall("1", "");
                    break;
            }
        }


        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        DialogDismiss(riderDialog);
        DialogDismiss(otpDialog);
        unbinder.unbind();
    }

    private void DialogDismiss(Dialog dialog) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.trip_btn, R.id.rider_detail_layout, R.id.navigation_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.trip_btn:
                switch (strTripStatus) {
                    case "Tap to arrive":
                        PresenterCall("2", "");
                        FirebaseTripStatus("2");
                        tripBtn.setText(activity.getResources().getString(R.string.top_to_start));
                        strTripStatus = "Tap to Start";
                        Constants.Previousstatus = "2";
                        CommonData.strDistanceBegin = "distancebegin";
                        CommonData.distance = 0;
                        CommonData.outsitedistance = 0;
                        CommonData.p = 0;
                        break;
                    case "Tap to Start":
                        if (response != null) {
                            assert response.body() != null;
                            ShowOTPDialog(true, response.body().getStartOTP());
                        }

                        break;
                    case "Tap to End":
                        //  PresenterCall("4");
                      /*  if (response != null) {
                            ShowOTPDialog(false, response.body().getEndOTP());
                        }*/
                        try {
                            String starttime = SharedHelper.getKey(context, "starttime");
                            long start = Long.parseLong(starttime);
                            duration = String.valueOf((((System.currentTimeMillis() - start) / 1000) / 60) % 60);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        DistaceUpdate();
                        PresenterCall("4", "");


                      /*  FirebaseTripStatus("4");
                        Constants.Previousstatus = "4";
                        stopWatch.pause();
                        try {
                            requestInterface = (RequestInterface) getActivity();
                            requestInterface.summaryFragment();
                        } catch (Exception e) {
                            Log.e("tag", "Eception of request screen" + e.getMessage());
                        }
                        tripBtn.setEnabled(false);*/
                        break;
                }
                break;
            case R.id.rider_detail_layout:
                riderDialog = new RiderProfile(activity, response);
                riderDialog.setCancelable(true);
                Objects.requireNonNull(riderDialog.getWindow()).getAttributes().windowAnimations = R.style.fate_in_and_fate_out;
                riderDialog.show();
                break;
            case R.id.navigation_layout:
                try {
                    if(destLocation!=null){
                        routeLat = destLocation.latitude;
                        routeLng = destLocation.longitude;
                        GoogleNavigation();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    public void GoogleNavigation() {
        if (!routeLat.isNaN() && !routeLng.isNaN()) {
            Uri gmmIntentUri = Uri.parse("google.navigation:q=" + routeLat + "," + routeLng);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            activity.startActivity(mapIntent);

        }
    }

    public void ShowOTPDialog(boolean status, String Otp) {
        otpDialog = new OTPDialog(activity, Otp, this, status);
        otpDialog.setCancelable(true);
        Objects.requireNonNull(otpDialog.getWindow()).getAttributes().windowAnimations = R.style.fate_in_and_fate_out;
        otpDialog.show();
    }

    @Override
    public void OnSuccessfully(Response<TripFlowModel> Response) {

        assert Response.body() != null;
        if (Response.body().getSuccess()) {
            this.response = Response;
            try {
                if (Constants.Previousstatus.equalsIgnoreCase("3")) {
                    address.setText(Response.body().getPickupdetails().getEnd());
                } else {
                    address.setText(Response.body().getPickupdetails().getStart());
                }
                requestInterface = (RequestInterface) getActivity();
                requestInterface.FlowDetails(Response);
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }

        } else {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void OnFailure(Response<TripFlowModel> Response) {
        try {
            assert Response.errorBody() != null;
            Utiles.showErrorMessage(Response.errorBody().string(),activity,getView());
        } catch (IOException e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }
    }

    public void PresenterCall(String Status, String distanceDriver) {
        try {
            HashMap<String, String> map = new HashMap<>();
            map.put("status", Status);
            map.put("tripId", SharedHelper.getKey(context, "trip_id"));

            if (Status.equalsIgnoreCase("3")) {
                map.put("allowanceDistance", distanceDriver);
                map.put("pickupLat", String.valueOf(MainActivity.mCurrentLocation.getLatitude()));
                map.put("pickupLng", String.valueOf(MainActivity.mCurrentLocation.getLongitude()));
                map.put("startTime", getCurrentTime());
                map.put("endAddress", "");
                map.put("endTime", "");
                map.put("dropLat", "");
                map.put("dropLng", "");
                map.put("distance", "");
                map.put("duration", "");
                map.put("waitingTime", "");
                map.put("fromAddress", getCompleteAddressString(MainActivity.mCurrentLocation.getLatitude(), MainActivity.mCurrentLocation.getLongitude()));
            } else if (Status.equalsIgnoreCase("4")) {
                map.put("pickupLat", "");
                map.put("pickupLng", "");
                map.put("startTime", "");
                map.put("dropLat", String.valueOf(MainActivity.mCurrentLocation.getLatitude()));
                map.put("dropLng", String.valueOf(MainActivity.mCurrentLocation.getLongitude()));
                map.put("endTime", getCurrentTime());
                map.put("fromAddress", "");
                map.put("endAddress", getCompleteAddressString(MainActivity.mCurrentLocation.getLatitude(), MainActivity.mCurrentLocation.getLongitude()));
                map.put("waitingSecond", String.valueOf(CommonData.stopWatch.getElapsedTimeSecs() + 60 * CommonData.stopWatch.getElapsedTimeMin() + 60 * 60 * CommonData.stopWatch.getElapsedTimeHour()));
                map.put("waitingTime", String.valueOf(CommonData.stopWatch.getElapsedTimeSecs() + 60 * CommonData.stopWatch.getElapsedTimeMin() + 60 * 60 * CommonData.stopWatch.getElapsedTimeHour()));
                map.put("distance", CommonData.strTotalDistance);
                map.put("duration", String.valueOf(duration));


            } else {
                map.put("pickupLat", "");
                map.put("pickupLng", "");
                map.put("startTime", "");
                map.put("endTime", "");
                map.put("fromAddress", "");
                map.put("dropLat", "");
                map.put("dropLng", "");
                map.put("distance", "");
                map.put("duration", "");
                map.put("waitingTime", "");
                map.put("endAddress", "");
            }
            TripFlowPresenter tripFlowPresenter = new TripFlowPresenter(this);
            tripFlowPresenter.TripFlowStatusApi(map, activity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void FirebaseTripStatus(String tripStatus) {
        DatabaseReference Accept = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        HashMap<String, Object> updatestatus = new HashMap<>();
        updatestatus.put("status", tripStatus);
        updatestatus.put("distance", CommonData.strTotalDistance);
        Accept.updateChildren(updatestatus);
    }
    private void DistaceUpdate() {
        DatabaseReference Accept = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        HashMap<String, Object> updatestatus = new HashMap<>();
        updatestatus.put("distance", CommonData.strTotalDistance);
        Accept.updateChildren(updatestatus);
    }
    @SuppressLint("LongLogTag")
    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder(" ");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void CheckTripStatus(boolean ischeck) {
        if (ischeck) {
            PresenterCall("3", String.valueOf(CommonData.distance));
            FirebaseTripStatus("3");
            SharedHelper.putKey(context, "starttime", String.valueOf(System.currentTimeMillis()));
            tripBtn.setText(activity.getResources().getString(R.string.tap_to_end));
            strTripStatus = "Tap to End";
            Constants.Previousstatus = "3";
            CommonData.strDistanceBegin = "distancebegin";
            CommonData.distance = 0;
            CommonData.startTime = 0;
            CommonData.currentTime = 0;
            CommonData.LastPastTime = 0;
            CommonData.p = 0;
            CommonData.stopWatch.start();
        } else {
            FirebaseTripStatus("4");
            Constants.Previousstatus = "4";
            try {
                requestInterface = (RequestInterface) getActivity();
                requestInterface.summaryFragment();
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }
            tripBtn.setEnabled(false);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessage(DestinationAddressEvent event) {
        if (Constants.Previousstatus.equalsIgnoreCase("3") && event.getAddress()!=null && !event.getAddress().isEmpty()) {
            address.setText(event.getAddress());
        }
        EventBus.getDefault().removeStickyEvent(DestinationAddressEvent.class); // don't forget to remove the sticky event if youre done with it
    }
}
