package com.auezeride.driver.TripflowFragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentManager;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.database.DataSnapshot;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Objects;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.ProgressWheel;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.FlowInterface.RequestInterface;
import com.auezeride.driver.Model.AcceptRequestModel;
import com.auezeride.driver.Model.DiclineRequest;
import com.auezeride.driver.Presenter.RequestPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.RequestView;
import com.auezeride.driver.CommonClass.CommonData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Response;


@SuppressLint("ValidFragment")
public class RequestFragement extends BaseFragment implements RequestView {


    @BindView(R.id.decline_request_txt)
    TextView declineRequestTxt;
    @BindView(R.id.map_view)
    ImageView mapView;

    @BindView(R.id.request_layout)
    LinearLayout requestLayout;
    @BindView(R.id.est_txt)
    TextView estTxt;
    @BindView(R.id.pickup_img)
    ImageView pickupImg;
    @BindView(R.id.pickup_address_txt)
    TextView pickupAddressTxt;
    @BindView(R.id.drop_img)
    ImageView dropImg;
    @BindView(R.id.destination_address_txt)
    TextView destinationAddressTxt;
    @BindView(R.id.accept_request_txt)
    Button acceptRequestTxt;
    Unbinder unbinder;
    @BindView(R.id.progressBarTwo)
    ProgressWheel progressBarTwo;

    private DataSnapshot requestDatasnapshot;
    @BindView(R.id.date_time_txt)
    TextView dateTimeTxt;
    @BindView(R.id.km_txt)
    TextView kmTxt;
    @BindView(R.id.fare_txt)
    TextView fareTxt;

    @SuppressLint("ValidFragment")
    public RequestFragement(DataSnapshot dataSnapshot) {
        this.requestDatasnapshot = dataSnapshot;
    }

    public RequestFragement() {

    }

    private boolean ScheduleRequest = false;
    private RequestInterface requestInterface;
    private Context context;
    private Activity activity;
    private MediaPlayer mySong;
    private FragmentManager fragmentManager;
    private Vibrator vibrator;
    private RequestPresenter requestPresenter;

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.request_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();
        fragmentManager = getFragmentManager();
        assert activity != null;
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));

        String mapBitmapString = SharedHelper.getKey(context, "mapImage");
        Bitmap mapBitmap = Utiles.StringToBitMap(mapBitmapString);
        if (mapBitmap != null) {

            setMapImage(mapBitmap);
        }
        progressBarTwo.setBarWidth(10);
        progressBarTwo.setRimWidth(10);
        progressBarTwo.setRimColor(Color.WHITE);
        progressBarTwo.resetCount();
        progressBarTwo.startSpinning();

        try {
            destinationAddressTxt.setText(IsNotNullable(requestDatasnapshot.child("drop_address").getValue()).toString());
            estTxt.setText(IsNotNullable(requestDatasnapshot.child("etd").getValue()).toString());
            pickupAddressTxt.setText(IsNotNullable(requestDatasnapshot.child("picku_address").getValue()).toString());
            kmTxt.setText(IsNotNullable(requestDatasnapshot.child("totalKM").getValue()).toString() + "KM");
            fareTxt.setText(getString(R.string.total_fares) + IsNotNullable(requestDatasnapshot.child("totalFare").getValue()).toString());

            if (requestDatasnapshot.child("request_type").getValue() != null) {
                if (Objects.requireNonNull(requestDatasnapshot.child("request_type").getValue()).toString().equalsIgnoreCase("rideLater")) {
                    ScheduleRequest = true;
                    dateTimeTxt.setVisibility(View.VISIBLE);
                    dateTimeTxt.setText(Objects.requireNonNull(requestDatasnapshot.child("datetime").getValue()).toString());
                } else {
                    ScheduleRequest = false;
                    dateTimeTxt.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        requestPresenter = new RequestPresenter(this);


        if (mySong != null && mySong.isPlaying()) {
            mySong.stop();
            mySong = null;
        }
        if (vibrator != null && vibrator.hasVibrator()) {
            vibrator.cancel();
            vibrator = null;
        } else {
            if (vibrator == null) {
                Vibration();
            }
        }
        if (mySong == null) {
            Ringtone();
        } else {
            if (!mySong.isPlaying()) {
                Ringtone();
            }

        }
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event

    public void Ringtone() {
        mySong = MediaPlayer.create(context, R.raw.requesting_tone);
        //  mySong.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mySong.setLooping(true);
        mySong.setVolume(1,1);
        mySong.start();
    }

    public void Vibration() {
        vibrator = (Vibrator) activity.getSystemService(Context.VIBRATOR_SERVICE);
        long[] pattern = {60, 120, 180, 240, 300, 360, 420, 480};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(pattern, 1));
        } else {

            vibrator.vibrate(pattern, 1);

        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonData.RequestBoolean = false;
        System.out.println("on detach method");
        if (mySong != null && mySong.isPlaying()) {
            mySong.stop();
        }
        if (vibrator != null && vibrator.hasVibrator()) {
            vibrator.cancel();
            vibrator = null;
        }
        Utiles.clearInstance();
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        CommonData.RequestBoolean = false;
        System.out.println("ondestoyed");
        unbinder.unbind();
        if (mySong != null && mySong.isPlaying()) {
            mySong.stop();
        }
        if (vibrator != null && vibrator.hasVibrator()) {
            vibrator.cancel();
            vibrator = null;
        }
        Utiles.clearInstance();
    }

    @Optional
    @OnClick({R.id.decline_request_txt, R.id.accept_request_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.decline_request_txt:
                requestPresenter.DiclineRequest(Objects.requireNonNull(requestDatasnapshot.child("request_id").getValue()).toString(), activity);
                break;
            case R.id.accept_request_txt:
         /*       if (ScheduleRequest) {
                    requestPresenter.SchudeleAcceptRequest(requestDatasnapshot.child("request_id").getValue().toString(), requestDatasnapshot.child("datetime").getValue().toString(), activity);
                } else {
                    requestPresenter.AcceptRequest(requestDatasnapshot.child("request_id").getValue().toString(), activity);
                }*/
                requestPresenter.AcceptRequest(Objects.requireNonNull(requestDatasnapshot.child("request_id").getValue()).toString(), activity);

                stopmusic();
                break;
        }
    }

    public void setMapImage(Bitmap mapImageBitmap) {

        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            mapImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Glide.with(context).load(stream.toByteArray()).asBitmap().centerCrop().skipMemoryCache(true).into(new BitmapImageViewTarget(mapView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    try {
                        mapView.setImageDrawable(circularBitmapDrawable);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopmusic();
    }


    void stopmusic() {
        //mySong.stop();

    }

    @Override
    public void OnSuccessAccept(Response<AcceptRequestModel> Response) {
        assert Response.body() != null;
        Utiles.CommonToast(activity, Response.body().getMessage());
        if(ScheduleRequest){
            try {

                assert Response.body() != null;
                CommonData.RequestBoolean = false;
                requestInterface = (RequestInterface) activity;
                requestInterface.ClearFragment();
                RemoveFragment();
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }
            stopmusic();
            Utiles.ClearFirebase(context);
        }else {
            SharedHelper.putKey(context, "trip_id", Response.body().getTripId());
            Utiles.CreateFirebaseTripData(Response.body().getTripId());
            try {
                requestInterface = (RequestInterface) activity;
                requestInterface.TripFragment();
                RemoveFragment();
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }
        }

    }

    @Override
    public void onFailureAccept(Response<AcceptRequestModel> Response) {
        try {
            assert Response.errorBody() != null;
            JSONObject errorjson = new JSONObject(Response.errorBody().string());
            if (errorjson.has("message")) {
                Utiles.displayMessage(getView(), context, errorjson.optString("message"));
            }
            CommonData.RequestBoolean = false;
            requestInterface = (RequestInterface) activity;
            requestInterface.ClearFragment();
            RemoveFragment();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        stopmusic();
        Utiles.ClearFirebase(context);
    }

    @Override
    public void onSuccessDicline(Response<DiclineRequest> Response) {
        try {
            CommonData.RequestBoolean = false;
            requestInterface = (RequestInterface) activity;
            requestInterface.ClearFragment();
            RemoveFragment();
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }
        stopmusic();
        Utiles.ClearFirebase(context);
    }

    @Override
    public void onFailureDicline(Response<DiclineRequest> Response) {
        try {
            CommonData.RequestBoolean = false;
            requestInterface = (RequestInterface) activity;
            requestInterface.ClearFragment();
            RemoveFragment();
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }
        stopmusic();
        Utiles.ClearFirebase(context);
    }

    @Override
    public void OnScheduleSuccessAccept(Response<AcceptRequestModel> Response) {
        try {

            assert Response.body() != null;
            Utiles.CommonToast(activity, Response.body().getMessage());
            CommonData.RequestBoolean = false;
            requestInterface = (RequestInterface) activity;
            requestInterface.ClearFragment();
            RemoveFragment();
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }
        stopmusic();
        Utiles.ClearFirebase(context);
    }

    @Override
    public void onScheduleFailureAccept(Response<AcceptRequestModel> Response) {
        try {
            assert Response.errorBody() != null;
            JSONObject errorjson = new JSONObject(Response.errorBody().string());
            if (errorjson.has("message")) {
                Utiles.displayMessage(getView(), context, errorjson.optString("message"));
            }
            CommonData.RequestBoolean = false;
            requestInterface = (RequestInterface) activity;
            requestInterface.ClearFragment();
            RemoveFragment();
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }
        stopmusic();
        Utiles.ClearFirebase(context);
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("enter the on pause method");
    }

    public void RemoveFragment(){
        try {
            fragmentManager.popBackStackImmediate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStop() {
        super.onStop();
        System.out.println("enter the on stop method");
    }

    public Object IsNotNullable(Object values) {
        if (values == null) {
            values = "";
        }

        return values;
    }

}

