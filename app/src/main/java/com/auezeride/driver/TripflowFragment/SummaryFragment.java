package com.auezeride.driver.TripflowFragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.FlowInterface.RequestInterface;
import com.auezeride.driver.MainActivity;
import com.auezeride.driver.Model.FareCaluationModel;
import com.auezeride.driver.Model.TripFlowModel;
import com.auezeride.driver.Presenter.FeedBackPresenter;
import com.auezeride.driver.Presenter.TripFlowPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.TripFlowView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


@SuppressLint("ValidFragment")
public class SummaryFragment extends BaseFragment implements TripFlowView {


    @BindView(R.id.total_amount_txt)
    TextView totalAmountTxt;
    @BindView(R.id.date_time_txt)
    TextView dateTimeTxt;
    @BindView(R.id.discount_amount_txt)
    TextView discountAmountTxt;
    @BindView(R.id.pickup_txt)
    TextView pickupTxt;
    @BindView(R.id.drop_address_txt)
    TextView dropAddressTxt;
    @BindView(R.id.driver_rating)
    RatingBar driverRating;
    @BindView(R.id.feedback_txt)
    EditText feedbackTxt;
    @BindView(R.id.submit_txt)
    Button submitTxt;
    Unbinder unbinder;
    @BindView(R.id.pickup_layout)
    LinearLayout pickupLayout;
    @BindView(R.id.taxi_layout)
    LinearLayout taxiLayout;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.waitings_layout)
    LinearLayout waitingLayout;
    @BindView(R.id.WaiingTimt_txt)
    TextView WaiingTimtTxt;
    @BindView(R.id.Waiting_fare_txt)
    TextView WaitingFareTxt;
    @BindView(R.id.bases_fare_txt)
    TextView basesFareTxt;

    private DatabaseReference databaseReference;
    private ValueEventListener valueEventListener;

    private Activity activity;
    private Context context;
    private RequestInterface requestInterface;
    private FareCaluationModel fareCaluationModel;
    private String status, strendAdrresss;
    @BindView(R.id.title_txt)
    TextView titleTxt;
    @BindView(R.id.base_fare_txt)
    TextView baseFareTxt;
    @BindView(R.id.distance_fare_txt)
    TextView distanceFareTxt;
    @BindView(R.id.time_fare_txt)
    TextView timeFareTxt;
    @BindView(R.id.balance_fare_txt)
    TextView balanceFareTxt;
    @BindView(R.id.payment_type_txt)
    TextView paymentTypeTxt;
    @BindView(R.id.detect_fare_txt)
    TextView detectFareTxt;
    @BindView(R.id.distance_txt)
    TextView distanceTxt;
    @BindView(R.id.time_txt)
    TextView timeTxt;
    @BindView(R.id.ordinary)
    LinearLayout ordinary;
    @BindView(R.id.cancellation_layout)
    LinearLayout cancellationLayout;
    @BindView(R.id.view_cancel)
    View viewCancel;
    @BindView(R.id.Ride_fare_txt)
    TextView RideFareTxt;
    @BindView(R.id.ride_fare_layout)
    LinearLayout rideFareLayout;
    @BindView(R.id.ride_view)
    View rideView;

    public SummaryFragment() {

    }


    @SuppressLint("ValidFragment")
    public SummaryFragment(FareCaluationModel fareCaluationModel, String status) {
        this.fareCaluationModel = fareCaluationModel;
        this.status = status;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        CommonData.strDistanceBegin = "totaldistancend";
        titleTxt.setSelected(true);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        if (status.equalsIgnoreCase("Api")) {
            strendAdrresss = getCompleteAddressString(fareCaluationModel.getmCurrentLocation().getLatitude(), fareCaluationModel.getmCurrentLocation().getLongitude());
            dropAddressTxt.setText(strendAdrresss);
            FirebaseTripStatus("Drop_address", strendAdrresss);
            FirebaseTripStatus("distance", fareCaluationModel.getDistance());
            FirebaseTripStatus("waitingTime", fareCaluationModel.getWaitingTime());
            PresenterCall("4");
            submitTxt.setEnabled(false);
            FareCalculation();
        } else {
            FareCalculation();
        }


        //get current Date and Time
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy hh:mm a"); // 12 hours formate
        String formattedDate = df.format(c.getTime());
        dateTimeTxt.setText(formattedDate);
        FirebaseTripStatus("datetime", formattedDate);

        return view;

    }

    public void PresenterCall(String Status) {
        distanceTxt.setText(dotLimitation(Double.parseDouble(fareCaluationModel.getDistance())) + " KM");
        timeTxt.setText(fareCaluationModel.getDuration() + " Min");
        HashMap<String, String> map = new HashMap<>();
        map.put("status", Status);
        map.put("tripId", SharedHelper.getKey(context, "trip_id"));
        map.put("distance", fareCaluationModel.getDistance());
        map.put("duration", fareCaluationModel.getDuration());
        map.put("dropLat", String.valueOf(fareCaluationModel.getmCurrentLocation().getLatitude()));
        map.put("dropLng", String.valueOf(fareCaluationModel.getmCurrentLocation().getLongitude()));
        map.put("pickupLat", "");
        map.put("pickupLng", "");
        map.put("endAddress", strendAdrresss);
        map.put("startTime", "");
        map.put("allowanceDistance", "");
        map.put("waitingTime", fareCaluationModel.getWaitingTime());
        map.put("endTime", MainActivity.getCurrentTime());
        map.put("fromAddress", "");
        TripFlowPresenter tripFlowPresenter = new TripFlowPresenter(this);
        tripFlowPresenter.TripFlowStatusApi(map, activity);
        System.out.println("enter the trip presenter" + map);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();

    }

    @OnClick(R.id.submit_txt)
    public void onViewClicked() {
        Utiles.hideKeyboard(activity);
        Utiles.ClearFirebase(context);
        sentFeedBack();
        CommonData.p = 1;
        CommonData.lStart = null;
        CommonData.lEnd = null;
        SharedHelper.putKey(context, "trip_id", "null");
        try {
            requestInterface = (RequestInterface) getActivity();
            requestInterface.ClearFragment();
            getFragmentManager().popBackStackImmediate();
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }

    }

    public void FirebaseTripStatus(String key, String value) {
        DatabaseReference Accept = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        HashMap<String, Object> updatestatus = new HashMap<>();
        updatestatus.put(key, value);
        Accept.updateChildren(updatestatus);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnSuccessfully(Response<TripFlowModel> Response) {
        System.out.println("enter json reponse" + new Gson().toJson(Response.body()));
        assert Response.body() != null;
        FirebaseTripStatus("convance_fare", Response.body().getFare().getPickupCharge());
        FirebaseTripStatus("total_fare", Response.body().getFare().getTotalFare());
        FirebaseTripStatus("time_fare", Response.body().getFare().getWaitingFare());
        FirebaseTripStatus("discount", Response.body().getFare().getDiscountAmt());
        FirebaseTripStatus("ispay", Response.body().getFare().getBalanceFare());
        if (Response.body().getFare().getApplyValues().getApplyNightCharge()) {
            if (Response.body().getFare().getNightObj().getIsApply()) {
                FirebaseTripStatus("isNight", Response.body().getFare().getNightObj().getAlertLable());
            } else {
                FirebaseTripStatus("isNight", "0");
            }
        } else {
            FirebaseTripStatus("isNight", "0");
        }
        if (Response.body().getFare().getApplyValues().getApplyPeakCharge()) {
            if (Response.body().getFare().getPeakObj().getIsApply()) {
                FirebaseTripStatus("isNight", Response.body().getFare().getPeakObj().getAlertLable());
            } else {
                FirebaseTripStatus("isNight", "0");
            }
        } else {
            FirebaseTripStatus("isNight", "0");
        }
        if (Response.body().getFare().getApplyValues().getApplyPickupCharge()) {
            FirebaseTripStatus("isPickup", "1");
        } else {
            FirebaseTripStatus("isPickup", "0");
        }
        if (Response.body().getFare().getApplyValues().getApplyWaitingTime()) {
            FirebaseTripStatus("isWaiting", "1");
        } else {
            FirebaseTripStatus("isWaiting", "0");
        }
        if (Response.body().getFare().getApplyValues().getApplyTax()) {
            FirebaseTripStatus("isTax", "1");
        } else {
            FirebaseTripStatus("isTax", "0");
        }
        FirebaseTripStatus("ispay", Response.body().getFare().getBalanceFare());
        FirebaseTripStatus("pay_type", Response.body().getFare().getPaymentMode());
        FirebaseTripStatus("tax", Response.body().getFare().getTax());
        FirebaseTripStatus("trip_type", Response.body().getFare().getFareType());
        FirebaseTripStatus("cancel_fare", Response.body().getFare().getOldCancellationAmt());
        FirebaseTripStatus("pickup_address", Response.body().getPickupdetails().getStart());
        FirebaseTripStatus("time", Response.body().getFare().getWaitingTime());


        if (Response.body().getFare().getFareType().equalsIgnoreCase("flatrate")) {
            ordinary.setVisibility(View.GONE);
            rideFareLayout.setVisibility(View.VISIBLE);
            rideView.setVisibility(View.VISIBLE);
            RideFareTxt.setText("$" + Response.body().getFare().getFlatFare());
            FirebaseTripStatus("distance_fare", Response.body().getFare().getFlatFare());
        } else {
            ordinary.setVisibility(View.VISIBLE);
            rideFareLayout.setVisibility(View.GONE);
            rideView.setVisibility(View.GONE);
            RideFareTxt.setText("$" + Response.body().getFare().getKMFare());
            FirebaseTripStatus("distance_fare", Response.body().getFare().getKMFare());
        }
        if (Response.body().getFare().getOldCancellationAmt().equalsIgnoreCase("0")) {
            cancellationLayout.setVisibility(View.GONE);
            viewCancel.setVisibility(View.GONE);


        } else {
            cancellationLayout.setVisibility(View.VISIBLE);
            viewCancel.setVisibility(View.VISIBLE);
        }
        try {
            totalAmountTxt.setText("$" + Response.body().getFare().getTotalFare());
            pickupTxt.setText(Response.body().getPickupdetails().getStart());
            submitTxt.setEnabled(true);
            baseFareTxt.setText("$" + Response.body().getFare().getPickupCharge());
            discountAmountTxt.setText("$" + Response.body().getFare().getTax());
            timeFareTxt.setText("$" + Response.body().getFare().getWaitingFare());

            distanceFareTxt.setText("$" + Response.body().getFare().getDistance());
            balanceFareTxt.setText("$" + Response.body().getFare().getOldCancellationAmt());
            paymentTypeTxt.setText(Response.body().getFare().getPaymentMode());
            detectFareTxt.setText("$" + Response.body().getFare().getTax());
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnFailure(Response<TripFlowModel> Response) {
        submitTxt.setEnabled(true);
        Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
    }

    @SuppressLint("LongLogTag")
    public String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    private void sentFeedBack() {
        HashMap<String, String> map = new HashMap<>();
        map.put("rating", String.valueOf(driverRating.getRating()));
        map.put("tripId", SharedHelper.getKey(context, "trip_id"));
        map.put("comments", feedbackTxt.getText().toString());
        FeedBackPresenter feedBackPresenter = new FeedBackPresenter();
        feedBackPresenter.getFeedBack(activity, map);

    }

    private void FareCalculation() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        valueEventListener = databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                try {
                    if (dataSnapshot.child("datetime").getValue() != null) {
                        dateTimeTxt.setText(dataSnapshot.child("datetime").getValue().toString());
                    }
                    if (dataSnapshot.child("pickup_address").getValue() != null) {
                        pickupTxt.setText(dataSnapshot.child("pickup_address").getValue().toString());
                    }
                    if (dataSnapshot.child("Drop_address").getValue() != null) {
                        dropAddressTxt.setText(dataSnapshot.child("Drop_address").getValue().toString());
                    }
                    if (dataSnapshot.child("total_fare").getValue() != null) {
                        totalAmountTxt.setText("$" + dataSnapshot.child("total_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("discount").getValue() != null) {
                        discountAmountTxt.setText("$" + dataSnapshot.child("discount").getValue().toString());

                    }
                    if (dataSnapshot.child("convance_fare").getValue() != null) {
                        baseFareTxt.setText("$" + dataSnapshot.child("convance_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("discount").getValue() != null) {
                        discountAmountTxt.setText("$" + dataSnapshot.child("discount").getValue().toString());

                    }
                    if (dataSnapshot.child("time_fare").getValue() != null) {
                        timeFareTxt.setText("$" + dataSnapshot.child("time_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("waitingTime").getValue() != null) {
                        WaiingTimtTxt.setText(dataSnapshot.child("waitingTime").getValue().toString() + " Min");

                    }
                    if (dataSnapshot.child("waiting_fare").getValue() != null) {
                        WaitingFareTxt.setText("$" + dataSnapshot.child("waiting_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("distance_fare").getValue() != null) {
                        distanceFareTxt.setText("$" + dataSnapshot.child("distance_fare").getValue().toString());
                        RideFareTxt.setText("$" + dataSnapshot.child("distance_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("cancel_fare").getValue() != null) {
                        balanceFareTxt.setText("$" + dataSnapshot.child("cancel_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("pay_type").getValue() != null && !dataSnapshot.child("pay_type").getValue().toString().equalsIgnoreCase("0")) {
                        paymentTypeTxt.setText(dataSnapshot.child("pay_type").getValue().toString());

                    }
                    if (dataSnapshot.child("tax").getValue() != null) {
                        detectFareTxt.setText("$" + dataSnapshot.child("tax").getValue().toString());

                    }
                    if (dataSnapshot.child("basefare").getValue() != null) {
                        basesFareTxt.setText("$" + dataSnapshot.child("basefare").getValue().toString());

                    }
                    if (dataSnapshot.child("distance").getValue() != null) {
                        distanceTxt.setText(dotLimitation(Double.parseDouble(dataSnapshot.child("distance").getValue().toString())) + " KM");

                    }
                    if (dataSnapshot.child("time").getValue() != null) {
                        timeTxt.setText(dataSnapshot.child("time").getValue().toString() + " Min");

                    }
                    if (dataSnapshot.child("isTax").getValue() != null && !dataSnapshot.child("isTax").getValue().toString().equalsIgnoreCase("0")) {
                        taxiLayout.setVisibility(View.VISIBLE);

                    } else {
                        taxiLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("isWaiting").getValue() != null && !dataSnapshot.child("isWaiting").getValue().toString().equalsIgnoreCase("0")) {
                        waitingLayout.setVisibility(View.VISIBLE);

                    } else {
                        waitingLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("isPickup").getValue() != null && !dataSnapshot.child("isPickup").getValue().toString().equalsIgnoreCase("0")) {
                        pickupLayout.setVisibility(View.VISIBLE);

                    } else {
                        pickupLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("trip_type").getValue() != null) {
                        if (dataSnapshot.child("trip_type").getValue().toString().equalsIgnoreCase("flatrate")) {
                            ordinary.setVisibility(View.GONE);
                            rideFareLayout.setVisibility(View.VISIBLE);
                            rideView.setVisibility(View.VISIBLE);
                        } else {
                            ordinary.setVisibility(View.VISIBLE);
                            rideFareLayout.setVisibility(View.GONE);
                            rideView.setVisibility(View.GONE);
                        }

                    }
                    if (dataSnapshot.child("cancel_fare").getValue() != null) {
                        if (dataSnapshot.child("cancel_fare").getValue().toString().equalsIgnoreCase("0")) {
                            cancellationLayout.setVisibility(View.GONE);
                            viewCancel.setVisibility(View.GONE);
                        } else {
                            cancellationLayout.setVisibility(View.VISIBLE);
                            viewCancel.setVisibility(View.VISIBLE);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public String dotLimitation(double values) {
        return new DecimalFormat("##.##").format(values);
    }
}
