package com.auezeride.driver.Model;

public class FeedbackListdataModel {
    String userbaneme;
    String userpic;
    String comment;
    public FeedbackListdataModel (){

    }
    public String getUserbaneme() {
        return userbaneme;
    }

    public void setUserbaneme(String userbaneme) {
        this.userbaneme = userbaneme;
    }

    public String getUserpic() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
