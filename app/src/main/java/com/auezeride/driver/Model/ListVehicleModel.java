package com.auezeride.driver.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListVehicleModel {

    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("cpy")
    @Expose
    private String cpy;
    @SerializedName("licence")
    @Expose
    private String licence;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("makename")
    @Expose
    private String makename;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("taxistatus")
    @Expose
    private String taxistatus;
    @SerializedName("noofshare")
    @Expose
    private Integer noofshare;
    @SerializedName("share")
    @Expose
    private Boolean share;
    @SerializedName("vehicletype")
    @Expose
    private String vehicletype;
    @SerializedName("registrationexpdate")
    @Expose
    private String registrationexpdate;
    @SerializedName("registration")
    @Expose
    private String registration;
    @SerializedName("permitexpdate")
    @Expose
    private String permitexpdate;
    @SerializedName("permit")
    @Expose
    private String permit;
    @SerializedName("insuranceexpdate")
    @Expose
    private String insuranceexpdate;
    @SerializedName("insurance")
    @Expose
    private String insurance;
    @SerializedName("type")
    @Expose
    private List<Type> type = null;
    @SerializedName("handicap")
    @Expose
    private String handicap;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getCpy() {
        return cpy;
    }

    public void setCpy(String cpy) {
        this.cpy = cpy;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMakename() {
        return makename;
    }

    public void setMakename(String makename) {
        this.makename = makename;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaxistatus() {
        return taxistatus;
    }

    public void setTaxistatus(String taxistatus) {
        this.taxistatus = taxistatus;
    }

    public Integer getNoofshare() {
        return noofshare;
    }

    public void setNoofshare(Integer noofshare) {
        this.noofshare = noofshare;
    }

    public Boolean getShare() {
        return share;
    }

    public void setShare(Boolean share) {
        this.share = share;
    }

    public String getVehicletype() {
        return vehicletype;
    }

    public void setVehicletype(String vehicletype) {
        this.vehicletype = vehicletype;
    }

    public String getRegistrationexpdate() {
        return registrationexpdate;
    }

    public void setRegistrationexpdate(String registrationexpdate) {
        this.registrationexpdate = registrationexpdate;
    }

    public String getRegistration() {
        return registration;
    }

    public void setRegistration(String registration) {
        this.registration = registration;
    }

    public String getPermitexpdate() {
        return permitexpdate;
    }

    public void setPermitexpdate(String permitexpdate) {
        this.permitexpdate = permitexpdate;
    }

    public String getPermit() {
        return permit;
    }

    public void setPermit(String permit) {
        this.permit = permit;
    }

    public String getInsuranceexpdate() {
        return insuranceexpdate;
    }

    public void setInsuranceexpdate(String insuranceexpdate) {
        this.insuranceexpdate = insuranceexpdate;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public List<Type> getType() {
        return type;
    }

    public void setType(List<Type> type) {
        this.type = type;
    }

    public String getHandicap() {
        return handicap;
    }

    public void setHandicap(String handicap) {
        this.handicap = handicap;
    }


    public class Type {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("luxury")
        @Expose
        private String luxury;
        @SerializedName("normal")
        @Expose
        private String normal;
        @SerializedName("basic")
        @Expose
        private String basic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLuxury() {
            return luxury;
        }

        public void setLuxury(String luxury) {
            this.luxury = luxury;
        }

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getBasic() {
            return basic;
        }

        public void setBasic(String basic) {
            this.basic = basic;
        }

    }



}