package com.auezeride.driver.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriverProfileModel {


    public class CurrentActiveTaxi {

        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("driver")
        @Expose
        private String driver;
        @SerializedName("cpy")
        @Expose
        private String cpy;
        @SerializedName("licence")
        @Expose
        private String licence;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("makename")
        @Expose
        private String makename;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("taxistatus")
        @Expose
        private String taxistatus;
        @SerializedName("noofshare")
        @Expose
        private String noofshare;
        @SerializedName("share")
        @Expose
        private Boolean share;
        @SerializedName("vehicletype")
        @Expose
        private String vehicletype;
        @SerializedName("rcbookexpdate")
        @Expose
        private String rcbookexpdate;
        @SerializedName("rcbook")
        @Expose
        private String rcbook;
        @SerializedName("permitexpdate")
        @Expose
        private String permitexpdate;
        @SerializedName("permit")
        @Expose
        private String permit;
        @SerializedName("insuranceexpdate")
        @Expose
        private String insuranceexpdate;
        @SerializedName("insurance")
        @Expose
        private String insurance;
        @SerializedName("type")
        @Expose
        private List<Type_> type = null;
        @SerializedName("handicap")
        @Expose
        private String handicap;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getCpy() {
            return cpy;
        }

        public void setCpy(String cpy) {
            this.cpy = cpy;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getMakename() {
            return makename;
        }

        public void setMakename(String makename) {
            this.makename = makename;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTaxistatus() {
            return taxistatus;
        }

        public void setTaxistatus(String taxistatus) {
            this.taxistatus = taxistatus;
        }

        public String getNoofshare() {
            return noofshare;
        }

        public void setNoofshare(String noofshare) {
            this.noofshare = noofshare;
        }

        public Boolean getShare() {
            return share;
        }

        public void setShare(Boolean share) {
            this.share = share;
        }

        public String getVehicletype() {
            return vehicletype;
        }

        public void setVehicletype(String vehicletype) {
            this.vehicletype = vehicletype;
        }

        public String getRcbookexpdate() {
            return rcbookexpdate;
        }

        public void setRcbookexpdate(String rcbookexpdate) {
            this.rcbookexpdate = rcbookexpdate;
        }

        public String getRcbook() {
            return rcbook;
        }

        public void setRcbook(String rcbook) {
            this.rcbook = rcbook;
        }

        public String getPermitexpdate() {
            return permitexpdate;
        }

        public void setPermitexpdate(String permitexpdate) {
            this.permitexpdate = permitexpdate;
        }

        public String getPermit() {
            return permit;
        }

        public void setPermit(String permit) {
            this.permit = permit;
        }

        public String getInsuranceexpdate() {
            return insuranceexpdate;
        }

        public void setInsuranceexpdate(String insuranceexpdate) {
            this.insuranceexpdate = insuranceexpdate;
        }

        public String getInsurance() {
            return insurance;
        }

        public void setInsurance(String insurance) {
            this.insurance = insurance;
        }

        public List<Type_> getType() {
            return type;
        }

        public void setType(List<Type_> type) {
            this.type = type;
        }

        public String getHandicap() {
            return handicap;
        }

        public void setHandicap(String handicap) {
            this.handicap = handicap;
        }

    }

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("fname")
    @Expose
    private String fname;
    @SerializedName("lname")
    @Expose
    private String lname;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("cnty")
    @Expose
    private String cnty;
    @SerializedName("cntyname")
    @Expose
    private String cntyname;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("cityname")
    @Expose
    private String cityname;
    @SerializedName("cur")
    @Expose
    private String cur;
    @SerializedName("actMail")
    @Expose
    private String actMail;
    @SerializedName("actHolder")
    @Expose
    private String actHolder;
    public Boolean getConnected() {
        return isConnected;
    }

    @SerializedName("isConnected")
    @Expose
    private Boolean isConnected;
    @SerializedName("actNo")
    @Expose
    private String actNo;
    @SerializedName("actBank")
    @Expose
    private String actBank;
    @SerializedName("actLoc")
    @Expose
    private String actLoc;
    @SerializedName("actCode")
    @Expose
    private String actCode;
    @SerializedName("__v")
    @Expose
    private String v;
    @SerializedName("coords")
    @Expose
    private List<String> coords = null;
    @SerializedName("softdel")
    @Expose
    private String softdel;
    @SerializedName("fcmId")
    @Expose
    private Object fcmId;
    @SerializedName("wallet")
    @Expose
    private String wallet;
    @SerializedName("rating")
    @Expose
    private Rating rating;
    @SerializedName("online")
    @Expose
    private String online;
    @SerializedName("sharebooked")
    @Expose
    private String sharebooked;
    @SerializedName("noofshare")
    @Expose
    private String noofshare;
    @SerializedName("share")
    @Expose
    private Boolean share;
    @SerializedName("curStatus")
    @Expose
    private String curStatus;
    @SerializedName("curService")
    @Expose
    private String curService;
    @SerializedName("serviceStatus")
    @Expose
    private String serviceStatus;
    @SerializedName("currentTaxi")
    @Expose
    private String currentTaxi;
    @SerializedName("aadharexp")
    @Expose
    private String aadharexp;
    @SerializedName("aadhar")
    @Expose
    private String aadhar;
    @SerializedName("licenceexp")
    @Expose
    private String licenceexp;
    @SerializedName("licence")
    @Expose
    private String licence;
    @SerializedName("taxis")
    @Expose
    private List<Taxi> taxis = null;
    @SerializedName("status")
    @Expose
    private List<Status> status = null;
    @SerializedName("baseurl")
    @Expose
    private String baseurl;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("lang")
    @Expose
    private String lang;
    @SerializedName("phcode")
    @Expose
    private String phcode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("profileurl")
    @Expose
    private String profileurl;
    @SerializedName("currentActiveTaxi")
    @Expose
    private CurrentActiveTaxi currentActiveTaxi;
    @SerializedName("isDriverCreditModuleEnabledForUseAfterLogin")
    @Expose
    private Boolean isDriverCreditModuleEnabledForUseAfterLogin;
    @SerializedName("address")
    @Expose
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCnty() {
        return cnty;
    }

    public void setCnty(String cnty) {
        this.cnty = cnty;
    }

    public String getCntyname() {
        return cntyname;
    }

    public void setCntyname(String cntyname) {
        this.cntyname = cntyname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCur() {
        return cur;
    }

    public void setCur(String cur) {
        this.cur = cur;
    }

    public String getActMail() {
        return actMail;
    }

    public void setActMail(String actMail) {
        this.actMail = actMail;
    }

    public String getActHolder() {
        return actHolder;
    }

    public void setActHolder(String actHolder) {
        this.actHolder = actHolder;
    }

    public String getActNo() {
        return actNo;
    }

    public void setActNo(String actNo) {
        this.actNo = actNo;
    }

    public String getActBank() {
        return actBank;
    }

    public void setActBank(String actBank) {
        this.actBank = actBank;
    }

    public String getActLoc() {
        return actLoc;
    }

    public void setActLoc(String actLoc) {
        this.actLoc = actLoc;
    }

    public String getActCode() {
        return actCode;
    }

    public void setActCode(String actCode) {
        this.actCode = actCode;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public List<String> getCoords() {
        return coords;
    }

    public void setCoords(List<String> coords) {
        this.coords = coords;
    }

    public String getSoftdel() {
        return softdel;
    }

    public void setSoftdel(String softdel) {
        this.softdel = softdel;
    }

    public Object getFcmId() {
        return fcmId;
    }

    public void setFcmId(Object fcmId) {
        this.fcmId = fcmId;
    }

    public String getWallet() {
        return wallet;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public String getOnline() {
        return online;
    }

    public void setOnline(String online) {
        this.online = online;
    }

    public String getSharebooked() {
        return sharebooked;
    }

    public void setSharebooked(String sharebooked) {
        this.sharebooked = sharebooked;
    }

    public String getNoofshare() {
        return noofshare;
    }

    public void setNoofshare(String noofshare) {
        this.noofshare = noofshare;
    }

    public Boolean getShare() {
        return share;
    }

    public void setShare(Boolean share) {
        this.share = share;
    }

    public String getCurStatus() {
        return curStatus;
    }

    public void setCurStatus(String curStatus) {
        this.curStatus = curStatus;
    }

    public String getCurService() {
        return curService;
    }

    public void setCurService(String curService) {
        this.curService = curService;
    }

    public String getServiceStatus() {
        return serviceStatus;
    }

    public void setServiceStatus(String serviceStatus) {
        this.serviceStatus = serviceStatus;
    }

    public String getCurrentTaxi() {
        return currentTaxi;
    }

    public void setCurrentTaxi(String currentTaxi) {
        this.currentTaxi = currentTaxi;
    }

    public String getAadharexp() {
        return aadharexp;
    }

    public void setAadharexp(String aadharexp) {
        this.aadharexp = aadharexp;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getLicenceexp() {
        return licenceexp;
    }

    public void setLicenceexp(String licenceexp) {
        this.licenceexp = licenceexp;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public List<Taxi> getTaxis() {
        return taxis;
    }

    public void setTaxis(List<Taxi> taxis) {
        this.taxis = taxis;
    }

    public List<Status> getStatus() {
        return status;
    }

    public void setStatus(List<Status> status) {
        this.status = status;
    }

    public String getBaseurl() {
        return baseurl;
    }

    public void setBaseurl(String baseurl) {
        this.baseurl = baseurl;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPhcode() {
        return phcode;
    }

    public void setPhcode(String phcode) {
        this.phcode = phcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProfileurl() {
        return profileurl;
    }

    public void setProfileurl(String profileurl) {
        this.profileurl = profileurl;
    }

    public CurrentActiveTaxi getCurrentActiveTaxi() {
        return currentActiveTaxi;
    }

    public void setCurrentActiveTaxi(CurrentActiveTaxi currentActiveTaxi) {
        this.currentActiveTaxi = currentActiveTaxi;
    }

    public Boolean getIsDriverCreditModuleEnabledForUseAfterLogin() {
        return isDriverCreditModuleEnabledForUseAfterLogin;
    }

    public void setIsDriverCreditModuleEnabledForUseAfterLogin(Boolean isDriverCreditModuleEnabledForUseAfterLogin) {
        this.isDriverCreditModuleEnabledForUseAfterLogin = isDriverCreditModuleEnabledForUseAfterLogin;
    }

    public class Rating {

        @SerializedName("cmts")
        @Expose
        private String cmts;
        @SerializedName("star")
        @Expose
        private String star;
        @SerializedName("tottrip")
        @Expose
        private String tottrip;
        @SerializedName("nos")
        @Expose
        private String nos;
        @SerializedName("rating")
        @Expose
        private String rating;

        public String getCmts() {
            return cmts;
        }

        public void setCmts(String cmts) {
            this.cmts = cmts;
        }

        public String getStar() {
            return star;
        }

        public void setStar(String star) {
            this.star = star;
        }

        public String getTottrip() {
            return tottrip;
        }

        public void setTottrip(String tottrip) {
            this.tottrip = tottrip;
        }

        public String getNos() {
            return nos;
        }

        public void setNos(String nos) {
            this.nos = nos;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

    }

    public class Status {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("canoperate")
        @Expose
        private String canoperate;
        @SerializedName("docs")
        @Expose
        private String docs;
        @SerializedName("models")
        @Expose
        private String models;
        @SerializedName("curstatus")
        @Expose
        private String curstatus;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCanoperate() {
            return canoperate;
        }

        public void setCanoperate(String canoperate) {
            this.canoperate = canoperate;
        }

        public String getDocs() {
            return docs;
        }

        public void setDocs(String docs) {
            this.docs = docs;
        }

        public String getModels() {
            return models;
        }

        public void setModels(String models) {
            this.models = models;
        }

        public String getCurstatus() {
            return curstatus;
        }

        public void setCurstatus(String curstatus) {
            this.curstatus = curstatus;
        }

    }
    public class Taxi {

        @SerializedName("color")
        @Expose
        private String color;
        @SerializedName("driver")
        @Expose
        private String driver;
        @SerializedName("cpy")
        @Expose
        private String cpy;
        @SerializedName("licence")
        @Expose
        private String licence;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("makename")
        @Expose
        private String makename;
        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("taxistatus")
        @Expose
        private String taxistatus;
        @SerializedName("noofshare")
        @Expose
        private String noofshare;
        @SerializedName("share")
        @Expose
        private Boolean share;
        @SerializedName("vehicletype")
        @Expose
        private String vehicletype;
        @SerializedName("rcbookexpdate")
        @Expose
        private String rcbookexpdate;
        @SerializedName("rcbook")
        @Expose
        private String rcbook;
        @SerializedName("permitexpdate")
        @Expose
        private String permitexpdate;
        @SerializedName("permit")
        @Expose
        private String permit;
        @SerializedName("insuranceexpdate")
        @Expose
        private String insuranceexpdate;
        @SerializedName("insurance")
        @Expose
        private String insurance;
        @SerializedName("type")
        @Expose
        private List<Type> type = null;
        @SerializedName("handicap")
        @Expose
        private String handicap;

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getDriver() {
            return driver;
        }

        public void setDriver(String driver) {
            this.driver = driver;
        }

        public String getCpy() {
            return cpy;
        }

        public void setCpy(String cpy) {
            this.cpy = cpy;
        }

        public String getLicence() {
            return licence;
        }

        public void setLicence(String licence) {
            this.licence = licence;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getMakename() {
            return makename;
        }

        public void setMakename(String makename) {
            this.makename = makename;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTaxistatus() {
            return taxistatus;
        }

        public void setTaxistatus(String taxistatus) {
            this.taxistatus = taxistatus;
        }

        public String getNoofshare() {
            return noofshare;
        }

        public void setNoofshare(String noofshare) {
            this.noofshare = noofshare;
        }

        public Boolean getShare() {
            return share;
        }

        public void setShare(Boolean share) {
            this.share = share;
        }

        public String getVehicletype() {
            return vehicletype;
        }

        public void setVehicletype(String vehicletype) {
            this.vehicletype = vehicletype;
        }

        public String getRcbookexpdate() {
            return rcbookexpdate;
        }

        public void setRcbookexpdate(String rcbookexpdate) {
            this.rcbookexpdate = rcbookexpdate;
        }

        public String getRcbook() {
            return rcbook;
        }

        public void setRcbook(String rcbook) {
            this.rcbook = rcbook;
        }

        public String getPermitexpdate() {
            return permitexpdate;
        }

        public void setPermitexpdate(String permitexpdate) {
            this.permitexpdate = permitexpdate;
        }

        public String getPermit() {
            return permit;
        }

        public void setPermit(String permit) {
            this.permit = permit;
        }

        public String getInsuranceexpdate() {
            return insuranceexpdate;
        }

        public void setInsuranceexpdate(String insuranceexpdate) {
            this.insuranceexpdate = insuranceexpdate;
        }

        public String getInsurance() {
            return insurance;
        }

        public void setInsurance(String insurance) {
            this.insurance = insurance;
        }

        public List<Type> getType() {
            return type;
        }

        public void setType(List<Type> type) {
            this.type = type;
        }

        public String getHandicap() {
            return handicap;
        }

        public void setHandicap(String handicap) {
            this.handicap = handicap;
        }

    }

    public class Type {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("luxury")
        @Expose
        private String luxury;
        @SerializedName("normal")
        @Expose
        private String normal;
        @SerializedName("basic")
        @Expose
        private String basic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLuxury() {
            return luxury;
        }

        public void setLuxury(String luxury) {
            this.luxury = luxury;
        }

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getBasic() {
            return basic;
        }

        public void setBasic(String basic) {
            this.basic = basic;
        }

    }

    public class Type_ {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("luxury")
        @Expose
        private String luxury;
        @SerializedName("normal")
        @Expose
        private String normal;
        @SerializedName("basic")
        @Expose
        private String basic;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLuxury() {
            return luxury;
        }

        public void setLuxury(String luxury) {
            this.luxury = luxury;
        }

        public String getNormal() {
            return normal;
        }

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getBasic() {
            return basic;
        }

        public void setBasic(String basic) {
            this.basic = basic;
        }

    }

}
