package com.auezeride.driver.View;

import com.auezeride.driver.Model.AddVehicleModel;
import com.auezeride.driver.Model.CarModel;
import com.auezeride.driver.Model.ServiceModel;

import retrofit2.Response;

public interface CarModelView {

     void OnSuccessfullsy(Response<CarModel> Response);

    void OnFailurse(Response<CarModel> Response);

    void AddvehicleSucessfully(Response<AddVehicleModel> response);

    void AddvehicleFailure(Response<AddVehicleModel> response);

    void editVehicleSuccessfull();

    void editVehicleFauiler();

    void onSuccessServiceList(ServiceModel serviceModels);

    void onFailureService(Throwable throwable);
}
