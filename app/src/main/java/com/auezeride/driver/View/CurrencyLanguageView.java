package com.auezeride.driver.View;


import com.auezeride.driver.Model.LanguageCurrencyModel;

import java.util.List;

public interface CurrencyLanguageView {

    void countriesReady(List<LanguageCurrencyModel> LanguageCurrencyModel);
}
