package com.auezeride.driver.View;

import com.auezeride.driver.Model.ChangePasswordModel;

import retrofit2.Response;

public interface ChangePasswordView {
    void OnSuccessfully(Response<ChangePasswordModel> Response);
    void OnFailure(Response<ChangePasswordModel> Response);


}
