package com.auezeride.driver.View;

import com.auezeride.driver.Model.AddVehicleDocModel;
import com.auezeride.driver.Model.DriverDocumentModel;

public interface DocumentView {
    void DocumentSuccess(retrofit2.Response<DriverDocumentModel> Response);

    void Errorlogview(retrofit2.Response<DriverDocumentModel> Response);

    void VehicleDocSuccess(retrofit2.Response<AddVehicleDocModel> Response);

    void VehicleDoFailed(retrofit2.Response<AddVehicleDocModel> Response);
}
