package com.auezeride.driver.View;


import com.auezeride.driver.Model.UpdatProfileModel;
import retrofit2.Response;

public interface EditProfileView {
    void OnSuccessfully(Response<UpdatProfileModel> Response);
    void OnFailure(Response<UpdatProfileModel> Response);

}
