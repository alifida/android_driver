package com.auezeride.driver.View;


import com.auezeride.driver.Model.TripFlowModel;
import retrofit2.Response;

public interface TripFlowView {

    void OnSuccessfully(Response<TripFlowModel> Response);

    void OnFailure(Response<TripFlowModel> Response);
}
