package com.auezeride.driver.View;

import com.auezeride.driver.Model.AcceptRequestModel;
import com.auezeride.driver.Model.DiclineRequest;

import retrofit2.Response;

public interface RequestView {

    void OnSuccessAccept(Response<AcceptRequestModel> Response);

    void onFailureAccept(Response<AcceptRequestModel> Response);

    void onSuccessDicline(Response<DiclineRequest> Response);

    void onFailureDicline(Response<DiclineRequest> Response);

    void OnScheduleSuccessAccept(Response<AcceptRequestModel> Response);

    void onScheduleFailureAccept(Response<AcceptRequestModel> Response);
}
