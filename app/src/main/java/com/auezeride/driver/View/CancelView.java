package com.auezeride.driver.View;

import com.auezeride.driver.Model.CancelTripModel;
import retrofit2.Response;

public interface CancelView {
    void OnSuccessfully(Response<CancelTripModel> Response);

    void OnFailure(Response<CancelTripModel> Response);
}
