package com.auezeride.driver.View;

import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.Model.OnlineOflline;

import java.util.List;

import retrofit2.Response;

public interface DriverView {
    void OnlineSuccess(retrofit2.Response<OnlineOflline> Response, String status);

    void OnlineFailed(retrofit2.Response<OnlineOflline> Response, String status);

    void VehicleListsuccessFully(Response<List<ListVehicleModel>> response);

    void VehicleListFailure(Response<List<ListVehicleModel>> response);
}

