package com.auezeride.driver.View;

import com.auezeride.driver.Model.EarningsModel;

import retrofit2.Response;

public interface EarningsView {

    void onSuccess(Response<EarningsModel> Response);

    void onFailure(Response<EarningsModel> Response);
}
