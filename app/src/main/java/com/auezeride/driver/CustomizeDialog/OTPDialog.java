package com.auezeride.driver.CustomizeDialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;

import android.view.Window;
import android.widget.Button;

import com.mukesh.OtpView;

import java.util.Objects;

import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.R;
import com.auezeride.driver.View.TripInterface;
import butterknife.BindView;

/**
 * Created by com on 18-Sep-18.
 */

public class OTPDialog extends Dialog {

    public Activity activity;
    @BindView(R.id.otp_view)
    OtpView otpView;
    @BindView(R.id.submit)
    Button submit;
    TripInterface tripInterface;
    public String strOTP;
    Boolean status = false;

    public OTPDialog(@NonNull Activity activity, String OTP, TripInterface tripInterface, Boolean status) {
        super(activity, R.style.DialogStyle);
        this.activity = activity;
        this.strOTP = OTP;
        this.tripInterface = tripInterface;
        this.status = status;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.otpverification);
        otpView = findViewById(R.id.otp_view);
        submit = findViewById(R.id.submit);

        submit.setOnClickListener(view -> {
            System.out.println("Enter the opt" + Objects.requireNonNull(otpView.getText()).toString());
            if (otpView.getText().toString().isEmpty()) {
                Utiles.displayMessage(getCurrentFocus(), activity, activity.getString(R.string.enter_your_otp));

            } else if (!strOTP.equals(otpView.getText().toString())) {
                Utiles.displayMessage(getCurrentFocus(), activity, activity.getString(R.string.enter_valid_opt));
            } else {
                tripInterface.CheckTripStatus(status);
                dismiss();

            }

        });
    }
}

