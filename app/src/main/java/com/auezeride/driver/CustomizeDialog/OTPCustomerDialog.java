package com.auezeride.driver.CustomizeDialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Window;
import android.widget.Button;

import com.mukesh.OtpView;

import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.R;
import com.auezeride.driver.View.RegisterView;
import butterknife.BindView;

/**
 * Created by com on 25-Jul-18.
 */

public class OTPCustomerDialog extends Dialog   {

    public Activity activity;
    @BindView(R.id.otp_view)
    OtpView otpView;


    @BindView(R.id.submit)
    Button submit;

    public String strOTP;
    public RegisterView registerView;

    public OTPCustomerDialog(@NonNull Activity activity, String OTP, RegisterView registerView) {
        super(activity, R.style.DialogStyle);
        this.activity = activity;
        this.strOTP = OTP;
        this.registerView = registerView;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.otpverification);
        otpView = findViewById(R.id.otp_view);
        submit = findViewById(R.id.submit);
        /*if(BuildConfig.DEBUG){
            otpView.setText(strOTP);
        }*/
        otpView.setText(strOTP);
        submit.setOnClickListener(view -> {
            System.out.println("Enter the opt" + otpView.getText().toString());
            if (otpView.getText().toString().isEmpty()) {
                Utiles.displayMessage(getCurrentFocus(), activity.getApplicationContext(), activity.getResources().getString(R.string.enter_your_otp));

            } else if (!strOTP.equals(otpView.getText().toString())) {
                Utiles.displayMessage(getCurrentFocus(), activity.getApplicationContext(), activity.getResources().getString(R.string.enter_Valid_opt));
            } else {
                dismiss();
                registerView.OTPVerification();
            }

        });

    }



    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


}
