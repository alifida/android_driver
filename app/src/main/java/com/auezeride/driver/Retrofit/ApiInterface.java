package com.auezeride.driver.Retrofit;


import com.auezeride.driver.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.driver.Model.AcceptRequestModel;
import com.auezeride.driver.Model.AddBankModel;
import com.auezeride.driver.Model.AddVehicleDocModel;
import com.auezeride.driver.Model.AddVehicleModel;
import com.auezeride.driver.Model.BankDetailsModel;
import com.auezeride.driver.Model.CancelTripModel;
import com.auezeride.driver.Model.CarModel;
import com.auezeride.driver.Model.ChangePasswordModel;
import com.auezeride.driver.Model.DeleteVehicle;
import com.auezeride.driver.Model.DiclineRequest;
import com.auezeride.driver.Model.DriverDocumentModel;
import com.auezeride.driver.Model.DriverProfileModel;
import com.auezeride.driver.Model.EarningModel;
import com.auezeride.driver.Model.EarningsModel;
import com.auezeride.driver.Model.FeedbackModel;
import com.auezeride.driver.Model.ForgetPasswordModel;
import com.auezeride.driver.Model.LanguageCurrencyModel;
import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.Model.LoginModel;
import com.auezeride.driver.Model.OTPModel;
import com.auezeride.driver.Model.OTPVerificationModel;
import com.auezeride.driver.Model.OnlineOflline;
import com.auezeride.driver.Model.PaymentModel;
import com.auezeride.driver.Model.Polygon.PolygonModel;
import com.auezeride.driver.Model.RatingModel;
import com.auezeride.driver.Model.RegisterModel;
import com.auezeride.driver.Model.ScheduleCancelModel;
import com.auezeride.driver.Model.ServiceModel;
import com.auezeride.driver.Model.TripDetailsModel;
import com.auezeride.driver.Model.TripFlowModel;
import com.auezeride.driver.Model.TripHistoryModel;
import com.auezeride.driver.Model.UpdatProfileModel;
import com.auezeride.driver.Model.UpdateVehicleModel;
import com.auezeride.driver.Model.scheduleTripListModel;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("commondata")
    Call<List<LanguageCurrencyModel>> getCountryandLanguage();

    @FormUrlEncoded
    @POST("driver")
    Call<RegisterModel> getRegister(@FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST("driverlogin")
    Call<LoginModel> getLogin(@FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST("riderslogins")
    Call<PaymentModel> submitPayment(@FieldMap HashMap<String, String> data);

    @Multipart
    @POST("driverDocs")
    Call<DriverDocumentModel> DocumentUpload(@Header("x-access-token") String Access_token, @PartMap HashMap<String, RequestBody> data, @Part MultipartBody.Part filePart);

    @FormUrlEncoded
    @PUT("driverpwd")
    Call<ChangePasswordModel> ChangePassword(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @GET("driver")
    Call<List<DriverProfileModel>> getProfile(@Header("x-access-token") String Access_token);

    @GET("carmakeandyear")
    Call<CarModel> getCarModel(@Header("x-access-token") String Access_token);

    @FormUrlEncoded
    @POST("drivertaxi")
    Call<AddVehicleModel> AddVehicle(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @PUT("drivertaxi")
    Call<AddVehicleModel> editVehicle(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @GET("drivertaxi/{id}")
    Call<List<ListVehicleModel>> getVehicleList(@Header("x-access-token") String Access_token, @Path("id") String id);

    @HTTP(method = "DELETE", path = "drivertaxi", hasBody = true)
    Call<DeleteVehicle> DeleteVhicle(@Header("x-access-token") String Access_token, @Body HashMap<String, String> data);


    @Multipart
    @POST("driverTaxiDocs")
    Call<AddVehicleDocModel> uploadVehicledoc(@Header("x-access-token") String Access_token, @PartMap HashMap<String, RequestBody> map, @Part MultipartBody.Part filePart);


    @Multipart
    @PUT("driver")
    Call<UpdatProfileModel> updateProfile(@Header("x-access-token") String Access_token, @PartMap HashMap<String, RequestBody> data, @Part MultipartBody.Part filePart);

    @FormUrlEncoded
    @PUT("setOnlineStatus")
    Call<OnlineOflline> Onlineoffline(@Header("x-access-token") String Access_token, @Field("status") String status);


    @FormUrlEncoded
    @PUT("DriverLocation")
    Call<ResponseBody> UpdateLocation(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @PUT("setCurrentTaxi")
    Call<UpdateVehicleModel> UpdateVehicle(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @PUT("declineRequest")
    Call<DiclineRequest> getDiclineRequest(@Header("x-access-token") String Access_token, @Field("requestId") String request_id);

    @FormUrlEncoded
    @PUT("acceptRequest")
    Call<AcceptRequestModel> getAcceptRequest(@Header("x-access-token") String Access_token, @Field("requestId") String request_id);

    @FormUrlEncoded
    @PUT("acceptScheduleRequest")
    Call<AcceptRequestModel> getScheduleAcceptRequest(@Header("x-access-token") String Access_token, @Field("requestId") String requestId, @Field("reqtripDT") String reqtripDT);

    @FormUrlEncoded
    @PUT("cancelTrip")
    Call<CancelTripModel> CancelTrip(@Header("x-access-token") String Access_token, @Field("tripId") String tripId, @Field("reason") String reason);

    @FormUrlEncoded
    @PUT("tripCurrentStatus")
    Call<TripFlowModel> TripFlowStatus(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @PUT("driverFeedback")
    Call<FeedbackModel> FeedBack(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST("driverTripHistory")
    Call<List<TripHistoryModel>> getTripHistory(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @FormUrlEncoded
    @POST("driverEarningsBtDate")
    Call<List<EarningModel>> getEaringList(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @GET("myRatings")
    Call<List<RatingModel>> getRating(@Header("x-access-token") String Access_token);

    @GET("feedbackLists")
    Call<ResponseBody> getFeedbackList(@Header("x-access-token") String Access_token);

    @POST("myEarnings")
    Call<EarningsModel> getEarings(@Header("x-access-token") String Access_token);

    @FormUrlEncoded
    @PUT("pastTripDetail")
    Call<TripDetailsModel> getTripDetails(@Header("x-access-token") String Access_token, @Field("tripId") String tripId);

    @GET("driverUpcomingScheduleTaxi")
    Call<List<scheduleTripListModel>> getScheduleList(@Header("x-access-token") String Access_token);

    @FormUrlEncoded
    @PUT("driverCancelScheduleTaxi")
    Call<ScheduleCancelModel> getcancelShedule(@Header("x-access-token") String Access_token, @Field("requestId") String requestId);

    @FormUrlEncoded
    @PUT("addDriverBank")
    Call<AddBankModel> AddAndUpdateBank(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);

    @GET("driverBankDetails")
    Call<BankDetailsModel> getBankDetails(@Header("x-access-token") String Access_token);

    @FormUrlEncoded
    @POST("driverForgotPassword")
    Call<ForgetPasswordModel> getSendOPTChangesPaassword(@Header("x-access-token") String Access_token, @Field("email") String email);

    @PATCH("driverForgotPassword")
    Call<OTPVerificationModel> getChangePassword(@Header("x-access-token") String Access_token, @Body HashMap<String, String> data);

    @GET("vehicletypelists")
    Flowable<ServiceModel> getServiceType(@Header("x-access-token") String Access_token);

    @GET("logout")
    Call<ResponseBody> getLogout(@Header("x-access-token") String Access_token);

    @FormUrlEncoded
    @POST("verifyNumberDriver")
    Call<OTPModel> getOpt(@Field("phone") String phone, @Field("email") String email_id, @Field("phcode") String phcode);


    @GET("geocode/json")
    Flowable<GeocoderModel> getAddressFromLocation(@Query("latlng") String latlng, @Query("key") String key);

    @GET("getCityBoundaryPolygon/{City}")
    Flowable<PolygonModel> getCityPolygon(@Path("City") String City);

    @FormUrlEncoded
    @PUT("driverRequestPayoutTransferAmount")
    Call<OTPVerificationModel> getSentPayoutRequest(@Header("x-access-token") String Access_token, @FieldMap HashMap<String, String> data);


    @GET("getBankData")
    Call<OTPModel> getStripedata(@Header("x-access-token") String Access_token);
}
