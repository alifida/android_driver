package com.auezeride.driver.FlowInterface;

import com.auezeride.driver.Model.TripFlowModel;

import retrofit2.Response;

public interface RequestInterface {
    void TripFragment();

    void summaryFragment();

    void ClearFragment();

    void CallCancelFragment();

    void ClearAllFragment();

    void FlowDetails(Response<TripFlowModel> Response);
}
