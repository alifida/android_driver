package com.auezeride.driver.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.OTPVerificationModel;
import com.auezeride.driver.Presenter.PayoutPresenter;
import com.auezeride.driver.R;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.IOException;
import java.util.HashMap;

import com.auezeride.driver.CommonClass.CommonFirebaseListoner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import retrofit2.Response;

public class DriverCreditFragment extends BaseFragment implements PayoutPresenter.PayoutView {
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.credits_txt)
    TextView creditsTxt;
    Unbinder unbinder;
    @BindView(R.id.payout_btn)
    Button payoutBtn;
    @BindView(R.id.bank_btn)
    Button bankBtn;
    @BindView(R.id.Cancel_reason_txt)
    MaterialEditText CancelReasonTxt;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.other_cancel_reason)
    CardView otherCancelReason;
    @BindView(R.id.cancel_reason_frame)
    FrameLayout cancelReasonFrame;
    private PayoutPresenter payoutPresenter;
    public DriverCreditFragment() {
        // Required empty public constructor
    }

    Activity activity;
    Context context;
    private FragmentManager fragmentManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        try {
            creditsTxt.setText(getString(R.string.your_remaining_credits) + " $ " + CommonData.walletBalance);
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragmentManager = getFragmentManager();
        payoutPresenter = new PayoutPresenter(activity, this);
        return view;
    }
    AlertDialog walletAlert;
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
    }
    @Optional
    @OnClick({R.id.payout_btn, R.id.bank_btn, R.id.back_img,R.id.submit,R.id.cancel_reason_frame})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.payout_btn:
               /* if (SharedHelper.getKey(context, "isconnoect").equalsIgnoreCase("1")) {

                } else {
                    Alertdialog();
                }
*/
                if (Utiles.MakeDouble(CommonData.walletBalance) >= Utiles.MakeDouble(CommonFirebaseListoner.minimumPayoutAmount)) {
                    slideUp(cancelReasonFrame);
                } else {
                    Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.your_available_balance_low));
                }
                break;
            case R.id.bank_btn:
                if(SharedHelper.getOnlineStatus(activity,"isconnect")){
                    Alertdialog();
                }else {
                    moveToFragment(new StripePayoutFragment("Add Bank"));
                }

                break;
            case R.id.back_img:
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.submit:
                if(CancelReasonTxt.getText().toString().isEmpty()){
                    CancelReasonTxt.setError(activity.getResources().getString(R.string.please_enter_your_amount));
                }else {
                    cancelReasonFrame.setVisibility(View.GONE);
                    Utiles.hideKeyboard(activity);
                    HashMap<String, String> map = new HashMap<>();
                    map.put("amount", CancelReasonTxt.getText().toString());
                    map.put("description", "Amount to payout");
                    payoutPresenter.submitpayment(map);
                    CancelReasonTxt.setText("");
                }
                break;
            case R.id.cancel_reason_frame:
                slideDown(cancelReasonFrame);
                break;
        }
    }
    public void slideUp(View view) {
        Animation slide_up = AnimationUtils.loadAnimation(activity,
                R.anim.slide_up);
        view.startAnimation(slide_up);
        view.setVisibility(View.VISIBLE);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {
        Animation slide_down = AnimationUtils.loadAnimation(activity,
                R.anim.slide_down);
        view.startAnimation(slide_down);
        view.setVisibility(View.GONE);
    }
    private void moveToFragment(Fragment fragment) {

        try {
            fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.wallet_containter, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Alertdialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(getResources().getString(R.string.app_name));
        builder1.setMessage("You already have an account");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "View Account",
                (dialog, id) -> {
                    dialog.dismiss();
                    Utiles.hideKeyboard(activity);
                    moveToFragment(new StripePayoutFragment("Login"));

                });
        builder1.setNegativeButton("Add Account", (dialog, which) ->{dialog.cancel();
            moveToFragment(new StripePayoutFragment("Add Bank"));} );

        walletAlert = builder1.create();
        walletAlert.show();
    }

    @Override
    public void onSuccess(Response<OTPVerificationModel> Response) {
        try {
            Utiles.displayMessage(getView(), activity, Response.body().getMessage());
            creditsTxt.setText(getString(R.string.your_remaining_credits)+" $ " +Response.body().getBalance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFailure(Response<OTPVerificationModel> Response) {
        try {
            String Message = Response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
        }
    }
}
