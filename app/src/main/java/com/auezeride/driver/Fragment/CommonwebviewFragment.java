package com.auezeride.driver.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.R;
import com.auezeride.driver.Retrofit.RetrofitGenerator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class CommonwebviewFragment extends Fragment {


    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.title_txt)
    TextView titleTxt;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.webView1)
    WebView webView1;
    Unbinder unbinder;

    public CommonwebviewFragment() {
        // Required empty public constructor
    }

    Context context;
    ProgressDialog progressDialog = null;
    Activity activity;
    String Strurl ="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_commonwebview, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();
        titleTxt.setText(CommonData.strTitle);

        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        if( CommonData.strTitle.equalsIgnoreCase(context.getResources().getString(R.string.about_us))){
            Strurl = RetrofitGenerator.BaseUrl+"aboutus";
        }else if( CommonData.strTitle.equalsIgnoreCase(context.getResources().getString(R.string.privacy_policy))){
            Strurl = RetrofitGenerator.BaseUrl+"privacypolicy";
        }else {
            Strurl = RetrofitGenerator.BaseUrl+"drivertnc";
        }





        startWebView(Strurl);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.back_img)
    public void onViewClicked() {
        getFragmentManager().popBackStackImmediate();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void startWebView(String url) {


        webView1.setWebViewClient( new WebViewClient());



        // Javascript inabled on webview
        webView1.getSettings().setJavaScriptEnabled(true);

        // Other webview options
        /*
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setBuiltInZoomControls(true);
        */

        /*
         String summary = "<html><body>You scored <b>192</b> points.</body></html>";
         webview.loadData(summary, "text/html", null);
         */

        //Load url in webview
        webView1.loadUrl(url);


    }

}