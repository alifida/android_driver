package com.auezeride.driver.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import com.auezeride.driver.Adapter.EarningsAdapter;
import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.Model.EarningModel;
import com.auezeride.driver.Presenter.EarningPresenter;
import com.auezeride.driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class DriverEarningsFragment extends BaseFragment implements EarningsAdapter.CallbackLs, EarningPresenter.EarningViews, DatePickerDialog.OnDateSetListener {


    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.earnings_recycleview)
    RecyclerView earningsRecycleview;
    @BindView(R.id.nodata_txt)
    TextView nodataTxt;
    Unbinder unbinder;
    Activity activity;
    Context context;
    FragmentManager fragmentManager;
    EarningsAdapter earningsAdapter;
    EarningPresenter earningPresenter;
    List<EarningModel> earningModels;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.from_edt)
    MaterialEditText fromEdt;
    @BindView(R.id.to_edt)
    MaterialEditText toEdt;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount = 0;
    int Page = 1;
    boolean IsFrom = false;
    String strfrom = "", strTo = "", sttype = "", strfromMonth = "";
    LinearLayoutManager LinearLayoutManagers;
    DatePickerDialog dpd;

    public DriverEarningsFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_driver_earnings, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        fragmentManager = getFragmentManager();
        earningPresenter = new EarningPresenter(this, activity);
        earningModels = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)


        );
        getEarningList();
        LinearLayoutManagers = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        earningsRecycleview.setLayoutManager(LinearLayoutManagers);
        earningsRecycleview.setItemAnimator(new DefaultItemAnimator());
        earningsRecycleview.setHasFixedSize(true);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
    }

    @OnClick({R.id.back_img, R.id.from_edt, R.id.to_edt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.from_edt:
                IsFrom = true;
                initCalendar();
                break;
            case R.id.to_edt:
                IsFrom = false;
                initCalendar();
                break;

        }

    }

    private void initCalendar() {
        Calendar minDate = Calendar.getInstance();
        minDate.add(Calendar.DATE, 0);
        dpd.setMaxDate(minDate);
        assert getFragmentManager() != null;
        dpd.show(getFragmentManager(), "datePicker");
    }

    @Override
    public void positionClick(int position) {
        try {
            if (earningModels.get(position).getType().equalsIgnoreCase("Days")) {
                MovetoFragment(new ParticularDateYourTripFragment(earningModels.get(position).getDate()));
            } else {

                Page = 1;
                strTo = "";
                strfrom = "";
                sttype = earningModels.get(position).getType();
                strfromMonth = earningModels.get(position).getDate();
                earningModels.clear();
                getEarningList();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setAdapter() {

        if (earningModels != null && !earningModels.isEmpty()) {

            earningsAdapter = new EarningsAdapter(activity, this, earningModels);
            earningsRecycleview.setAdapter(earningsAdapter);
            nodataTxt.setVisibility(View.GONE);
            earningsRecycleview.setVisibility(View.VISIBLE);
        } else {
            earningsRecycleview.setVisibility(View.GONE);
            nodataTxt.setVisibility(View.VISIBLE);
        }
        setPagination();
    }

    @Override
    public void Onsuccess(Response<List<EarningModel>> Response) {
        System.out.println("enter the gson conversion in android"+new Gson().toJson(Response.body()));
        assert Response.body() != null;
        if (!Response.body().isEmpty()) {
            earningModels.addAll(Response.body());
            setAdapter();
        }

    }

    @Override
    public void onFailure(Response<List<EarningModel>> Response) {
        Utiles.showErrorMessage(new Gson().toJson(Response), activity, getView());
    }

    private void getEarningList() {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("_page", "" + Page);
        hashMap.put("from", strfrom);
        hashMap.put("to", strTo);
        hashMap.put("type", sttype);
        hashMap.put("fromMonth", strfromMonth);
        earningPresenter.getEanings(hashMap);
    }

    private void MovetoFragment(Fragment fragment) {
        try {
            fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                    .replace(R.id.yourcantainer, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setPagination() {
        earningsRecycleview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = LinearLayoutManagers.getChildCount();
                    totalItemCount = LinearLayoutManagers.getItemCount();
                    pastVisiblesItems = LinearLayoutManagers.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;

                            Page += 1;
                            getEarningList();
                            //Do pagination.. i.e. fetch new data
                        }
                    }
                }
            }
        });


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        System.out.println("monthofyear=" + monthOfYear);
        String month = "", day = "";
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
            System.out.println("day=" + day);
        } else {
            day = String.valueOf(dayOfMonth);
        }
        if (monthOfYear < 9) {
            month = "0" + (++monthOfYear);
            System.out.println("month=" + month);
        } else {
            month = String.valueOf(++monthOfYear);
        }
        //    String date = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        String date = year + "-" + month + "-" + day;
        System.out.println("new date of settext==" + date);
        //dateTxt.setText(month + "-" + day + "-" + year);
        //   strDate = date;
        if (IsFrom) {
            fromEdt.setText(date);
        } else {
            toEdt.setText(date);
        }
        if (!fromEdt.getText().toString().isEmpty() && !toEdt.getText().toString().isEmpty()) {
            strfrom = fromEdt.getText().toString();
            strTo = toEdt.getText().toString();
            Page = 1;
            earningModels.clear();
            sttype = "";
            strfromMonth = "";
            getEarningList();
        }
    }

}
