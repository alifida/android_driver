package com.auezeride.driver.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Objects;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.EventBus.DocumentUpload;
import com.auezeride.driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class ManagedocFragment extends BaseFragment {
    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.waring_img)
    ImageView waringImg;
    @BindView(R.id.img_expn)
    ImageView imgExpn;
    @BindView(R.id.missing_txt)
    TextView missingTxt;
    @BindView(R.id.driving_lyt)
    RelativeLayout drivingLyt;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.upload_btn)
    Button uploadBtn;
    @BindView(R.id.download_img)
    ImageView downloadImg;
    @BindView(R.id.manage_btn)
    Button manageBtn;
    @BindView(R.id.manag_lyt)
    LinearLayout managLyt;
    @BindView(R.id.upload_rlyt)
    RelativeLayout uploadRlyt;
    Unbinder unbinder;
    Activity activity;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.driving_txt)
    TextView drivingTxt;
    @BindView(R.id.ins_waring_img)
    ImageView insWaringImg;
    @BindView(R.id.ins_img_expn)
    ImageView insImgExpn;
    @BindView(R.id.ins_missing_txt)
    TextView insMissingTxt;
    @BindView(R.id.insurance_lyt)
    RelativeLayout insuranceLyt;
    @BindView(R.id.ins_view)
    View insView;
    @BindView(R.id.ins_upload_btn)
    Button insUploadBtn;
    @BindView(R.id.ins_download_img)
    ImageView insDownloadImg;
    @BindView(R.id.ins_manage_btn)
    Button insManageBtn;
    @BindView(R.id.ins_manag_lyt)
    LinearLayout insManagLyt;
    @BindView(R.id.ins_upload_rlyt)
    RelativeLayout insUploadRlyt;
    @BindView(R.id.taxi_txt)
    TextView taxiTxt;
    @BindView(R.id.taxi_waring_img)
    ImageView taxiWaringImg;
    @BindView(R.id.taxi_img_expn)
    ImageView taxiImgExpn;
    @BindView(R.id.taxi_missing_txt)
    TextView taxiMissingTxt;
    @BindView(R.id.taxi_lyt)
    RelativeLayout taxiLyt;
    @BindView(R.id.taxi_view)
    View taxiView;
    @BindView(R.id.taxi_upload_btn)
    Button taxiUploadBtn;
    @BindView(R.id.taxi_download_img)
    ImageView taxiDownloadImg;
    @BindView(R.id.taxi_manage_btn)
    Button taxiManageBtn;
    @BindView(R.id.taxi_manag_lyt)
    LinearLayout taxiManagLyt;
    @BindView(R.id.taxi_upload_rlyt)
    RelativeLayout taxiUploadRlyt;

    Fragment fragment;
    @BindView(R.id.next_btn)
    Button nextBtn;

    String flow = "";
    Context context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_manage_doc, null, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();

        FontChangeCrawler fontChangeCrawler = new FontChangeCrawler(Objects.requireNonNull(getActivity()).getAssets(), getString(R.string.app_font));
        fontChangeCrawler.replaceFonts(getActivity().findViewById(android.R.id.content));

        SharedHelper.putKey(Objects.requireNonNull(getContext()), "docStatus", "3");

        flow = SharedHelper.getKey(getContext(), "appflow");

        if (flow.equalsIgnoreCase("registration")) {
            nextBtn.setVisibility(View.VISIBLE);
        } else {
            nextBtn.setVisibility(View.GONE);
        }
        CheckdocumentUpload();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();

    }

    @OnClick({R.id.back_img, R.id.driving_lyt, R.id.upload_btn, R.id.download_img, R.id.manage_btn, R.id.insurance_lyt, R.id.ins_upload_btn, R.id.ins_download_img, R.id.ins_manage_btn, R.id.taxi_lyt, R.id.taxi_upload_btn, R.id.taxi_download_img, R.id.taxi_manage_btn, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.driving_lyt:
                licenceVisiblity();
                break;
            case R.id.upload_btn:
                fragment = new UploadDocFragment();
                moveToFragment(fragment, "licence");
                CommonData.Documenttype = "licence";
                SharedHelper.putKey(Objects.requireNonNull(getContext()), "docStatus", "1");
                break;
            case R.id.download_img:
                //Utiles.CommonToast(activity, "download");
                break;
            case R.id.manage_btn:
                fragment = new UploadDocFragment();
                CommonData.Documenttype = "licence";
                moveToFragment(fragment, "licence");
                break;

            case R.id.insurance_lyt:
                insuranceVisiblity();
                break;
            case R.id.ins_upload_btn:
                fragment = new UploadDocFragment();
                moveToFragment(fragment, "insurance");
                CommonData.Documenttype = "insurance";
                SharedHelper.putKey(Objects.requireNonNull(getContext()), "docStatus", "2");
                break;
            case R.id.ins_download_img:
             //   Utiles.CommonToast(activity, "download");
                break;
            case R.id.ins_manage_btn:
                fragment = new UploadDocFragment();
                moveToFragment(fragment, "insurance");
                CommonData.Documenttype = "insurance";
                break;
            case R.id.taxi_lyt:
                taxiVisiblity();
                break;
            case R.id.taxi_upload_btn:
                fragment = new UploadDocFragment();
                moveToFragment(fragment, "taxi");
                CommonData.Documenttype = "passing";
                SharedHelper.putKey(Objects.requireNonNull(getContext()), "docStatus", "3");
                break;
            case R.id.taxi_download_img:
              //  Utiles.CommonToast(activity, "download");
                break;
            case R.id.taxi_manage_btn:
                fragment = new UploadDocFragment();
                moveToFragment(fragment, "taxi");
                CommonData.Documenttype = "passing";
                break;
            case R.id.next_btn:
                if (!SharedHelper.getKey(Objects.requireNonNull(getContext()), "docStatus").equalsIgnoreCase("3")) {
                    Utiles.CommonToast(getActivity(), "Oops! Some document has pending ,Please upload all document");
                } else {
                    fragment = new AddVehicleFragment();
                    moveToFragment(fragment, "addVehicle");
                }
                break;
        }
    }

    private void licenceVisiblity() {
        if (uploadRlyt.getVisibility() == View.VISIBLE) {
            view.setVisibility(View.GONE);
            uploadRlyt.setVisibility(View.GONE);
            imgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand));
        } else {
            view.setVisibility(View.VISIBLE);
            uploadRlyt.setVisibility(View.VISIBLE);
            imgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less));
            if (missingTxt.getVisibility() == View.VISIBLE) {
                uploadBtn.setVisibility(View.VISIBLE);
                managLyt.setVisibility(View.GONE);
                waringImg.setVisibility(View.VISIBLE);
            } else {
                uploadBtn.setVisibility(View.GONE);
                managLyt.setVisibility(View.VISIBLE);
                waringImg.setVisibility(View.GONE);
            }
        }
    }

    private void insuranceVisiblity() {
        if (insUploadRlyt.getVisibility() == View.VISIBLE) {
            insView.setVisibility(View.GONE);
            insUploadRlyt.setVisibility(View.GONE);
            insImgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand));
        } else {
            insView.setVisibility(View.VISIBLE);
            insUploadRlyt.setVisibility(View.VISIBLE);
            insImgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less));
            //  insMissingTxt.setVisibility(View.GONE);
            if (insMissingTxt.getVisibility() == View.VISIBLE) {
                insUploadBtn.setVisibility(View.VISIBLE);
                insManagLyt.setVisibility(View.GONE);
                insWaringImg.setVisibility(View.VISIBLE);
            } else {
                insUploadBtn.setVisibility(View.GONE);
                insManagLyt.setVisibility(View.VISIBLE);
                insWaringImg.setVisibility(View.GONE);
            }
        }
    }

    private void taxiVisiblity() {
        if (taxiUploadRlyt.getVisibility() == View.VISIBLE) {
            taxiView.setVisibility(View.GONE);
            taxiUploadRlyt.setVisibility(View.GONE);
            taxiImgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand));
        } else {
            taxiView.setVisibility(View.VISIBLE);
            taxiUploadRlyt.setVisibility(View.VISIBLE);
            taxiImgExpn.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less));
            if (taxiMissingTxt.getVisibility() == View.VISIBLE) {
                taxiUploadBtn.setVisibility(View.VISIBLE);
                taxiManagLyt.setVisibility(View.GONE);
                taxiWaringImg.setVisibility(View.VISIBLE);
            } else {
                taxiUploadBtn.setVisibility(View.GONE);
                taxiManagLyt.setVisibility(View.VISIBLE);
                taxiWaringImg.setVisibility(View.GONE);
            }
        }
    }

    private void moveToFragment(Fragment fragment, String page) {

        if (flow.equalsIgnoreCase("registration")) {
            if(!page.equalsIgnoreCase("addVehicle")){
                Bundle bundle = new Bundle();
                bundle.putString("page", page);
                fragment.setArguments(bundle);
            }
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.containter, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        } else {
            Bundle bundle = new Bundle();
            bundle.putString("page", page);
            fragment.setArguments(bundle);
            Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                    .replace(R.id.drawer_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessage(DocumentUpload event) {
        DocumentUpload(event);
        EventBus.getDefault().removeStickyEvent(DocumentUpload.class); // don't forget to remove the sticky event if youre done with it
    }

    private void DocumentUpload(DocumentUpload Document) {
        switch (CommonData.Documenttype) {
            case "licence":
                SharedHelper.putKey(context, "licence", Document.getImageurl());
                SharedHelper.putKey(context, "licence_date", Document.getStrdate());
                missingTxt.setVisibility(View.GONE);
                uploadBtn.setVisibility(View.VISIBLE);
                managLyt.setVisibility(View.GONE);
                waringImg.setVisibility(View.VISIBLE);
                break;
            case "insurance":
                SharedHelper.putKey(context, "insurance", Document.getImageurl());
                SharedHelper.putKey(context, "insurance_date", Document.getStrdate());
                insMissingTxt.setVisibility(View.GONE);
                insUploadBtn.setVisibility(View.VISIBLE);
                insManagLyt.setVisibility(View.GONE);
                insWaringImg.setVisibility(View.VISIBLE);
                break;
            case "passing":
                SharedHelper.putKey(context, "passing", Document.getImageurl());
                SharedHelper.putKey(context, "passing_date", Document.getStrdate());
                taxiMissingTxt.setVisibility(View.GONE);
                taxiUploadBtn.setVisibility(View.VISIBLE);
                taxiManagLyt.setVisibility(View.GONE);
                taxiWaringImg.setVisibility(View.VISIBLE);
                break;

        }
        CheckdocumentUpload();
    }

    public void CheckdocumentUpload() {
        if (SharedHelper.getKey(context, "licence") != null && !SharedHelper.getKey(context, "licence").isEmpty()) {
            missingTxt.setVisibility(View.GONE);
            waringImg.setVisibility(View.GONE);
            Utiles.Documentimg(SharedHelper.getKey(context, "licence"), downloadImg, activity);
        }
        if (SharedHelper.getKey(context, "insurance") != null && !SharedHelper.getKey(context, "insurance").isEmpty()) {
            insMissingTxt.setVisibility(View.GONE);
            insWaringImg.setVisibility(View.GONE);
            Utiles.Documentimg(SharedHelper.getKey(context, "insurance"), insDownloadImg, activity);
        }
        if (SharedHelper.getKey(context, "passing") != null && !SharedHelper.getKey(context, "passing").isEmpty()) {
            taxiMissingTxt.setVisibility(View.GONE);
            taxiWaringImg.setVisibility(View.GONE);
            Utiles.Documentimg(SharedHelper.getKey(context, "licence"), taxiDownloadImg, activity);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
}