package com.auezeride.driver.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.EventBus.RequestEvent;
import com.auezeride.driver.MainActivity;
import com.auezeride.driver.Model.AddVehicleModel;
import com.auezeride.driver.Model.CarModel;
import com.auezeride.driver.Model.DriverProfileModel;
import com.auezeride.driver.Model.ServiceModel;
import com.auezeride.driver.Presenter.CarModelPresenter;
import com.auezeride.driver.Presenter.DriverProfilePresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.CarModelView;
import com.auezeride.driver.View.ProfileView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

public class AddVehicleFragment extends BaseFragment implements Validator.ValidationListener, CarModelView, ProfileView {

    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.add_img)
    ImageView addImg;
    //  @NotEmpty(message = getString(R.string.select_make))
    @NotEmpty(message = "Select Make")
    @BindView(R.id.make_edt)
    MaterialEditText makeEdt;
    @NotEmpty(message = "Select Model")
    // @NotEmpty(message = getString(R.string.select_model))
    @BindView(R.id.model_edt)
    MaterialEditText modelEdt;
    //  @NotEmpty(message = getString(R.string.select_year))
    @NotEmpty(message = "Select Year")
    @BindView(R.id.year_edt)
    MaterialEditText yearEdt;
    @BindView(R.id.submit_btn)
    Button submitBtn;
    Unbinder unbinder;
    @BindView(R.id.header)
    RelativeLayout header;
    // @NotEmpty(message = getString(R.string.select_licence_plate))
    @NotEmpty(message = "")
    @BindView(R.id.licence_edt)
    MaterialEditText licenceEdt;
    @NotEmpty(message = "Enter your Vehicle color")
    // @NotEmpty(message = getString(R.string.enter_your_vehicle_Color))
    @BindView(R.id.color_edt)
    MaterialEditText colorEdt;

    List<String> CarModel = null;

    Validator validator;

    int basic = 0, normal = 0, lux = 0;
    @BindView(R.id.basci_lyt)
    RelativeLayout basciLyt;
    @BindView(R.id.normal_lyt)
    RelativeLayout normalLyt;
    @BindView(R.id.lux_lyt)
    RelativeLayout luxLyt;
    @BindView(R.id.basic_check)
    CheckBox basicCheck;
    @BindView(R.id.basic_normal)
    CheckBox basicNormal;
    @BindView(R.id.lux_check)
    CheckBox luxCheck;

    List<com.auezeride.driver.Model.CarModel> carModels;

    public AlertDialog.Builder alert;
    public AlertDialog alert11;
    AlertDialog vehicleAlert;
    Activity activity;
    Context context;
    static String strYear, strmake, strModel;
    CarModelPresenter carModelPresenter;
    @BindView(R.id.handcap_check)
    CheckBox handcapCheck;

    String str_makeid, str_model, str_year, str_licence, str_color, str_handi, str_basic, str_normal, str_lux;
    Bundle bundle;
    Fragment fragment;
    @BindView(R.id.service_type_spinner)
    Spinner serviceTypeSpinner;
    CompositeDisposable disposable;
    private String strServiceType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_addvehicle, null, false);

        unbinder = ButterKnife.bind(this, view);
        validator = new Validator(this);
        validator.setValidationListener(this);
        activity = getActivity();
        disposable = new CompositeDisposable();
        carModelPresenter = new CarModelPresenter(this, disposable);
        carModelPresenter.getCarModel(activity);


        context = getContext();

        FontChangeCrawler fontChangeCrawler = new FontChangeCrawler(getActivity().getAssets(), getString(R.string.app_font));
        fontChangeCrawler.replaceFonts(getActivity().findViewById(android.R.id.content));

        luxCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (luxCheck.isChecked()) {
                lux = 1;
            } else {
                lux = 0;
            }
        });
        carModelPresenter.getservicetype(activity);
        basicNormal.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (basicNormal.isChecked()) {
                normal = 1;
            } else {
                normal = 0;
            }
        });

        basicCheck.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (basicCheck.isChecked()) {
                basic = 1;
            } else {
                basic = 0;
            }
        });
        //  serviceTypeSpinner.setEnabled(false);
        //serviceTypeSpinner.setClickable(false);
        bundle = this.getArguments();
        if (bundle != null) {
            str_makeid = bundle.getString("makeid");
            strmake = bundle.getString("makename");
            str_model = bundle.getString("model");
            str_licence = bundle.getString("licence");
            str_year = bundle.getString("year");
            str_color = bundle.getString("color");
            str_handi = bundle.getString("handicap");
            strServiceType = bundle.getString("service_type");

            makeEdt.setText(strmake);
            modelEdt.setText(str_model);
            yearEdt.setText(str_year);
            licenceEdt.setText(str_licence);
            colorEdt.setText(str_color);
         /*   if (!str_handi.equals("false")) {
                handcapCheck.setChecked(true);
            } else {
                handcapCheck.setChecked(false);
            }
            if (str_basic.equals("true")) {
                basicCheck.setChecked(true);
            }
            if (str_lux.equals("true")) {
                luxCheck.setChecked(true);
            }
            if (str_normal.equals("true")) {
                basicNormal.setChecked(true);
            }*/
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        Utiles.clearInstance();
        try {
            Utiles.compositeClreate(disposable);
            if (vehicleAlert != null && vehicleAlert.isShowing()) {
                vehicleAlert.dismiss();
            }
            if (alert11 != null && alert11.isShowing()) {
                alert11.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick({R.id.back_img, R.id.make_edt, R.id.model_edt, R.id.year_edt, R.id.licence_edt, R.id.submit_btn, R.id.basci_lyt, R.id.normal_lyt, R.id.lux_lyt, R.id.basic_check, R.id.basic_normal, R.id.lux_check})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                Utiles.hideKeyboard(activity);
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.make_edt:
                vehicleDoc("Select Make", 0);

                break;
            case R.id.model_edt:
                if (makeEdt.getText().toString().isEmpty()) {
                    showErrorMessage("Please select Vehicle Make");
                } else {
                    vehicleDoc("Select Model", 1);
                }

                break;
            case R.id.year_edt:
                vehicleDoc("Select Year", 2);
                break;
            case R.id.licence_edt:
                Utiles.CommonToast(getActivity(), "licence");
                break;
            case R.id.submit_btn:
                validator.validate();
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        Utiles.hideKeyboard(activity);
       /* if (basic == 0 && normal == 0 && lux == 0) {
            showErrorMessage("You must select at least one car type");
        } else {*/
        HashMap<String, String> map = new HashMap<>();
        map.put("makename", makeEdt.getText().toString());
        map.put("model", modelEdt.getText().toString());
        map.put("year", yearEdt.getText().toString());
        map.put("licence", licenceEdt.getText().toString());
        map.put("cpy", "");
        map.put("driver", SharedHelper.getKey(context, "userid"));
        map.put("color", colorEdt.getText().toString());
        map.put("vehicletype", strServiceType);
        if (bundle != null) {
            map.put("makeid", str_makeid);
        }
           /* if (handcapCheck.isChecked()) {
                map.put("handicap", "true");
            } else {
                map.put("handicap", "false");
            }
            if (basicCheck.isChecked()) {
                map.put("basic", "true");
            } else {
                map.put("basic", "false");
            }
            if (basicNormal.isChecked()) {
                map.put("normal", "true");
            } else {
                map.put("normal", "false");
            }
            if (luxCheck.isChecked()) {
                map.put("luxury", "true");
            } else {
                map.put("luxury", "false");
            }*/

        if (bundle != null) {
            Alertdialog(activity.getResources().getString(R.string.are_you_update), true, map);

        } else {
            Alertdialog(activity.getResources().getString(R.string.are_you_sure_add), false, map);

        }
        /*}*/
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                switch (view.getId()) {
                    case R.id.make_edt:
                        ((EditText) view).setError(context.getString(R.string.select_make));

                        break;
                    case R.id.model_edt:
                        ((EditText) view).setError(context.getString(R.string.select_model));

                        break;
                    case R.id.year_edt:
                        ((EditText) view).setError(context.getString(R.string.select_year));

                        break;
                    case R.id.licence_edt:
                        ((EditText) view).setError(context.getString(R.string.select_licence_plate));

                        break;
                    case R.id.color_edt:
                        ((EditText) view).setError(context.getString(R.string.enter_your_vehicle_Color));
                        break;

                }

            } else {
                Utiles.CommonToast(getActivity(), message);
            }
        }
    }

    private void showErrorMessage(String error) {
        Snackbar snackbar = Snackbar
                .make(Objects.requireNonNull(getActivity()).findViewById(android.R.id.content), error, Snackbar.LENGTH_SHORT);
        View snackbarView = snackbar.getView();
        TextView textView = snackbarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbarView.setBackgroundColor(Color.BLACK);
        snackbar.show();
    }

    private void vehicleDoc(String title, int status) {
        final LayoutInflater inflater = (LayoutInflater) Objects.requireNonNull(getActivity()).getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.alert_doc, null);

        RecyclerView rclr_datas = layout.findViewById(R.id.rclr_datas);

        @SuppressLint("WrongConstant") LinearLayoutManager layoutManagershops
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rclr_datas.setLayoutManager(layoutManagershops);

        DocAdapter docAdapter = new DocAdapter(getContext(), status, carModels);
        rclr_datas.setAdapter(docAdapter);

        alert = new AlertDialog.Builder(getActivity());
        alert.setTitle(title);
        alert.setView(layout);
        alert.setCancelable(true);
        alert11 = alert.create();
        alert11.show();


    }

    @Override
    public void OnSuccessfullsy(Response<CarModel> Response) {
        carModels = new ArrayList<>();
        carModels.add(Response.body());

    }

    @Override
    public void OnFailurse(Response<CarModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void AddvehicleSucessfully(Response<AddVehicleModel> response) {
        assert response.body() != null;
        Utiles.CommonToast(activity, response.body().getMessage());
        if (SharedHelper.getKey(context, "appflow").equalsIgnoreCase("login")) {
            assert getFragmentManager() != null;
            EventBus.getDefault().postSticky(new RequestEvent(""));
            getFragmentManager().popBackStackImmediate();
        } else {

            fragment = new AddVehicleDocFragment();
            Bundle bundle = new Bundle();
            bundle.putString("makeid", response.body().getTaxi().getId());
            fragment.setArguments(bundle);
            moveToFragment(fragment);
        }
    }

    @Override
    public void AddvehicleFailure(Response<AddVehicleModel> response) {
        try {
            assert response.errorBody() != null;
            String Message = response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void editVehicleSuccessfull() {
        assert getFragmentManager() != null;
        EventBus.getDefault().postSticky(new RequestEvent(""));
        getFragmentManager().popBackStackImmediate();
    }

    @Override
    public void editVehicleFauiler() {
        Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
    }

    @Override
    public void onSuccessServiceList(ServiceModel serviceModels) {
        getServiceTypeSPinner(serviceModels.getVehiclelists());
    }

    private void getServiceTypeSPinner(List<String> vehiclelists) {
        if (vehiclelists != null) {
            ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(activity,
                    R.layout.dropdown, vehiclelists);
            languageAdapter.setDropDownViewResource(R.layout.dropdown);
            serviceTypeSpinner.setAdapter(languageAdapter);
            int spinnerPosition = languageAdapter.getPosition(Utiles.Nullpointer(strServiceType));
            serviceTypeSpinner.setSelection(spinnerPosition);
            serviceTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    strServiceType = adapterView.getItemAtPosition(position).toString();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
    }


    @Override
    public void onFailureService(Throwable throwable) {

        try {
            HttpException error = (HttpException) throwable;
            String errorBody = error.response().errorBody().string();
            JSONObject jsonObject = new JSONObject(errorBody);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }
        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void OnSuccessfully(Response<List<DriverProfileModel>> Response) {
        SharedHelper.getKey(context, "appflow").equalsIgnoreCase("login");
        List<DriverProfileModel> driverProfileModels = Response.body();
        SharedHelper.putKey(context, "login_status", "true");
        SharedHelper.putKey(context, "fname", driverProfileModels.get(0).getFname());
        SharedHelper.putKey(context, "lname", driverProfileModels.get(0).getLname());
        SharedHelper.putKey(context, "address", driverProfileModels.get(0).getAddress());
        SharedHelper.putKey(context, "email", driverProfileModels.get(0).getEmail());
        SharedHelper.putKey(context, "phcode", driverProfileModels.get(0).getPhcode());
        SharedHelper.putKey(context, "phone", driverProfileModels.get(0).getPhone());
        SharedHelper.putKey(context, "lang", driverProfileModels.get(0).getLang());
        SharedHelper.putKey(context, "cur", driverProfileModels.get(0).getCur());
        SharedHelper.putKey(context, "code", driverProfileModels.get(0).getCode());
        SharedHelper.putKey(context, "filepath", driverProfileModels.get(0).getBaseurl());
        if (Utiles.IsNull(driverProfileModels.get(0).getLicence())) {
            SharedHelper.putKey(context, "licence", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getLicence());

        }
        if (Utiles.IsNull(driverProfileModels.get(0).getAadhar())) {
            SharedHelper.putKey(context, "insurance", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getAadhar());

        }
       /* if (Utiles.IsNull(driverProfileModels.get(0).getPassing())) {
            SharedHelper.putKey(context, "passing", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getPassing());

        }*/
        SharedHelper.putKey(context, "licence_date", driverProfileModels.get(0).getLicenceexp());
        SharedHelper.putKey(context, "insurance_date", driverProfileModels.get(0).getAadharexp());
        // SharedHelper.putKey(context, "passing_date", driverProfileModels.get(0).getPassingexp());
        Constants.WalletAlertEnable = driverProfileModels.get(3).getIsDriverCreditModuleEnabledForUseAfterLogin();

        SharedHelper.putKey(context, "profile", driverProfileModels.get(1).getProfileurl());
        SharedHelper.putKey(context, "vehicleId", driverProfileModels.get(2).getCurrentActiveTaxi().getId());
        SharedHelper.putKey(context, "vmake", driverProfileModels.get(2).getCurrentActiveTaxi().getMakename());
        SharedHelper.putKey(context, "vmodel", driverProfileModels.get(2).getCurrentActiveTaxi().getModel());
        SharedHelper.putKey(context, "numplate", driverProfileModels.get(2).getCurrentActiveTaxi().getLicence());

        SharedHelper.putKey(getContext(), "appflow", "login");
        Intent i = new Intent(context, MainActivity.class);
        activity.startActivity(i);
        activity.finish();
    }

    @Override
    public void OnFailure(Response<List<DriverProfileModel>> Response) {
        Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
    }

    public class DocAdapter extends RecyclerView.Adapter<DocAdapter.ViewHolder> {
        int Size = 0;
        Context context;
        int status;
        List<CarModel> carModels;
        CarModel carModel;

        public DocAdapter(Context context, int status, List<CarModel> carModels) {
            this.context = context;
            this.status = status;
            this.carModels = carModels;

        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.adapter_doc_data, parent, false);
            FontChangeCrawler fontChangeCrawler = new FontChangeCrawler(Objects.requireNonNull(getActivity()).getAssets(), getString(R.string.app_font));
            fontChangeCrawler.replaceFonts(getActivity().findViewById(android.R.id.content));

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
            if (status == 0) {
                carModel = carModels.get(0);
                holder.data_txt.setText(carModel.getCarmake().get(0).getDatas().get(position).getMake());
            } else if (status == 1) {
                holder.data_txt.setText(CarModel.get(position));
            } else if (status == 2) {
                carModel = carModels.get(0);
                holder.data_txt.setText(carModel.getYears().get(0).getDatas().get(position).getName());
            }

            holder.data_txt.setTag(position);
            holder.data_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert11.dismiss();
                    int pos = (int) v.getTag();


                    if (status == 0) {
                        strmake = carModel.getCarmake().get(0).getDatas().get(pos).getMake();
                        makeEdt.setText(holder.data_txt.getText().toString());
                    } else if (status == 1) {
                        strModel = CarModel.get(pos);
                        modelEdt.setText(holder.data_txt.getText().toString());
                    } else if (status == 2) {
                        strYear = carModel.getYears().get(0).getDatas().get(pos).getName();
                        yearEdt.setText(holder.data_txt.getText().toString());
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            if (carModels != null) {
                if (status == 0) {
                    Size = carModels.get(0).getCarmake().get(0).getDatas().size();
                } else if (status == 1) {
                    if (strmake != null) {
                        for (int i = 0; i < carModels.get(0).getCarmake().get(0).getDatas().size(); i++) {
                            if (strmake.equalsIgnoreCase(carModels.get(0).getCarmake().get(0).getDatas().get(i).getMake())) {
                                Size = carModels.get(0).getCarmake().get(0).getDatas().get(i).getModel().size();
                                CarModel = carModels.get(0).getCarmake().get(0).getDatas().get(i).getModel();
                            }

                        }

                    }

                } else if (status == 2) {
                    Size = carModels.get(0).getYears().get(0).getDatas().size();
                }
            }
            return Size;
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            TextView data_txt;

            ViewHolder(View view) {
                super(view);

                data_txt = view.findViewById(R.id.data_txt);

            }
        }

    }

    private void moveToFragment(Fragment fragment) {
        if (SharedHelper.getKey(getContext(), "appflow").equalsIgnoreCase("login")) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.add_vehilce_container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();
        } else {
            DriverProfilePresenter driverProfilePresenter = new DriverProfilePresenter(this);
            driverProfilePresenter.getProfile(activity, true);
        }
    }

    public void Alertdialog(String Message, final Boolean status, final HashMap<String, String> map) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                activity.getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (status) {
                            carModelPresenter.geteditVehicle(activity, map);
                        } else {
                            carModelPresenter.getAddVehicle(activity, map);
                        }

                        dialog.cancel();

                    }
                });
        builder1.setNegativeButton(activity.getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });
        vehicleAlert = builder1.create();
        vehicleAlert.show();


    }

}
