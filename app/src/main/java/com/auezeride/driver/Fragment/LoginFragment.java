package com.auezeride.driver.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.JWTUtils;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.MainActivity;
import com.auezeride.driver.Model.DriverProfileModel;
import com.auezeride.driver.Model.LoginModel;
import com.auezeride.driver.Presenter.DriverProfilePresenter;
import com.auezeride.driver.Presenter.LoginPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.Loginview;
import com.auezeride.driver.View.ProfileView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class LoginFragment extends BaseFragment implements Loginview, Validator.ValidationListener, ProfileView {

    @NotEmpty(message = "Enter the Driver code or Mobile Number")
    //@NotEmpty(message = getString(R.string.enter_the_driver_code))
    @BindView(R.id.email_edt)
    EditText emailEdt;

    //@NotEmpty(message = getString(R.string.enter_password))
    @NotEmpty(message = "Enter the Password")
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.fpassword_txt)
    TextView fpasswordTxt;
    Unbinder unbinder;

    public LoginFragment() {
        // Required empty public constructor
    }

    Context context;
   Activity activity;
    Validator validator;
    CountryPicker picker;
    String strCountryCountry = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();
        CommonData.walletBalance = "0";
        validator = new Validator(this);
        validator.setValidationListener(this);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));

        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                strCountryCountry = dialCode;
                picker.dismiss();
                LoginApiCall();
                // Implement your code here
            }
        });
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();

    }


    @OnClick({R.id.login_btn, R.id.fpassword_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                //startActivity(new Intent(context, MainActivity.class));
                validator.validate();
                break;
            case R.id.fpassword_txt:
                Fragment fragment = new ForgotPasswordFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.login_fragment, fragment);
                fragmentTransaction.addToBackStack("ForgetPassword");
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void LoginVIew(Response<LoginModel> Response) {
        try {
            SharedHelper.putKey(getContext(), "token", Response.body().getToken());
            new JWTUtils(null, this).decoded(Response.body().getToken(), context, "login");
            Log.e("tokentoken", "" + Response.body().getToken());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Errorlogview(Response<LoginModel> Response) {
        try {
            String Message = Response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void JsonResponse(String object) {
        try {
            JSONObject obj = new JSONObject(object);
            String email = obj.getString("email");
            String name = obj.getString("name");
            String id = obj.getString("id");

            Log.e("response", "" + email + "==" + name + "==" + id);
            SharedHelper.putKey(context, "login_status", "true");
            SharedHelper.putKey(getContext(), "userid", id);
            SharedHelper.putKey(getContext(), "email", email);
            SharedHelper.putKey(getContext(), "appflow", "login");
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("drivers_data").child(id).child("FCM_id").setValue(SharedHelper.getToken(context, "device_token"));
            ProfilePresenter();
            Utiles.ClearFirebase(activity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void ProfilePresenter() {
        DriverProfilePresenter driverProfilePresenter = new DriverProfilePresenter(this);
        driverProfilePresenter.getProfile(activity, true);
    }

    @Override
    public void onValidationSucceeded() {
        if (checkForMobile(emailEdt.getText().length())) {
            strCountryCountry = "+91";
            LoginApiCall();
        } else {
            LoginApiCall();
        }
        //  startActivity(new Intent(context, MainActivity.class));
    }

    public boolean checkForMobile(int NumberProto) {
        return NumberProto >= 10;
    }

    public void LoginApiCall() {
        Utiles.hideKeyboard(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put("username", emailEdt.getText().toString());
        map.put("password", passwordEdt.getText().toString());
        map.put("phcode", strCountryCountry);
        map.put("fcmId", SharedHelper.getToken(context, "device_token"));
        Log.e("tage", "Login url" + map);
        LoginPresenter loginPresenter = new LoginPresenter(this);
        loginPresenter.getLogin(map, activity);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                switch (view.getId()){
                    case R.id.email_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_email));
                        break;
                    case R.id.password_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_password));
                        break;
                }
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void OnSuccessfully(Response<List<DriverProfileModel>> Response) {
        List<DriverProfileModel> driverProfileModels = Response.body();
        assert driverProfileModels != null;
        SharedHelper.putKey(context, "fname", driverProfileModels.get(0).getFname());
        SharedHelper.putKey(context, "lname", driverProfileModels.get(0).getLname());
        SharedHelper.putKey(context, "address", driverProfileModels.get(0).getAddress());
        SharedHelper.putKey(context, "email", driverProfileModels.get(0).getEmail());
        SharedHelper.putKey(context, "phcode", driverProfileModels.get(0).getPhcode());
        SharedHelper.putKey(context, "phone", driverProfileModels.get(0).getPhone());
        SharedHelper.putKey(context, "lang", driverProfileModels.get(0).getLang());
        SharedHelper.putKey(context, "cur", driverProfileModels.get(0).getCur());
        SharedHelper.putKey(context, "code", driverProfileModels.get(0).getCode());
        SharedHelper.putKey(context, "filepath", driverProfileModels.get(0).getBaseurl());
        SharedHelper.putOnline(activity, "isconnect", driverProfileModels.get(0).getConnected());
        if (Utiles.IsNull(driverProfileModels.get(0).getLicence())) {
            SharedHelper.putKey(context, "licence", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getLicence());

        }
        if (Utiles.IsNull(driverProfileModels.get(0).getAadhar())) {
            SharedHelper.putKey(context, "insurance", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getAadhar());

        }
       /* if (Utiles.IsNull(driverProfileModels.get(0).getPassing())) {
            SharedHelper.putKey(context, "passing", driverProfileModels.get(0).getBaseurl() + driverProfileModels.get(0).getPassing());

        }*/
        SharedHelper.putKey(context, "licence_date", driverProfileModels.get(0).getLicenceexp());
        SharedHelper.putKey(context, "insurance_date", driverProfileModels.get(0).getAadharexp());
        //  SharedHelper.putKey(context, "passing_date", driverProfileModels.get(0).getPassingexp());
        Constants.WalletAlertEnable = driverProfileModels.get(3).getIsDriverCreditModuleEnabledForUseAfterLogin();

        SharedHelper.putKey(context, "profile", driverProfileModels.get(1).getProfileurl());
        if (driverProfileModels.get(2).getCurrentActiveTaxi() != null) {
            SharedHelper.putKey(context, "vehicleId", driverProfileModels.get(2).getCurrentActiveTaxi().getId());
            SharedHelper.putKey(context, "vmake", driverProfileModels.get(2).getCurrentActiveTaxi().getMakename());
            SharedHelper.putKey(context, "vmodel", driverProfileModels.get(2).getCurrentActiveTaxi().getModel());
            SharedHelper.putKey(context, "numplate", driverProfileModels.get(2).getCurrentActiveTaxi().getLicence());
            Intent i = new Intent(context, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            activity.finish();
        } else {
            Alertdialog();
        }
    }

    @Override
    public void OnFailure(Response<List<DriverProfileModel>> Response) {
        try {
            String Message = Response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }
    }

    public void Alertdialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(R.string.please_added_vehicle);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                activity.getResources().getString(R.string.ok),
                (dialog, id) -> {
                    SharedHelper.putKey(getContext(), "appflow", "registration");
                    Fragment fragment = new AddVehicleFragment();
                    moveToFragment(fragment);
                    dialog.cancel();

                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void moveToFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(R.id.login_fragment, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

    }
}