package com.auezeride.driver.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class SupportFragment extends BaseFragment {


    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.about_us_linear)
    LinearLayout aboutUsLinear;
    @BindView(R.id.privacy_policy_linear)
    LinearLayout privacyPolicyLinear;
    @BindView(R.id.terms_codition_linear)
    LinearLayout termsCoditionLinear;
    @BindView(R.id.containter)
    FrameLayout containter;
    Unbinder unbinder;

    private Activity activity;
    private Context context;
    private FragmentManager fragmentManager;
    private Fragment fragment;

    public SupportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_support, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        fragmentManager = getFragmentManager();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
    @Optional
    @OnClick({R.id.back_img, R.id.about_us_linear, R.id.privacy_policy_linear, R.id.terms_codition_linear})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.about_us_linear:
                CommonData.strTitle = context.getResources().getString(R.string.about_us);
                fragment = new CommonwebviewFragment();
                FragmentCalling(fragment);
                break;
            case R.id.privacy_policy_linear:
                CommonData.strTitle = context.getResources().getString(R.string.privacy_policy);
                fragment = new CommonwebviewFragment();
                FragmentCalling(fragment);
                break;
            case R.id.terms_codition_linear:
                CommonData.strTitle = context.getResources().getString(R.string.terms_amp_condition);
                fragment = new CommonwebviewFragment();
                FragmentCalling(fragment);
                break;
        }
    }

    private void FragmentCalling(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
