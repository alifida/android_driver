package com.auezeride.driver.Fragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.ybs.countrypicker.CountryPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import com.auezeride.driver.Apllicationcontroller.MySMSBroadcastReceiver;
import com.auezeride.driver.CommonClass.BaseFragment;
import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.JWTUtils;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.CustomizeDialog.OTPCustomerDialog;
import com.auezeride.driver.Model.OTPModel;
import com.auezeride.driver.Model.RegisterModel;
import com.auezeride.driver.Presenter.RegisterPresenter;
import com.auezeride.driver.R;
import com.auezeride.driver.View.RegisterView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class RegisterFragment extends BaseFragment implements RegisterView, Validator.ValidationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.back_img)
    ImageButton backImg;
    @NotEmpty(message = "")
    //  @Length(min = 4, message = getString(R.string.minimum_four))
    @Length(min = 4, message = "Enter  Minimum 4 character")
    @BindView(R.id.fname_edit)
    MaterialEditText fnameEdit;
    @NotEmpty(message = "")
    @Length(min = 4, message = "Enter  Minimum 4 character")
    @BindView(R.id.lname_edt)
    MaterialEditText lnameEdt;
    @NotEmpty(message = "")
    @Email(message = "Enter Valid Email id")
    @BindView(R.id.email_edt)
    MaterialEditText emailEdt;
    @NotEmpty(message = "Enter Your Password")
    @BindView(R.id.password_edt)
    MaterialEditText passwordEdt;
    @BindView(R.id.cc_edt)
    MaterialEditText ccEdt;
    @NotEmpty(message = "")
    // @Length(min = 10, message = getString(R.string.enter_the_10_digit))
    @Length(min = 6, max = 15, message = "Enter the Minimum 6 to 15 Character")
    @BindView(R.id.mobile_edt)
    MaterialEditText mobileEdt;
    @BindView(R.id.referral_edt)
    MaterialEditText referralEdt;
    @NotEmpty(message = "Enter Your Address")
    @BindView(R.id.address)
    MaterialEditText address;
    @BindView(R.id.terms_condition_txt)
    CheckBox termsConditionTxt;
    @BindView(R.id.submit)
    Button submit;
    Unbinder unbinder;
    Context context;

    public RegisterFragment() {
    }

    private Validator validator;
    Activity activity;
    private CountryPicker picker;
    Fragment fragment;
    private RegisterPresenter registerPresenter;
    private GoogleApiClient client;
    private MySMSBroadcastReceiver smsBroadcast;
    private int RC_HINT = 2;
    private FragmentActivity fragmentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();
        fragmentActivity = getActivity();
        validator = new Validator(this);
        validator.setValidationListener(this);
        picker = CountryPicker.newInstance("Select Country");
        picker.setListener((name, code, dialCode, flagDrawableResID) -> {
            ccEdt.setText(dialCode);
            picker.dismiss();
        });
        registerPresenter = new RegisterPresenter(this);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));

        try {
            if (client == null) {
                client = new GoogleApiClient.Builder(activity)
                        .addConnectionCallbacks(this)
                        .enableAutoManage(fragmentActivity, this)
                        .addApi(Auth.CREDENTIALS_API)
                        .build();
                client.connect();
                requestHint();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        startSMSListener();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        activity.registerReceiver(smsBroadcast, intentFilter);
        return view;
    }

    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        try {
            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                    client, hintRequest);
            activity.startIntentSenderForResult(intent.getIntentSender(), RC_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    public void startSMSListener() {
        SmsRetrieverClient client = SmsRetriever.getClient(activity /* context */);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(aVoid -> {
            //  Utiles.CommonToast(activity, "start");
        });

        task.addOnFailureListener(e -> {
            // Failed to start retriever, inspect Exception for more details
            // ...
            // Utiles.CommonToast(activity, "Failed");
        });

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        if (client != null && client.isConnected()) {
            client.stopAutoManage(fragmentActivity);
            client.disconnect();
        }
        unbinder.unbind();
    }

    @OnClick({R.id.back_img, R.id.submit, R.id.cc_edt,R.id.terms_contionlink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                Utiles.hideKeyboard(activity);
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.submit:
                if(!termsConditionTxt.isChecked()){
                    Utiles.CommonToast(activity,activity.getResources().getString(R.string.please_check_terms_and_condition));
                    return;
                }
                validator.validate();
                break;
            case R.id.cc_edt:
                assert getFragmentManager() != null;
                picker.show(getFragmentManager(), "Country_picker");
                break;
            case R.id.terms_contionlink:
                    moveToFragment(new SupportFragment());
                break;
        }
    }


    @Override
    public void RegisterView(Response<RegisterModel> Response) {
        try {
            assert Response.body() != null;
            new JWTUtils(this, null).decoded(Response.body().getToken(), context, "reg");
            SharedHelper.putKey(activity, "token", Response.body().getToken());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void Errorlogview(Response<RegisterModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();
            Utiles.ShowError(Message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void JsonResponse(String s) {
        try {
            JSONObject obj = new JSONObject(s);
            String email = obj.getString("email");
            String name = obj.getString("name");
            String id = obj.getString("id");
            Log.e("response", "" + email + "==" + name + "==" + id);
            SharedHelper.putKey(context, "userid", id);
            SharedHelper.putKey(context, "email", email);
            SharedHelper.putKey(activity, "appflow", "registration");
            fragment = new ManagedocFragment();
            moveToFragment(fragment);
            Utiles.setDriversData(context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessOTP(Response<OTPModel> Response) {
        assert Response.body() != null;
        System.out.println("Enter OTP code" + Response.body().getCode());
        OTPCustomerDialog dialogClass = new OTPCustomerDialog(activity, Response.body().getCode(), this);
        dialogClass.setCancelable(false);
        Objects.requireNonNull(dialogClass.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
        dialogClass.show();
    }

    @Override
    public void onFailureOTP(Response<OTPModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();
            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, "Something went Wrong");
        }
    }

    @Override
    public void OTPVerification() {
        Utiles.hideKeyboard(activity);

        getRegisterApi();
    }

    @Override
    public void onValidationSucceeded() {
        Utiles.hideKeyboard(activity);
        registerPresenter.getOTP(Objects.requireNonNull(mobileEdt.getText()).toString(), Objects.requireNonNull(emailEdt.getText()).toString(), Objects.requireNonNull(ccEdt.getText()).toString(), activity);

    }

    private void getRegisterApi() {
        HashMap<String, String> map = new HashMap<>();
        map.put("fname", Objects.requireNonNull(fnameEdit.getText()).toString());
        map.put("lname", Objects.requireNonNull(lnameEdt.getText()).toString());
        map.put("email", Objects.requireNonNull(emailEdt.getText()).toString());
        map.put("phcode", Objects.requireNonNull(ccEdt.getText()).toString());
        map.put("phone", Objects.requireNonNull(mobileEdt.getText()).toString());
        map.put("password", Objects.requireNonNull(passwordEdt.getText()).toString());
        map.put("address", Objects.requireNonNull(address.getText()).toString());
        map.put("fcmId", SharedHelper.getToken(context, "device_token"));
        map.put("cnty", "");
        map.put("cntyname", "");
        map.put("lang", CommonData.strLanguage);
        map.put("state", "");
        map.put("statename", "");
        map.put("city", "");
        map.put("cityname", "");
        map.put("cur", CommonData.strCurrency);
        map.put("actMail", "");
        map.put("actHolder", "");
        map.put("actNo", "");
        map.put("actBank", "");
        map.put("actLoc", "");
        map.put("actCode", "");


        registerPresenter.getRegisterApi(map, activity);

        SharedHelper.putKey(context, "licence", "");
        SharedHelper.putKey(context, "licence_date", "");
        SharedHelper.putKey(context, "insurance", "");
        SharedHelper.putKey(context, "insurance_date", "");
        SharedHelper.putKey(context, "passing", "");
        SharedHelper.putKey(context, "passing_date", "");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
                switch (view.getId()) {
                    case R.id.fname_edit:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.minimum_four));
                        break;
                    case R.id.lname_edt:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.minimum_four));
                        break;
                    case R.id.email_edt:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.email_address));
                        break;
                    case R.id.password_edt:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.enter_password));
                        break;
                    case R.id.mobile_edt:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.enter_the_10_digit));
                        break;
                    case R.id.address:
                        ((EditText) view).setError(activity.getResources(). getString(R.string.enter_address));
                        break;

                }
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void moveToFragment(Fragment fragment) {
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(R.id.containter, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void mobileNumberParse(String number) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(number, "");
            int countryCode = numberProto.getCountryCode();
            long nationalNumber = numberProto.getNationalNumber();
            mobileEdt.setText(String.valueOf(nationalNumber));

        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_HINT && resultCode == Activity.RESULT_OK) {

            Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
            assert credential != null;
            System.out.println("enter the mobile number" + credential.getId());
            mobileNumberParse(credential.getId());

        }
    }
}
