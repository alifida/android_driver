package com.auezeride.driver;

import android.Manifest;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.constant.Unit;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.androidadvance.topsnackbar.TSnackbar;
import com.firebase.geofire.GeoLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.play.core.internal.m;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jakewharton.rxbinding.view.RxView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Logger;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;

import com.auezeride.driver.Activity.ProfileActivity;
import com.auezeride.driver.Activity.WelcomeActivity;
import com.auezeride.driver.Adapter.VehicleListAdapter;

import com.auezeride.driver.CommonClass.CommonData;
import com.auezeride.driver.CommonClass.CommonFirebaseListoner;
import com.auezeride.driver.CommonClass.Constants;
import com.auezeride.driver.CommonClass.FontChangeCrawler;
import com.auezeride.driver.CommonClass.SharedHelper;
import com.auezeride.driver.CommonClass.Stopwatch;
import com.auezeride.driver.CommonClass.Utiles;
import com.auezeride.driver.CustomizeDialog.WalletAlertDialog;
import com.auezeride.driver.EventBus.DestinationAddressEvent;
import com.auezeride.driver.FlowInterface.RequestInterface;
import com.auezeride.driver.ForceToUpdate.inApp.InAppUpdateManager;
import com.auezeride.driver.Fragment.AddVehicleFragment;
import com.auezeride.driver.Fragment.DriverCreditFragment;
import com.auezeride.driver.Fragment.DriverEarningsFragment;
import com.auezeride.driver.Fragment.ManageVehiclesFragment;
import com.auezeride.driver.Fragment.ManagedocFragment;
import com.auezeride.driver.Fragment.RatingFragment;
import com.auezeride.driver.Fragment.YourTripFragment;
import com.auezeride.driver.Geofire.GeoFire;
import com.auezeride.driver.GooglePlace.GooglePlcaeModel.AddressComponent;
import com.auezeride.driver.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.driver.Model.FareCaluationModel;
import com.auezeride.driver.Model.ListVehicleModel;
import com.auezeride.driver.Model.OnlineOflline;
import com.auezeride.driver.Model.TripFlowModel;
import com.auezeride.driver.Navigationdrawer.FragmentDrawer;
import com.auezeride.driver.Presenter.CityPolygonPresenter.CityPolygonPresenter;
import com.auezeride.driver.Presenter.DriverPresenter;
import com.auezeride.driver.Presenter.GoogleGeocoderPresenter;
import com.auezeride.driver.Presenter.LogoutPresenter;

import com.auezeride.driver.Service.TripRquestService;
import com.auezeride.driver.TripflowFragment.CancelFragment;
import com.auezeride.driver.TripflowFragment.RequestFragement;
import com.auezeride.driver.TripflowFragment.SummaryFragment;
import com.auezeride.driver.TripflowFragment.TripFlowFragment;
import com.auezeride.driver.View.DriverView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, LocationSource,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, ValueEventListener, DirectionCallback, RequestInterface, DriverView, VehicleListAdapter.UpdateVehicleInterface, Stopwatch.StopWatchListener, GoogleGeocoderPresenter.GoogleGeoCoderView, CityPolygonPresenter.PolyGonView {
    @SuppressLint("StaticFieldLeak")
    public static DrawerLayout mDrawerLayout;
    FragmentDrawer drawerFragment;
    @BindView(R.id.menu_imgbtn)
    ImageButton menuImgbtn;
    ImageView userProfileImage;
    RelativeLayout logout_layout;
    TextView txtUserName;
    @BindView(R.id.driver_online_layout)
    RelativeLayout driverOnlineLayout;
    LocationManager locationManager;
    Fragment fragment = null;
    AlertDialog VehicleDialog;
    Fragment FlowFragment = null;
    static GeoFire geoFire;
    Context context = MainActivity.this;
    AppCompatActivity activity = MainActivity.this;
    BitmapDescriptor mapCarIcon;
    GoogleApiClient mGoogleApiClient;
    RecyclerView vehicle_list;
    VehicleListAdapter vehicleListAdapter;
    List<ListVehicleModel> vehicleModels = new ArrayList<>();
    @BindView(R.id.change_txt)
    TextView changeTxt;
    @BindView(R.id.profile_img)
    ImageView profileImg;
    @BindView(R.id.online_offline_switch)
    Switch onlineOfflineSwitch;
    @BindView(R.id.online_offline_text)
    TextView onlineOfflineText;
    @BindView(R.id.plate_txt)
    TextView plateTxt;
    @BindView(R.id.category_model_txt)
    TextView categoryModelTxt;
    @BindView(R.id.proostatus)
    TextView proostatus;
    public static GoogleMap mMap;
    View mapView;
    Bitmap mapBitmap;
    Marker currentLocMarker, pickUPrDropMarker;
    static DatabaseReference ProofstatusReference, RequestDatabaseReference, TripFlowReference, scheduleTripDatabase;
    static ValueEventListener ProofstatusValue, RequestValueListener, TripFlowValue, scheduleTripValue;
    protected DatabaseReference CategoryDatabase;
    protected ValueEventListener CategoryValueEvent;
    DriverPresenter driverPresenter;
    float previousBearing = 0;
    AlertDialog alert11;
    int mapPosition = 0;
    static String driverId = "", strPreviousClassName = "";
    public static Location mCurrentLocation, starLocation, EndLocation;
    final Handler handler = new Handler();
    private Runnable runnable;
    LatLng prevLatLng = new LatLng(0, 0);
    ArrayList<String> arrayCategory = new ArrayList<>();
    @BindView(R.id.online_offline_layout)
    LinearLayout onlineOfflineLayout;
    @BindView(R.id.Driver_details)
    RelativeLayout DriverDetails;
    private OnLocationChangedListener mMapLocationListener = null;
    public CityPolygonPresenter cityPolygonPresenter;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    protected LocationRequest mLocationRequest;
    TextView change_txt;
    static String strMake, strModel, strPlateNum, Tripstatus = "";

    public static LatLng destLocation;
    public static FragmentManager FragmentManage;
    //proof and online status
    private static Boolean proofstatus = false;
    private static Boolean onlinestatus = false;

    private Polyline routePolyline;

   /* public static double distance = 0;
    double speed;
    public static int p = 1;
    public static String strDistanceBegin;
    public static String strTotalDistance = "0";*/

    FragmentManager fragmentManager;
    GoogleGeocoderPresenter googleGeocoderPresenter;

    private CompositeDisposable disposable;
    private String strCityName = "";
    List<LatLng> PolyGonData;
    private InAppUpdateManager inAppUpdateManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);
        findviewById();
        getDateFormate();
        Constants.RquestScreen = false;
        FragmentManage = getSupportFragmentManager();
        strMake = SharedHelper.getKey(context, "vmake");
        strModel = SharedHelper.getKey(context, "vmodel");
        strPlateNum = SharedHelper.getKey(context, "numplate");
        if (strMake != null && !strMake.isEmpty()) {
            categoryModelTxt.setText(Utiles.Nullpointer(strMake) + Utiles.Nullpointer(strModel));
        }
        driverPresenter = new DriverPresenter(this);
        fragmentManager = getSupportFragmentManager();


        disposable = new CompositeDisposable();
        googleGeocoderPresenter = new GoogleGeocoderPresenter(this, disposable);
        cityPolygonPresenter = new CityPolygonPresenter(activity, new CompositeDisposable(), this);
        PolyGonData = new ArrayList<>();
        if (SharedHelper.getKey(context, "vehicleId") != null && !SharedHelper.getKey(context, "vehicleId").isEmpty()) {
            getVehicleCategory(SharedHelper.getKey(context, "vehicleId"));
        }
        DriverProofstatus();
        startService();
        CommonFirebaseListoner.Previous = "0";
        CommonFirebaseListoner.FirebaseTripFlow();
        CommonFirebaseListoner.setActivity(activity);

        /*Constraints myConstraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.NOT_REQUIRED)
                .build();
        OneTimeWorkRequest mywork=
                new OneTimeWorkRequest.Builder(MyWorker.class)
                        .setConstraints(myConstraints)
                        .build();
        WorkManager.getInstance().enqueueUniqueWork("work",ExistingWorkPolicy.KEEP,mywork);*/
        if (!BuildConfig.DEBUG) {
            inAppUpdateManager = InAppUpdateManager.Builder(this, 1001);
            inAppUpdateManager.resumeUpdates(true);
            inAppUpdateManager.mode(com.auezeride.driver.ForceToUpdate.inApp.Constants.UpdateMode.IMMEDIATE);
            inAppUpdateManager.checkForAppUpdate();
        }

    }

    public void findviewById() {

        mDrawerLayout = findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, null);
        drawerFragment.setDrawerListener(this);
        userProfileImage = mDrawerLayout.findViewById(R.id.rider_profile_image);
        txtUserName = mDrawerLayout.findViewById(R.id.userName);
        logout_layout = mDrawerLayout.findViewById(R.id.logout_layout);
        txtUserName.setOnClickListener(v -> {
            mDrawerLayout.closeDrawer(GravityCompat.START);
            startActivity(new Intent(context, ProfileActivity.class));
        });
        logout_layout.setOnClickListener(view -> Alertdialog());

        driverId = SharedHelper.getKey(context, "userid");
        System.out.println("enter the driver id" + driverId);
        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(this.findViewById(android.R.id.content));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), userProfileImage, activity);
        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), profileImg, activity);
        if (SharedHelper.getKey(context, "fname") != null) {
            txtUserName.setText(SharedHelper.getKey(context, "fname") + SharedHelper.getKey(context, "lname"));
        }
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!gps_enabled) {

            GPSTurnOnAlert();
        }
        mGoogleApiClient.connect();

        updateSmartLocation();
    }

    @OnClick({R.id.menu_imgbtn,})
    public void onViewClicked() {
        mDrawerLayout.openDrawer(GravityCompat.START);

    }

    public void getRequestStatus() {
        RequestDatabaseReference = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_data").child(driverId + "/request")).getDatabaseReference();
        RequestValueListener = RequestDatabaseReference.addValueEventListener(this);
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= 29) {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_BACKGROUND_LOCATION
                                , Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        } else {
            if (ContextCompat.checkSelfPermission(MainActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
            }
        }

    }

    public void startService() {
        if (!Utiles.isMyServiceRunning(activity, TripRquestService.class)) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                startService(new Intent(context, TripRquestService.class));
            } else {
                Intent serviceIntent = new Intent(context, TripRquestService.class);
                context.startForegroundService(serviceIntent);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the task you need to do.
                    startLocationUpdates();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                        startService(new Intent(context, TripRquestService.class));
                    } else {
                        Intent serviceIntent = new Intent(context, TripRquestService.class);
                        context.startForegroundService(serviceIntent);
                    }

                } else {
                    System.out.println("INSIDE request permission");
                    // permission denied, boo! Disable the functionality that depends on this permission.
                }
            }
        }
    }

    public void GPSTurnOnAlert() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(Constants.SET_INTERVAL); //5 seconds
        locationRequest.setFastestInterval(Constants.SET_FASTESTINTERVAL); //3 seconds
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        currentLocation();
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                            startService(new Intent(context, TripRquestService.class));
                        } else {
                            Intent serviceIntent = new Intent(context, TripRquestService.class);
                            context.startForegroundService(serviceIntent);
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied. But could be fixed by showing the user
                        // a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(MainActivity.this, Constants.REQUEST_CHECK_SETTINGS);

                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way to fix the
                        // settings so we won't show the dialog.

                        break;
                }
            }
        });
    }

    protected void startLocationUpdates() {
        checkPermission();
        mGoogleApiClient.connect();
        //LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this, Looper.getMainLooper());
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        switch (position) {
            case 0:
                RemoveRequestListioner();
                Log.e("profilepage", "Profile page");
                startActivity(new Intent(context, ProfileActivity.class));
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
            case 1:
                fragment = new DriverCreditFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
            case 2:
                CommonData.Earningdate = "";
                fragment = new YourTripFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
            case 3:
                fragment = new RatingFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;

            case 4:
                fragment = new ManageVehiclesFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;

            case 5:
                fragment = new ManagedocFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;

            case 6:
                fragment = new DriverEarningsFragment();
                moveToFragment(fragment);
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (FlowFragment == null) {
            super.onBackPressed();
            CLoseDrawer();
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }


    }

    private void moveToFragment(Fragment fragment) {
        Reintialize();
        try {
            if (!isFinishing()) {
                runOnUiThread(() -> {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    FragmentManage.beginTransaction().setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right)
                            .replace(R.id.overall_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();

                });

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void FlowFragment(Fragment fragment) {
        Reintialize();
        try {
            if (!isFinishing()) {
                runOnUiThread(() -> FragmentManage.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .replace(R.id.containterss, fragment, fragment.getClass().getSimpleName()).commitNowAllowingStateLoss());

            } else {
                callMethodWithDelay(fragment);
                System.out.println("enter the flow fragment in isfinish");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("enter the flow fragment in exception" + e.getMessage());
            callMethodWithDelay(fragment);
        }

    }

    int count = 0;

    public void callMethodWithDelay(Fragment fragment) {
        if (count == 2) {
            count = 0;
        } else {
            count++;
            new Handler().postDelayed(() -> FlowFragment(fragment), 3000);
        }


    }


    private void SummaryFragment(Fragment fragment) {
        Reintialize();
        try {
            System.out.println("enter the isvisble" + isFinishing());
            if (!isFinishing()) {
                runOnUiThread(() -> FragmentManage.beginTransaction()
                        .replace(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss());

            } else {

                if (CommonData.RequestBoolean) {
                    fragment.setUserVisibleHint(false);
                }
                CommonData.RequestBoolean = false;
            }
        } catch (Exception e) {
            Toast.makeText(activity, "text", Toast.LENGTH_LONG).show();
            if (CommonData.RequestBoolean) {
                fragment.setUserVisibleHint(false);
            }
            CommonData.RequestBoolean = false;
            e.printStackTrace();
        }
    }

    public void RemoveFragment(Fragment fragment) {
        Reintialize();
        runOnUiThread(() -> {
            if (fragment != null) {
                runOnUiThread(() -> FragmentManage.beginTransaction().remove(fragment).commitAllowingStateLoss());

            }
        });

    }

    @OnClick({R.id.driver_online_layout, R.id.change_txt})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.driver_online_layout:

                break;
            case R.id.change_txt:
                driverPresenter.getVehcleList(SharedHelper.getKey(context, "userid"), activity);
                break;
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        LatLng curPos;
        float curPosBearing;
        if (mCurrentLocation == null && location != null) {
            getAddress(new LatLng(location.getLatitude(), location.getLongitude()));
        }


        mCurrentLocation = location;
        if (mMapLocationListener != null) {
            mMapLocationListener.onLocationChanged(location);
        }


        if (mCurrentLocation != null) {
            System.out.println("ONLOCATIOn CHANGE bearing" + mCurrentLocation.getBearing());
            curPos = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            curPosBearing = mCurrentLocation.getBearing();

        } else {
            curPos = new LatLng(location.getLatitude(), location.getLongitude());
            curPosBearing = location.getBearing();
            System.out.println("location null");
        }
        mCurrentLocation = location;
        updateLocationToFirebase(mCurrentLocation);


        if (mMap != null) {
            try {

                System.out.println("Key moved ===>" + mCurrentLocation.getSpeed());

                zoomCameraToPosition(curPos);
               /* if (destLocation != null) {
                    GoogleDirection.withServerKey(Constants.GoogleDirectionApi)
                            .from(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()))
                            .to(destLocation)
                            .unit(Unit.METRIC)
                            .transportMode(TransportMode.DRIVING)
                            .execute(this);

                }*/


                if (currentLocMarker == null) {


                    currentLocMarker = mMap.addMarker(new MarkerOptions()
                            .icon(mapCarIcon)
                            .visible(false)
                            .position(curPos)
                            .flat(true));
                    currentLocMarker.setAnchor(0.5f, 0.5f);
                    currentLocMarker.setRotation(curPosBearing);

                } else {

                    if (mCurrentLocation.getBearing() != 0.0)
                        previousBearing = mCurrentLocation.getBearing();

                    if (prevLatLng != new LatLng(0, 0)) {

                        if (!(curPos.equals(prevLatLng))) {

                            double[] startValues = new double[]{prevLatLng.latitude, prevLatLng.longitude};

                            double[] endValues = new double[]{curPos.latitude, curPos.longitude};

                            System.out.println("Start location===>" + startValues[0] + "  " + startValues[1]);
                            System.out.println("end location===>" + endValues[0] + "  " + endValues[1]);

                            System.out.println("inside locationchange bearing" + mCurrentLocation.getBearing());

                            animateMarkerTo(currentLocMarker, startValues, endValues, mCurrentLocation.getBearing());

                        } else {
                            System.out.println("outside locationchange bearing" + mCurrentLocation.getBearing());
                            if (mCurrentLocation.getBearing() == 0.0)
                                currentLocMarker.setRotation(previousBearing);
                            else
                                currentLocMarker.setRotation(mCurrentLocation.getBearing());
                            // currentLocMarker.setRotation(mCurrentLocation.getBearing());
                        }
                    } else {
                        currentLocMarker.setPosition(curPos);
                        currentLocMarker.setRotation(mCurrentLocation.getBearing());
                    }

                    prevLatLng = new LatLng(curPos.latitude, curPos.longitude);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onConnected(@Nullable Bundle bundle) {


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mMapLocationListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mMapLocationListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mapCarIcon = BitmapDescriptorFactory.fromResource(R.mipmap.ic_auto);
        mMap.setMyLocationEnabled(true);

       //added by Mudassar Nazir start
        if (mMap != null) {
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {

                    mCurrentLocation = location;
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng)                              // Sets the center of the map to current location
                            .zoom(Constants.MAP_ZOOM_SIZE)
                            .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                            .build();
                    mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    updateLocationToFirebase(location);
                }
            });
        }


        /*if (mMap != null) {
            mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
                    getAddress(latLng);

                    CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(latLng)                              // Sets the center of the map to current location
                            .zoom(Constants.MAP_ZOOM_SIZE)
                            .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                            .build();

                    if(currentLocMarker == null){
                        currentLocMarker = mMap.addMarker(new MarkerOptions()
                                .icon(mapCarIcon)
                                .visible(false)
                                .position(latLng)
                                .flat(true)
                                .anchor(0.5f, 0.5f)
                                .rotation(mCurrentLocation.getBearing()));
                    }else
                    {
                        currentLocMarker.remove();
                        mMap.clear();
                        currentLocMarker = mMap.addMarker(new MarkerOptions()
                                .icon(mapCarIcon)
                                .visible(false)
                                .position(latLng)
                                .flat(true)
                                .anchor(0.5f, 0.5f)
                                .rotation(mCurrentLocation.getBearing()));
                        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                        updateLocationToFirebase(location);
                    }
                }
            });
        }*/

        //end


        try {
          /*  MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.maps_style);
            googleMap.setMapStyle(style);*/

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
//        currentLocMarker.remove();

//        currentLocMarker.setIcon(mapCarIcon);
        mMap.getMinZoomLevel();
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
            layoutParams.setMargins(30, 180, 0, 0);
        }

        checkPermission();


        mGoogleApiClient.connect();


        currentLocation();
        try {
            mMap.setOnMapLoadedCallback(() -> {
                // Make a snapshot when map's done loading
                mMap.snapshot(bitmap -> {
                    //Getting Map as Bitmap
                    mapBitmap = bitmap;
                    //  googleIcon();
                    SharedHelper.putKey(context, "mapImage", Utiles.BitMapToString(mapBitmap));

                });
            });
        } catch (Exception e) {
            System.out.println("Exception" + e);
        }


    }

    public void googleIcon() {
        //  RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) DriverDetails.getLayoutParams();

        View googleLogo = mapView.findViewWithTag("GoogleWatermark");
        RelativeLayout.LayoutParams glLayoutParams = (RelativeLayout.LayoutParams) googleLogo.getLayoutParams();
        // glLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, 0);
        //    glLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT, 0);
        // glLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START, 0);
        //  glLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        //glLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_END, RelativeLayout.TRUE);
        glLayoutParams.addRule(RelativeLayout.ABOVE, R.id.Driver_details);
        googleLogo.setLayoutParams(glLayoutParams);

    }

    public void currentLocation() {
        mCurrentLocation = getFusedLocation();

        if (mCurrentLocation != null) {
            RemovePolyline();
            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            getAddress(latLng);
            System.out.println("INSIDE LOCAION CHANGE" + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)                              // Sets the center of the map to current location
                    .zoom(Constants.MAP_ZOOM_SIZE)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            if (currentLocMarker == null) {

                currentLocMarker = mMap.addMarker(new MarkerOptions()
                        .icon(mapCarIcon)
                        .visible(false)
                        .position(latLng)
                        .flat(true)
                        .anchor(0.5f, 0.5f)
                        .rotation(mCurrentLocation.getBearing()));
            } else {
                currentLocMarker.setPosition(latLng);
            }
        }
    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        System.out.println("enter the google direction status" + direction.getStatus());
        if (direction.isOK()) {
            if (mCurrentLocation != null && destLocation != null) {
                onDirectionSuccessMarkerPlacing(direction);
            }
        }

    }

    public void onDirectionSuccessMarkerPlacing(Direction direction) {
        LatLng curPos = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

        ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();
        if (curPos.latitude != 0 && curPos.longitude != 0) {

            zoomCameraToPosition(curPos);
        }

        if (routePolyline != null) {
            routePolyline.setPoints(directionPositionList);

        } else {
            routePolyline = mMap.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.BLUE));
            routePolyline.setVisible(true);
        }
        if (currentLocMarker.getPosition() != null) {
            if (currentLocMarker.getPosition().latitude != 0 && currentLocMarker.getPosition().longitude != 0) {
                if (mCurrentLocation.getBearing() != 0.0) {
                    final CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLocMarker.getPosition())      // Sets the center of the map to Mountain View
                            //.zoom(Constants.MAP_ZOOM_SIZE_ONTRIP)      // Sets the zoom
                            .bearing(mCurrentLocation.getBearing())                // Sets the orientation of the camera to east
                            .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    CameraUpdate center = CameraUpdateFactory.newLatLng(curPos);
                    mMap.animateCamera(center, 400, null);
                } else {
                    final CameraPosition cameraPosition = new CameraPosition.Builder()
                            .target(currentLocMarker.getPosition())      // Sets the center of the map to Mountain View
                            //.zoom(Constants.MAP_ZOOM_SIZE_ONTRIP)      // Sets the zoom
                            .bearing(previousBearing)                // Sets the orientation of the camera to east
                            .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                            .build();
                    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                    CameraUpdate center = CameraUpdateFactory.newLatLng(curPos);
                    mMap.animateCamera(center, 400, null);
                }
            }
        }

        try {
            if (Constants.Previousstatus.matches("3")) {


                if (pickUPrDropMarker != null)
                    pickUPrDropMarker.remove();

                // before loop:
                System.out.println("destLocation location ===>" + destLocation);
                pickUPrDropMarker = mMap.addMarker(new MarkerOptions().position(destLocation).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ub__ic_pin_dropoff)));
                pickUPrDropMarker.setVisible(true);

            } else {

                if (pickUPrDropMarker != null)
                    pickUPrDropMarker.remove();

                System.out.println("destLocation location ===>" + destLocation);
                pickUPrDropMarker = mMap.addMarker(new MarkerOptions().position(destLocation).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ub__ic_pin_pickup)));
                pickUPrDropMarker.setVisible(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDirectionFailure(Throwable t) {

    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        if (dataSnapshot.getValue() != null) {
            System.out.println("enter the request" + SharedHelper.getKey(context, "trip_id"));
            if (SharedHelper.getKey(context, "trip_id") == null || SharedHelper.getKey(context, "trip_id").equalsIgnoreCase("null") || SharedHelper.getKey(context, "trip_id").isEmpty()) {
                if (dataSnapshot.child("status").getValue().equals("1")) {

                    if (!CommonData.RequestBoolean) {
                        removeAllFragments(FragmentManage);
                        CLoseDrawer();
                        Constants.RequestStart = false;
                        FlowFragment = new RequestFragement(dataSnapshot);
                        if (FlowFragment instanceof RequestFragement && !FlowFragment.isVisible()) {
                            // do you stuff
                            CommonData.RequestBoolean = true;
                            if (CommonData.isRequestFrom) {
                                SummaryFragment(FlowFragment);
                            } else {
                                SummaryFragment(FlowFragment);
                                if (!FlowFragment.isVisible()) {
                                    CommonData.RequestBoolean = false;

                                }
                            }
                        }
                        CommonData.requestionInprogress = true;

                    }

                } else if (dataSnapshot.child("status").getValue().equals("2")) {
                    if (!CommonData.RequestBoolean) {
                        CommonData.RequestBoolean = true;
                        ScheduleTrip();
                    }

                } else {
                    RemoveFragment(FlowFragment);
                    CommonData.RequestBoolean = false;
                    CommonData.isRequestFrom = true;
                    Constants.RequestStart = true;
                    CommonData.requestionInprogress = false;
                }

            }
        }


    }

    public void CLoseDrawer() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void ScheduleTrip() {
        scheduleTripDatabase = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(SharedHelper.getKey(context, "userid")).child("accept").child("trip_id");
        scheduleTripDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    SharedHelper.putKey(context, "trip_id", dataSnapshot.getValue().toString());
                    FirebaseTripFlow();
                    //Utiles.CreateFirebaseTripData(dataSnapshot.getValue().toString());
                    //  scheduleTripDatabase.removeEventListener(scheduleTripValue);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("tage", "error respone" + databaseError.getMessage());
            }
        });
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {
        Log.e("tage", "error respone" + databaseError.getMessage());
    }



    public Location getFusedLocation() {
        LocationRequest mLocationRequest = LocationRequest.create();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        System.out.println("Location Provoider:" + " Fused Location");

        if (mCurrentLocation == null) {

            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            System.out.println("Location Provoider:" + " Fused Location Fail: GPS Location");

            if (locationManager != null) {

                //To avoid duplicate listener
                try {
                    locationManager.removeUpdates(this);
                    System.out.print("remove location listener success");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.print("remove location listener failed");
                }

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        Constants.MIN_TIME_BW_UPDATES,
                        Constants.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    mCurrentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }
                if (mCurrentLocation == null) {

                    System.out.println("Location Provoider:" + " GPS Location Fail: Network Location");

                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            Constants.MIN_TIME_BW_UPDATES,
                            Constants.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        mCurrentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }

                }
            }
        }

        return mCurrentLocation;
    }

    public void zoomCameraToPosition(LatLng curPos) {

        boolean contains = mMap.getProjection().getVisibleRegion().latLngBounds.contains(curPos);

        if (!contains) {

            float zoomPosition;
            if (SharedHelper.getKey(context, "trip_id") == null || SharedHelper.getKey(context, "trip_id").matches("null")) {
                zoomPosition = Constants.MAP_ZOOM_SIZE;
                System.out.println("enter the normal no trip");
            } else {
                zoomPosition = Constants.MAP_ZOOM_SIZE_ONTRIP;
                System.out.println("enter the normal  trip");
            }


            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(curPos)                              // Sets the center of the map to current location
                    .zoom(zoomPosition)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : fragmentManager.getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case Constants.REQUEST_CHECK_SETTINGS:


                switch (resultCode) {
                    case Activity.RESULT_OK:
                        // All required changes were successfully made
                        mCurrentLocation = getFusedLocation();

                        new Handler().postDelayed(new Runnable() {

                            /*
                             * Showing splash screen with a timer. This will be useful when you
                             * want to show case your app logo / company
                             */

                            @Override
                            public void run() {
                                // This method will be executed once the timer is over
                                if (mCurrentLocation != null) {
                                    zoomCameraToPosition(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
                                }

                            }
                        }, Constants.GET_ZOOM_TIME);

                        break;

                    case Activity.RESULT_CANCELED:

                        // The user was asked to change settings, but chose not to
                        Toast.makeText(this, "Location not enabled, user cancelled.", Toast.LENGTH_SHORT).show();
                        finish();
                        break;

                    default:

                        break;
                }

                break;
            case 1001:
                if (resultCode == Activity.RESULT_CANCELED) {
                    inAppUpdateManager.checkForAppUpdate();
                }
                break;


        }
    }

    // Animation handler for old APIs without animation support
    private void animateMarkerTo(final Marker marker, double[] startValues, double[] endValues, final float bearing) {


        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(1300);
        latLngAnimator.setInterpolator(new DecelerateInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                double[] animatedValue = (double[]) animation.getAnimatedValue();
                marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
            }
        });
        latLngAnimator.start();
        // marker.setRotation(bearing);
        //rotateMarker(marker,bearing,mMap);

        float rotate = getRotate(new LatLng(startValues[0], startValues[1]), new LatLng(endValues[0], endValues[1]));
        System.out.println("Rotate===>" + rotate);
        //marker.setRotation(360 - rotate + myMap.getCameraPosition().bearing);
        if (mCurrentLocation.getBearing() == 0.0)
            marker.setRotation(previousBearing);
        else
            marker.setRotation(mCurrentLocation.getBearing());
    }

    private static float getRotate(LatLng curPos, LatLng nextPos) {
        double x1 = curPos.latitude;
        double x2 = nextPos.latitude;
        double y1 = curPos.longitude;
        double y2 = nextPos.longitude;

        return (float) (Math.atan2(y2 - y1, x2 - x1) / Math.PI * 180);
    }

    @Override
    public void TripFragment() {
        runOnUiThread(() -> {
            driverOnlineLayout.setVisibility(View.GONE);
            RemoveFragment(FlowFragment);
            FlowFragment = new TripFlowFragment("empty", mCurrentLocation);
            FlowFragment(FlowFragment);
            FirebaseTripFlow();
        });

    }

    @Override
    public void summaryFragment() {
        CommonData.strDistanceBegin = "totaldistancend";

        CommonData.p = 1;
        CommonData.lStart = null;
        CommonData.lEnd = null;
        String starttime = SharedHelper.getKey(context, "starttime");
        String endtime = getCurrentTime();
        Integer duration = 0;
        DateFormat format = new SimpleDateFormat("HH:mm");//24 Hour Format

        Date d1;
        Date d2;
        try {
            d1 = format.parse(starttime);
            d2 = format.parse(endtime);

            long diff = d2.getTime() - d1.getTime();
            long diffMinutes = diff / (60 * 1000);
            duration = (int) diffMinutes;

            System.out.println("THe time difference" + diffMinutes);

        } catch (Exception e) {
            e.printStackTrace();
        }
        mCurrentLocation = getFusedLocation();
        FareCaluationModel fareCaluationModel = new FareCaluationModel();
        fareCaluationModel.setDistance(CommonData.strTotalDistance);
        fareCaluationModel.setWaitingTime(String.valueOf(CommonData.stopWatch.getElapsedTimeSecs() + 60 * CommonData.stopWatch.getElapsedTimeMin() + 60 * 60 * CommonData.stopWatch.getElapsedTimeHour()));
        fareCaluationModel.setDuration(String.valueOf(CommonData.stopWatch.getElapsedTimeMin() + 60 * CommonData.stopWatch.getElapsedTimeHour()));
        fareCaluationModel.setmCurrentLocation(mCurrentLocation);

        RemoveFragment(FlowFragment);
        RemovePolyline();
        FlowFragment = new SummaryFragment(fareCaluationModel, "Api");
        SummaryFragment(FlowFragment);
        CommonData.distance = 0;

    }

    @Override
    public void ClearFragment() {
        Notrip();
        mMap.clear();
        currentLocMarker = null;
        currentLocation();
        RemovePolyline();


    }

    @Override
    public void CallCancelFragment() {
        SummaryFragment(new CancelFragment());
    }

    @Override
    public void ClearAllFragment() {
        removeAllFragments(FragmentManage);
        Notrip();
        mMap.clear();
        currentLocMarker = null;
        currentLocation();
        RemovePolyline();
    }

    public void Notrip() {
        runOnUiThread(() -> {
            driverOnlineLayout.setVisibility(View.VISIBLE);
            RemoveFragment(FlowFragment);
            FlowFragment = null;
            destLocation = null;
        });

        //currentLocation();

    }

    public void RemovePolyline() {
        destLocation = null;
        runOnUiThread(() -> {
            try {
                if (routePolyline != null) {
                    routePolyline.setVisible(false);
                    routePolyline.remove();
                    routePolyline = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (pickUPrDropMarker != null) {
                    pickUPrDropMarker.setVisible(false);
                    pickUPrDropMarker.remove();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }

    @Override
    public void FlowDetails(Response<TripFlowModel> Response) {
        if (!Constants.Previousstatus.equalsIgnoreCase("4") && !Constants.Previousstatus.equalsIgnoreCase("5")) {
            if (Response.body().getStatus().equalsIgnoreCase("Start Trip")) {
                destLocation = new LatLng(Double.parseDouble(Response.body().getPickupdetails().getEndcoords().get(1)), Double.parseDouble(Response.body().getPickupdetails().getEndcoords().get(0)));
                DrawablePolyline();

            } else {
                destLocation = new LatLng(Double.parseDouble(Response.body().getPickupdetails().getStartcoords().get(1)), Double.parseDouble(Response.body().getPickupdetails().getStartcoords().get(0)));
                DrawablePolyline();
            }
        }

    }

    public void DrawablePolyline() {
        if (mCurrentLocation != null) {
            GoogleDirection.withServerKey(Constants.GoogleDirectionApi)
                    .from(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()))
                    .to(destLocation)
                    .unit(Unit.METRIC)
                    .transportMode(TransportMode.DRIVING)
                    .execute(this);
        }
    }

    @Override
    public void OnlineSuccess(Response<OnlineOflline> Response, String status) {
        assert Response.body() != null;
        if (Response.body().getSuccess()) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("drivers_data").child(driverId).child("online_status").setValue(status);
        } else {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("drivers_data").child(driverId).child("online_status").setValue(status);
        }

    }

    @Override
    public void OnlineFailed(Response<OnlineOflline> Response, String status) {
        if (status.equalsIgnoreCase("1")) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("drivers_data").child(driverId).child("online_status").setValue("0");
        } else {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
            ref.child("drivers_data").child(driverId).child("online_status").setValue("1");
        }
    }

    @Override
    public void VehicleListsuccessFully(Response<List<ListVehicleModel>> response) {
        vehicleModels.clear();
        vehicleModels.addAll(response.body());
        alertChange();
    }

    @Override
    public void VehicleListFailure(Response<List<ListVehicleModel>> response) {
        Utiles.displayMessage(getCurrentFocus(), context, activity.getResources().getString(R.string.something_went_wrong));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void UpdateVehicle(ListVehicleModel listVehicleModel) {
        UpdateVehicledIFirebase(listVehicleModel.getId());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (VehicleDialog != null) {
                    try {
                        VehicleDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        SharedHelper.putKey(context, "vehicleId", listVehicleModel.getId());
        SharedHelper.putKey(context, "vmake", listVehicleModel.getMakename());
        SharedHelper.putKey(context, "vmodel", listVehicleModel.getModel());
        SharedHelper.putKey(context, "numplate", listVehicleModel.getLicence());
        HashMap<String, String> map = new HashMap<>();
        map.put("makeid", listVehicleModel.getId());
        map.put("service", "");
        driverPresenter.UpdateVicle(map, activity);
        // plateTxt.setText(Utiles.Nullpointer(listVehicleModel.getLicence()));
        categoryModelTxt.setText(Utiles.Nullpointer(listVehicleModel.getMakename()) + Utiles.Nullpointer(listVehicleModel.getModel()));

    }

    public void UpdateVehicledIFirebase(String vehicleID) {
        DatabaseReference Driverdata = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(SharedHelper.getKey(context, "userid"));
        HashMap<String, Object> map = new HashMap<>();
        map.put("vehicle_id", vehicleID);
        Driverdata.updateChildren(map);
        getVehicleCategory(vehicleID);
    }

    public void getVehicleCategory(final String VehicleID) {
        if (driverId != null) {
            remogeoFire();
            if (CategoryDatabase != null) {
                CategoryDatabase.removeEventListener(CategoryValueEvent);
            }
            CategoryDatabase = FirebaseDatabase.getInstance().getReference().child("vehicle_list").child(driverId).child(VehicleID).child("category");
            CategoryValueEvent = CategoryDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    if (dataSnapshot.getValue() != null && !CommonData.requestionInprogress) {
                        mGoogleApiClient.connect();
                        arrayCategory.clear();
                        System.out.println("Enter the car category" + dataSnapshot);
                        for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                            if (!arrayCategory.contains(dataSnapshot.getKey())) {
                                arrayCategory.add(dataSnapshot1.getKey());
                            }
                        }

                        SharedHelper.setList(activity, "categorylist", arrayCategory);
                        updateLocationToFirebase(mCurrentLocation);
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }

    public void AddVehicleToFirebase(String vehicleID) {
        if (driverId != null) {
            DatabaseReference Driverdata = FirebaseDatabase.getInstance().getReference().child("vehicle_list").child(driverId).child(vehicleID);
            HashMap<String, Object> map = new HashMap<>();
            map.put("vehicle_id", vehicleID);
            map.put("status", "0");
            Driverdata.updateChildren(map);
            DatabaseReference Addvehicle = FirebaseDatabase.getInstance().getReference().child("vehicle_list").child(driverId).child(vehicleID).child("category");
            HashMap<String, Object> cartype = new HashMap<>();
            cartype.put("auto", "0");
            Addvehicle.updateChildren(cartype);
            getVehicleCategory(vehicleID);
        }

    }

    @Override
    public void onTick(String time) {

    }

    @Override
    public void geocoderOnSucessful(GeocoderModel geocoderModel) {
        try {
            if (geocoderModel.getResults() != null && !geocoderModel.getResults().isEmpty()) {
                for (AddressComponent addressComponent : geocoderModel.getResults().get(0).getAddressComponents()) {
                    if (addressComponent.getTypes().get(0).equalsIgnoreCase("administrative_area_level_2")) {
                        callPolygon(addressComponent.getShortName());
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void geocoderOnFailure(Throwable throwable) {

    }

    @Override
    public void SuccessPolygon(List<LatLng> latLngs) {
        PolyGonData.addAll(latLngs);
        setPolygon(latLngs);
    }

    private class DoubleArrayEvaluator implements TypeEvaluator<double[]> {

        private double[] mArray;

        /**
         * Create a DoubleArrayEvaluator that does not reuse the animated value. Care must be taken
         * when using this option because on every evaluation a new <code>double[]</code> will be
         * allocated.
         *
         * @see #DoubleArrayEvaluator(double[])
         */
        DoubleArrayEvaluator() {
        }

        /**
         * Create a DoubleArrayEvaluator that reuses <code>reuseArray</code> for every evaluate() call.
         * Caution must be taken to ensure that the value returned from
         * {@link ValueAnimator#getAnimatedValue()} is not cached, modified, or
         * used across threads. The value will be modified on each <code>evaluate()</code> call.
         *
         * @param reuseArray The array to modify and return from <code>evaluate</code>.
         */
        DoubleArrayEvaluator(double[] reuseArray) {
            mArray = reuseArray;
        }

        /**
         * Interpolates the value at each index by the fraction. If
         * {@link #DoubleArrayEvaluator(double[])} was used to construct this object,
         * <code>reuseArray</code> will be returned, otherwise a new <code>double[]</code>
         * will be returned.
         *
         * @param fraction   The fraction from the starting to the ending values
         * @param startValue The start value.
         * @param endValue   The end value.
         * @return A <code>double[]</code> where each element is an interpolation between
         * the same index in startValue and endValue.
         */
        @Override
        public double[] evaluate(float fraction, double[] startValue, double[] endValue) {
            double[] array = mArray;
            if (array == null) {
                array = new double[startValue.length];
            }

            for (int i = 0; i < array.length; i++) {
                double start = startValue[i];
                double end = endValue[i];
                array[i] = start + (fraction * (end - start));
            }
            return array;
        }
    }

    public void updateLocationToFirebase(Location location) {
        try {
            for (String category : arrayCategory) {
                System.out.println("enter the car category size" + category);
                SharedHelper.putKey(context, "category", category);
                if (SharedHelper.getKey(context, "trip_id") == null || SharedHelper.getKey(context, "trip_id").equalsIgnoreCase("null") || SharedHelper.getKey(context, "trip_id").isEmpty()) {
                    geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child(category.toLowerCase()));
                } else {
                    geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child("trip_location"));

                }

                geofire(location, geoFire);
            }
            if (mCurrentLocation != null) {
                if (starLocation == null) {
                    starLocation = mCurrentLocation;
                    EndLocation = mCurrentLocation;

                } else {
                    starLocation = mCurrentLocation;
                }
                double distanceInKiloMeters = (starLocation).distanceTo(EndLocation) / 1000; // as distance is in meter
                if (distanceInKiloMeters >= 0.2 | CommonData.isFistTime) {
                    CommonData.isFistTime = false;
                    EndLocation = starLocation;
                    if (onlinestatus) {
                        upDateLocationPresnter("1", new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));

                    } else {
                        upDateLocationPresnter("0", new LatLng(0.0, 0.0));

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void remogeoFire() {
        try {
            if (!CommonData.RequestBoolean) {
                if (mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.disconnect();
                }
                if (arrayCategory != null && !arrayCategory.isEmpty()) {
                    for (String category : arrayCategory) {
                        geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child(category.toLowerCase()));
                        geoFire.removeLocation(driverId);

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public Location LatLngToLocation(String locatin, Double lat, Double lng) {
        final Location location = new Location(locatin);
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }

    public void geofire(Location location, GeoFire geoFire) {
        if (driverId != null && !driverId.equalsIgnoreCase("null")) {
            mCurrentLocation = location;
            if (mCurrentLocation != null) {
                if (proofstatus && onlinestatus) {
                    System.out.println("location which is updated in==>" + mCurrentLocation.getLatitude() + "<====>" + mCurrentLocation.getLongitude());
                    //this.geoFire.offlineLocation(driverId, new GeoLocation(0.0, 0.0), new GeoFire.CompletionListener() {
                    MainActivity.geoFire.setLocation(driverId, new GeoLocation(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), previousBearing, (key, error) -> {
                        if (error != null) {
                            System.err.println("There was an error saving the location to GeoFire: " + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());
                        } else {
                            System.out.println("OnlineOflline Location saved on server successfully!");
                        }
                    });
                } else {

                    MainActivity.geoFire.offlineLocation(driverId, new GeoLocation(0.0, 0.0), new GeoFire.CompletionListener() {
                        @Override
                        public void onComplete(String key, DatabaseError error) {
                            if (error != null) {
                                System.err.println("There was an error saving the location to GeoFire: " + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());
                            } else {
                                System.out.println("Offline Location saved on server successfully!");
                            }
                        }
                    });
                }


            }
        }

    }

    public void updateSmartLocation() {
       /* if (runnable == null) {

            handler.postDelayed(runnable = new Runnable() {
                @Override
                public void run() {
                    handler.postDelayed(this, Constants.updateLocationToFBHandlerTime);
                    Log.d("tag", "Location Update Handler: Updating Now...");
                    updateLocationToFirebase(getFusedLocation());
                }
            }, Constants.updateLocationToFBHandlerTime);
        }*/
    }

    @Override
    protected void onStart() {
        super.onStart();
        Constants.RquestScreen = false;
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        updateSmartLocation();
        if (SharedHelper.getKey(context, "trip_id") != null && !SharedHelper.getKey(context, "trip_id").equalsIgnoreCase("null") && !SharedHelper.getKey(context, "trip_id").isEmpty()) {
            FirebaseTripFlow();

        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        Constants.RquestScreen = false;
        updateSmartLocation();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
        logout_layout.setEnabled(true);
        logout_layout.setClickable(true);

        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), userProfileImage, activity);
        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), profileImg, activity);
        if (SharedHelper.getKey(context, "fname") != null) {
            txtUserName.setText(SharedHelper.getKey(context, "fname") + " " + SharedHelper.getKey(context, "lname"));
            plateTxt.setText(SharedHelper.getKey(context, "fname") + " " + SharedHelper.getKey(context, "lname"));


        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Constants.RquestScreen = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        Constants.RquestScreen = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Constants.RquestScreen = true;
        //     StopService();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        try {
            if (alert11 != null && alert11.isShowing()) {
                alert11.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        RemoveRequestListioner();
        Constants.Previousstatus = "0";
        if (TripFlowValue != null) {
            TripFlowReference.removeEventListener(TripFlowValue);
        }
        Utiles.RemoveRefenceListioner(TripFlowReference, TripFlowValue);
        Utiles.RemoveRefenceListioner(ProofstatusReference, ProofstatusValue);
        Utiles.RemoveRefenceListioner(CategoryDatabase, CategoryValueEvent);
        Utiles.RemoveRefenceListioner(RequestDatabaseReference, RequestValueListener);
        Utiles.RemoveRefenceListioner(TripFlowReference, TripFlowValue);
        Utiles.RemoveRefenceListioner(scheduleTripDatabase, scheduleTripValue);

    }

    public static void RemoveRequestListioner() {
        if (RequestValueListener != null) {
            RequestDatabaseReference.removeEventListener(RequestValueListener);
        }
    }

    public void alertChange() {
        final LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.alert_change, null);

        TextView manage_txt = layout.findViewById(R.id.manage_txt);
        TextView txt_add_new = layout.findViewById(R.id.txt_add_new);
        vehicle_list = layout.findViewById(R.id.vehicle_list);

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(this.findViewById(android.R.id.content));

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setView(layout);

        VehicleDialog = alert.create();
        VehicleDialog.setCancelable(true);
        VehicleDialog.show();


        manage_txt.setOnClickListener(v -> {
            VehicleDialog.dismiss();
            fragment = new ManageVehiclesFragment();
            moveToFragment(fragment);
        });

        txt_add_new.setOnClickListener(v -> {
            VehicleDialog.dismiss();
            fragment = new AddVehicleFragment();
            moveToFragment(fragment);
        });

        setAdapter();
    }

    public void setAdapter() {
        if (vehicleModels != null && !vehicleModels.isEmpty()) {
            vehicle_list.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            vehicle_list.setItemAnimator(new DefaultItemAnimator());
            vehicle_list.setHasFixedSize(true);
            vehicleListAdapter = new VehicleListAdapter(activity, vehicleModels, this);
            vehicle_list.setAdapter(vehicleListAdapter);
        }
    }

    // foramte getfrom firebase
    private Object cancelExceeds;
    private Object lastCanceledDate;
    private Object creditBalance;

    public void DriverProofstatus() {

        ProofstatusReference = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(SharedHelper.getKey(context, "userid"));
        ProofstatusValue = ProofstatusReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("current vehicle status" + dataSnapshot);
                if (dataSnapshot.getValue() != null) {
                    Object Proofstatus = dataSnapshot.child("proof_status").getValue();
                    Object Onlinestatus = dataSnapshot.child("online_status").getValue();
                    Object vehicleID = dataSnapshot.child("vehicle_id").getValue();
                    creditBalance = dataSnapshot.child("credits").getValue();
                    cancelExceeds = dataSnapshot.child("cancelExceeds").getValue();
                    lastCanceledDate = dataSnapshot.child("lastCanceledDate").getValue();
                    CommonData.walletBalance = creditBalance.toString();
                    System.out.println("Enter the vehicle id" + vehicleID);

                    if (vehicleID != null && !vehicleID.equals("0")) {
                        getVehicleCategory(vehicleID.toString());
                    }
                    if (Proofstatus != null && Proofstatus.toString().equalsIgnoreCase("Accepted")) {
                        if (creditBalance != null) {
                            if (MakeDouble(CommonFirebaseListoner.lowBalanceAlerts) < Double.valueOf(creditBalance.toString()) && Constants.WalletAlertEnable) {
                                WalletBalance(CommonFirebaseListoner.minimumBalanceDriverAlerts, true);
                            } else {
                                proostatus.setVisibility(View.GONE);
                                proofstatus = true;
                                onlineOfflineSwitch.setEnabled(true);
                                if (Onlinestatus.toString().equalsIgnoreCase("1")) {
                                    onlinestatus = true;
                                    SharedHelper.putOnline(activity, "onlineStatus", onlinestatus);
                                    if (!onlineOfflineSwitch.isChecked()) {
                                        onlineOfflineSwitch.setChecked(true);
                                    }
                                    onlineOfflineText.setText(R.string.available);
                                    getRequestStatus();
                                    CommonData.isFistTime = true;
                                } else {
                                    onlinestatus = false;
                                    if (onlineOfflineSwitch.isChecked()) {
                                        onlineOfflineSwitch.setChecked(false);
                                    }
                                    SharedHelper.putOnline(activity, "onlineStatus", onlinestatus);
                                    onlineOfflineText.setText(R.string.unavailable);
                                    RemoveRequestListioner();
                                    CommonData.isFistTime = false;

                                }
                            }
                            LimitAlert();
                        } else {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            ref.child("drivers_data").child(driverId).child("credits").setValue("0");
                        }


                    } else {
                        SharedHelper.putOnline(activity, "onlineStatus", onlinestatus);
                        proofstatus = false;
                        onlineOfflineSwitch.setEnabled(false);
                        onlineOfflineSwitch.setChecked(false);
                        proostatus.setVisibility(View.VISIBLE);
                        if (Proofstatus == null) {
                            DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                            ref.child("drivers_data").child(driverId).child("proof_status").setValue("Accepted");
                        }
                        if (Proofstatus != null && Proofstatus.toString().matches("Rejected")) {
                            showdialog((getResources().getString(R.string.proof_rejected)));
                        } else {
                            showdialog(getResources().getString(R.string.proof_not_accept));
                        }

                    }


                } else {
                    Utiles.setDriversData(context);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("tag", databaseError.getMessage());

            }
        });
        onlineOfflineSwitch.setOnCheckedChangeListener((compoundButton, ischeck) -> {
            if (Constants.RequestStart) {
                if (ischeck) {
                    OnlineOfflinePresenterCall("1");
                    onlineOfflineText.setText(R.string.available);
                } else {
                    OnlineOfflinePresenterCall("0");
                    onlineOfflineText.setText(R.string.unavailable);
                }
            }
        });
    }

    public Double MakeDouble(String value) {
        double floats;
        try {
            floats = Double.valueOf(value);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            floats = 0.0;
        }

        return floats;
    }

    public void LimitAlert() {
        if (CommonFirebaseListoner.isWallet) {
            /*if (cancelExceeds != null && !cancelExceeds.toString().isEmpty()) {
                System.out.println("ente the addressssss" + getDateFormate());
                System.out.println("ente the endaddjd" + lastCanceledDate.toString());
                if (!cancelExceeds.equals("0") && !lastCanceledDate.equals("0") && getDateFormate().equalsIgnoreCase(lastCanceledDate.toString())) {
                    WalletBalance(driverCancelLimiitExceeds, true);
                } else {
                    updateLimitdate();
                }
            } else {
                updateLimitdate();
            }*/
            if (MakeDouble(CommonFirebaseListoner.minimumBalances) < Double.valueOf(creditBalance.toString()) && Constants.WalletAlertEnable) {
                WalletBalance(CommonFirebaseListoner.exceededminimumBalanceDriver, false);
            }
        }

    }

    public void updateLimitdate() {
        DatabaseReference Driverdata = FirebaseDatabase.getInstance().getReference().child("drivers_data").child(SharedHelper.getKey(context, "userid"));
        HashMap<String, Object> map = new HashMap<>();
        map.put("cancelExceeds", "0");
        map.put("lastCanceledDate", "0");
        Driverdata.updateChildren(map);
    }

    public void WalletBalance(String amount, Boolean from) {
        WalletAlertDialog dialogClass = new WalletAlertDialog(activity, amount, from);
        dialogClass.setCancelable(false);
        Objects.requireNonNull(dialogClass.getWindow()).getAttributes().windowAnimations = R.style.fate_in_and_fate_out;
        try {
            runOnUiThread(dialogClass::show);
        } catch (Exception e) {
            e.printStackTrace();
        }
        onlinestatus = false;
        if (onlineOfflineSwitch.isChecked()) {
            onlineOfflineSwitch.setChecked(false);
        }
        onlineOfflineText.setText(R.string.unavailable);

        OnlineOfflinePresenterCall("0");
        RemoveRequestListioner();
    }

    public void OnlineOfflinePresenterCall(String status) {

        driverPresenter.getOnlineOffline(status, activity, context, true);
    }

    public void Alertdialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(R.string.logout);
        builder1.setMessage(R.string.are_your);
        builder1.setCancelable(false);
        builder1.setPositiveButton(
                activity.getResources().getString(R.string.yes),
                (dialog, id) -> {
                    dialog.dismiss();
                    if (mGoogleApiClient.isConnected()) {
                        mGoogleApiClient.disconnect();
                    }
                    RemoveRequestListioner();
                    StopService();
                    LogoutPresenter logoutPresenter = new LogoutPresenter();
                    logoutPresenter.LogoutData(activity);
                    try {
                        remogeoFire();
                        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
                        ref.child("drivers_data").child(driverId).child("online_status").setValue("0");
                       /* if (handler != null && runnable != null) {
                            handler.removeCallbacks(runnable);
                            handler.removeCallbacksAndMessages(null);
                            handler.removeMessages(0);

                        }*/

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    CommonData.walletBalance = "0";
                    Intent intent = new Intent(context, WelcomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    SharedHelper.clearSharedPreferences(context);
                    ActivityCompat.finishAffinity(MainActivity.this);
                });
        builder1.setNegativeButton(activity.getResources().getString(R.string.no), (dialog, which) -> dialog.dismiss());
        /*if (alert11 != null && alert11.isShowing()) {
            alert11.dismiss();
            alert11 = builder1.create();
        } else {
            alert11 = builder1.create();
        }
*/
        alert11 = builder1.create();
        alert11.show();

    }

    public void StopService() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            stopService(new Intent(context, TripRquestService.class));
        } else {
            stopService(new Intent(context, TripRquestService.class));
        }

    }

    public void upDateLocationPresnter(String status, LatLng latLng) {
        HashMap<String, String> map = new HashMap<>();
        map.put("lat", String.valueOf(latLng.latitude));
        map.put("lon", String.valueOf(latLng.longitude));
        map.put("status", status);
        driverPresenter.UpdateLocation(map, activity);
    }

    public void showdialog(String message) {
        final TSnackbar snackbar = TSnackbar.make(findViewById(android.R.id.content), message, TSnackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.RED);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.RED);
        TextView textView = snackbarView.findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        textView.setTypeface(null, Typeface.BOLD);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        params.setMargins(0, 30, 0, 0);
        textView.setLayoutParams(params);
        snackbar.show();
    }

    private String previousdest = "";
    private String previousaddess = "";

    public void FirebaseTripFlow() {

        TripFlowReference = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        TripFlowValue = TripFlowReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    System.out.println("enter the json reposnedd" + dataSnapshot);
                    Object status = dataSnapshot.child("status").getValue();
                    Object driver_alavance_dis = dataSnapshot.child("driver_alavance_dis").getValue();
                    Object destinationlat = dataSnapshot.child("Drop_latlng").getValue();
                    Object destinationaddress = dataSnapshot.child("Drop_address").getValue();
                    if (driver_alavance_dis != null && driver_alavance_dis.toString().equalsIgnoreCase("0")) {
                        CommonData.driveAllovanceDis = driver_alavance_dis.toString();
                    }
                    if (destinationlat != null && !destinationlat.toString().equalsIgnoreCase("0") && !previousdest.equalsIgnoreCase(destinationlat.toString())) {
                        previousdest = destinationlat.toString();
                        if (Constants.Previousstatus.equalsIgnoreCase("3")) {
                            destLocation = Utiles.getDestination(destinationlat.toString());
                            DrawablePolyline();
                        }
                    }
                    if (destinationaddress != null && !destinationaddress.toString().equalsIgnoreCase("0") && !previousaddess.equalsIgnoreCase(destinationaddress.toString())) {
                        previousaddess = destinationaddress.toString();
                        EventBus.getDefault().postSticky(new DestinationAddressEvent(destinationaddress.toString()));
                    }
                    if (status != null) {
                        if (!Constants.Previousstatus.equalsIgnoreCase(status.toString())) {
                            Constants.Previousstatus = status.toString();
                            TripFlowSwitch(status.toString());


                        }

                    }
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("tag", "enter the error response" + databaseError.getMessage());
            }
        });
    }

    public void TripFlowSwitch(String status) {
        switch (status) {
            case "1":
                RemoveFragment(FlowFragment);
                FlowFragment = new TripFlowFragment("1", mCurrentLocation);
                driverOnlineLayout.setVisibility(View.GONE);
                FlowFragment(FlowFragment);
                break;
            case "2":
                RemoveFragment(FlowFragment);
                driverOnlineLayout.setVisibility(View.GONE);
                FlowFragment = new TripFlowFragment("2", mCurrentLocation);
                FlowFragment(FlowFragment);
                break;
            case "3":
                RemoveFragment(FlowFragment);
                driverOnlineLayout.setVisibility(View.GONE);
                FlowFragment = new TripFlowFragment("3", mCurrentLocation);
                FlowFragment(FlowFragment);
                break;
            case "4":
                mMap.clear();
                currentLocMarker = null;
                driverOnlineLayout.setVisibility(View.GONE);
                //updateView(driverOnlineLayout,false);
                RemoveFragment(FlowFragment);
                FlowFragment = new SummaryFragment(null, "Farebase");
                SummaryFragment(FlowFragment);
                RemovePolyline();
                break;
            case "5":
                mMap.clear();
                currentLocMarker = null;
                Notrip();
                RemoveFragment(FlowFragment);
                currentLocation();
                removeAllFragments(fragmentManager);
                Utiles.ClearFirebase(context);
                RemovePolyline();
                SharedHelper.putKey(activity, "trip_id", "null");
                break;
        }

    }

    public void updateView(View view, boolean ischeck) {
        RxView.layoutChangeEvents(view)
                .subscribe(ignore -> {
                    if (ischeck) {
                        if (view.getVisibility() == View.GONE) {
                            view.setVisibility(View.VISIBLE);
                        }
                    } else {
                        if (view.getVisibility() == View.VISIBLE) {
                            view.setVisibility(View.GONE);
                        }
                    }

                }, Exception::new);

    }


    public static String getCurrentTime() {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm");//24 Hour Format
        date.setTimeZone(TimeZone.getDefault());
        String localTime = date.format(currentLocalTime);
        return localTime.replaceAll(" ", "%20");

    }

    private void removeAllFragments(FragmentManager fragmentManager) {
        Reintialize();
        runOnUiThread(() -> {
            if (fragmentManager != null) {
                try {
                    while (fragmentManager.getBackStackEntryCount() > 0) {
                        fragmentManager.popBackStackImmediate();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void Reintialize() {
        if (FragmentManage == null) {
            FragmentManage = getSupportFragmentManager();
        }
    }

    public void getDateFormate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        System.out.println("Current date formate => " + formattedDate.replaceAll(" ", ""));
        formattedDate.replaceAll(" ", "");
    }

    public void getAddress(final LatLng LatLng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(LatLng.latitude, LatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && !addresses.isEmpty()) {
                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                if (addresses.get(0).getLocality() != null && !addresses.get(0).getLocality().isEmpty()) {
                    callPolygon(addresses.get(0).getLocality());
                } else {
                    googleGeocoderPresenter.getAddressFromLocation(LatLng);
                }
            } else {
                googleGeocoderPresenter.getAddressFromLocation(LatLng);
            }

        } catch (IOException e) {
            e.printStackTrace();
            googleGeocoderPresenter.getAddressFromLocation(LatLng);

        }
    }

    public void callPolygon(String cityname) {
        if (!cityname.equalsIgnoreCase(strCityName)) {
            strCityName = cityname;
            cityPolygonPresenter.getAddressFromLocation(cityname);
        }
    }

    public void setPolygon(List<LatLng> data) {
        if (mMap != null) {
            mMap.addPolygon(new PolygonOptions().addAll(data).fillColor(activity.getResources().getColor(R.color.light_blue)));
        }
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}
